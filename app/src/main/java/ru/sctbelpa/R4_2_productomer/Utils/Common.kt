package ru.sctbelpa.R4_2_productomer.Utils

import android.content.res.Resources
import android.graphics.Color
import android.util.Log
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import ru.sctbelpa.R4_2_productomer.DataStorage.IRawValue
import ru.sctbelpa.R4_2_productomer.DataStorage.IResult
import ru.sctbelpa.R4_2_productomer.GUI.lineDataSet
import ru.sctbelpa.R4_2_productomer.R
import ru.sctbelpa.libsmoothspline.Point
import java.util.*
import kotlin.math.max


const val temperatureFormat = "%.3f °C"
const val LOG_TAG = "Common"

const val FORCE_MAX_ROI = false

fun resultValues(res: IResult, resources: Resources, locale: Locale): Pair<String, String> {
    var Tres: String
    var aW: String
    try {
        val fr = res.tresult
        val _aw = res.aw
        Tres = String.format(locale, temperatureFormat, fr)
        aW = String.format(locale, "%g", _aw)
    } catch (ex: Exception) {
        Log.e("Common.resultValues()", ex.message, ex)
        Tres = resources.getString(R.string.undefined)
        aW = resources.getString(R.string.undefined)
    }

    return Pair(Tres, aW)
}

fun GraphData(res: IResult, resources: Resources, ROIMin: Int): LineData {
    val rawdata = res.rawData

    if (rawdata.isEmpty()) {
        return LineData()
    }

    val offset = rawdata[0].timestamp.time

    val rawDatabuilder: LineDataSet.() -> Unit = {
        setDrawFilled(false)
        setDrawValues(false)
        setDrawCircles(true)
        circleSize = 2.0f
        setCircleColor(Color.GRAY)
        color = android.R.color.transparent // hide lines
    }

    val resultTimestamp: Long
    try {
        resultTimestamp = res.resultTimestamp
    } catch (ex: Exception) {
        // Точка перегиба не найдена, рисуем только сырые данные, причем все
        Log.e(LOG_TAG, "No results", ex)

        val dataSet = lineDataSet(convertIRawValues2Entrys(offset, rawdata), "RawData", rawDatabuilder)

        return LineData(dataSet)
    }

    val ROI = selectROI(rawdata, resultTimestamp, ROIMin)

    val RawdataSet = lineDataSet(convertIRawValues2Entrys(offset, ROI, rawdata), "RawData", rawDatabuilder)

    val spline = lineDataSet(genSmoothData(res, offset, ROI), "Spline") {
        setDrawFilled(true)
        setDrawValues(false)
        setDrawCircles(false)
        mode = LineDataSet.Mode.LINEAR
        color = resources.getColor(R.color.spline_line)
        fillColor = resources.getColor(R.color.spline_fill)
    }

    val dY_graph = lineDataSet(rescaleData(genDY(res, ROI), RawdataSet, offset), "d2Y") {
        setDrawFilled(false)
        setDrawValues(false)
        setDrawCircles(false)
        mode = LineDataSet.Mode.LINEAR
        color = resources.getColor(R.color.d2Y_color)
        //setAxisDependency(YAxis.AxisDependency.RIGHT); // раскоментировать, чтобы рисовать по разным Y осям
    }

    val result = LineData(/*RawdataSet, */spline, dY_graph)

    graphMarkers(ROI, res, offset, resultTimestamp, resources, result,
            resources.getColor(R.color.result_marker_color))

    return result
}

fun graphMarkers(ROI: Pair<Long, Long>, res: IResult, offset: Long,
                 resultTimestamp: Long, resources: Resources, target: LineData, markercolor: Int) {

    val allMarkers = lineDataSet(
            convertPoints2Entrys(offset, ROI, res.falseKinksResults), "allMarkers") {
        setCircleColor(resources.getColor(R.color.false_results_markers))
        setDrawValues(false)
        setDrawCircles(true)
        color = Color.TRANSPARENT // hide lines
    }
    target.addDataSet(allMarkers)

    val resultMarker = lineDataSet(
            singlePointList(offset, resultTimestamp, res.tresult), "resultMarker") {
        setCircleColor(markercolor)
        circleHoleColor = resources.getColor(R.color.result_marker_color)
        valueTextSize = 20f
        setDrawValues(false)
        setDrawCircles(true)
        circleSize = 5.0f
    }
    target.addDataSet(resultMarker)
}

fun selectROI(rawdata: List<IRawValue>, resultTimestamp: Long,
              ROIMin: Int): Pair<Long, Long> {

    if (FORCE_MAX_ROI) {
        return Pair(rawdata[0].timestamp.time,
                rawdata[rawdata.size - 1].timestamp.time)
    } else {
        // нужно, чтобы в диопазон попадал 0 *С, результат и максимум по ROItailsMax отсчетов дополнительно
        val interestList = findDataZeroCrossing(rawdata)
        if (interestList.isEmpty()) {
            return Pair(rawdata[0].timestamp.time,
                    rawdata[rawdata.size - 1].timestamp.time)
        }

        interestList.add(resultTimestamp)

        val interestMin = interestList.min()!!
        val interestMax = interestList.max()!!

        val distancePath: Int = max(
                rawdata.count { it.timestamp.time in interestMin..interestMax } / 4,
                ROIMin
        )

        val size = rawdata.size

        var countBefore = rawdata.indexOfFirst { it.timestamp.time >= interestMin }
        var countAfter = rawdata.reversed().indexOfFirst { it.timestamp.time <= interestMax }

        countBefore = if (countBefore > distancePath) {
            countBefore - distancePath
        } else {
            0
        }
        countAfter = if (countAfter > distancePath) {
            countAfter - distancePath
        } else {
            0
        }

        return Pair(rawdata[countBefore].timestamp.time,
                rawdata[size - 1 - countAfter].timestamp.time)
    }
}

private fun findDataZeroCrossing(rawdata: List<IRawValue>): MutableList<Long> {
    val result = ArrayList<Long>()
    for (i in 0 until rawdata.size - 1) {
        val current = rawdata[i]
        val next = rawdata[i + 1]

        if (current.temperature * next.temperature <= 0) {
            result.add(current.timestamp.time)
        }
    }
    return result
}

private fun convertIRawValues2Entrys(offset: Long, rawdata: List<IRawValue>): List<Entry> =
        rawdata.map { Entry((it.timestamp.time - offset).toFloat(), it.temperature) }

private fun convertIRawValues2Entrys(offset: Long, ROI: Pair<Long, Long>,
                                     data: List<IRawValue>): List<Entry> = data
        .filter {
            val ts = it.timestamp.time
            return@filter ts >= ROI.first && ts <= ROI.second
        }
        .map { Entry((it.timestamp.time - offset).toFloat(), it.temperature) }


private fun genSmoothData(res: IResult, t_offset: Long, ROI: Pair<Long, Long>): List<Entry> = res.smoothData(ROI).map { Entry((it.X - t_offset).toFloat(), it.Y.toFloat()) }

private fun genDY(res: IResult, ROI: Pair<Long, Long>): List<Point> = res.dYData(ROI)

private fun singlePointList(offset: Long, resultTimestamp: Long,
                            result: Float): List<Entry> {
    return listOf(Entry((resultTimestamp - offset).toFloat(), result))
}

private fun rescaleData(src: List<Point>, reference: LineDataSet, t_offset: Long): List<Entry> {
    val yMin = reference.yMin
    val yMax = reference.yMax
    val diff = yMax - yMin

    val srcMin = src.minBy { it.Y }
    val srcMax = src.maxBy { it.Y }
    val srcDiff = srcMax!!.Y - srcMin!!.Y

    val k = diff / srcDiff

    return src.map { Entry((it.X - t_offset).toFloat(), (it.Y * k).toFloat()) }
}

private fun convertPoints2Entrys(offset: Long, ROI: Pair<Long, Long>, data: List<Point>): List<Entry> = data
        .filter {
            val ts = it.X
            return@filter ts >= ROI.first && ts <= ROI.second
        }
        .map { Entry((it.X - offset).toFloat(), it.Y.toFloat()) }
