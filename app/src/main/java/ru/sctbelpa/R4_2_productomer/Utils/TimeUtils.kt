package ru.sctbelpa.R4_2_productomer.Utils

import java.util.concurrent.TimeUnit

/**
 * converts time (in milliseconds) to human-readable format
 * "<dd:>hh:mm:ss"
</dd:> */
fun millisToShortDHMS(duration: Long): String {
    val days = TimeUnit.MILLISECONDS.toDays(duration)
    val hours = TimeUnit.MILLISECONDS.toHours(duration) -
            TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(duration))
    val minutes = TimeUnit.MILLISECONDS.toMinutes(duration) -
            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duration))
    val seconds = TimeUnit.MILLISECONDS.toSeconds(duration) -
            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
    return if (days == 0L) {
        String.format("%02d:%02d:%02d", hours, minutes, seconds)
    } else {
        String.format("%dd%02d:%02d:%02d", days, hours, minutes, seconds)
    }
}

fun ms2S(ms: Long): Float = ms.toFloat() / 1000.0F
fun S2ms(s: Float): Long = (s * 1000F).toLong()
