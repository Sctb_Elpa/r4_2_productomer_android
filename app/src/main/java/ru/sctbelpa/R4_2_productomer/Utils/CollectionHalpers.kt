package ru.sctbelpa.R4_2_productomer.Utils

infix fun <T> Collection<T>.sameContentWith(collection: Collection<T>?) = collection?.let { this.size == it.size && this.containsAll(it) }

fun <K, V> MutableMap<K, V>.removeAll(collection: Collection<K>?) = collection?.forEach { this@removeAll.remove(it) }