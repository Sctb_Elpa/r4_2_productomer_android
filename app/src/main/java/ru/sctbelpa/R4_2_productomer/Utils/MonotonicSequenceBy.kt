package ru.sctbelpa.R4_2_productomer.Utils

import java.util.*


/** if foundf duplicates in sequence collapse it to 1 element
 * listOf(1, 2, 2, 3).asSequence().collapseSeries() -> 1, 2, 3
 **/
class MonotonicBy<T, K>(private val sequence: Sequence<T>,
                        private val keySelector: (T) -> K,
                        private val comparator: Comparator<K>) : Sequence<T> {

    override fun iterator(): Iterator<T> = object : Iterator<T> {
        val iterator = sequence.iterator()
        var nextState: Int = -1 // -1 for unknown, 0 for done, 1 for continue
        var nextItem: T? = null

        var nextKey: K? = null

        private fun calcNext() {
            while (iterator.hasNext()) {
                val item = iterator.next()
                val key = keySelector(item)
                val k = nextKey

                if (k == null || comparator.compare(key, k) < 0) {
                    nextKey = key
                    nextItem = item
                    nextState = 1
                    return
                }
            }
            nextState = 0
        }

        override fun next(): T {
            if (nextState == -1)
                calcNext()
            if (nextState == 0)
                throw NoSuchElementException()
            val result = nextItem
            nextItem = null
            nextState = -1

            @Suppress("UNCHECKED_CAST")
            return result as T
        }

        override fun hasNext(): Boolean {
            if (nextState == -1)
                calcNext()
            return nextState == 1
        }
    }
}

fun <T, K : Comparable<K>> Sequence<T>.monotonicDownBy(keySelector: (T) -> K): Sequence<T> =
        MonotonicBy(this, keySelector, object : Comparator<K> {
            override fun compare(o1: K, o2: K): Int {
                return o1.compareTo(o2)
            }
        })

fun <T, K : Comparable<K>> Sequence<T>.monotonicUpBy(keySelector: (T) -> K): Sequence<T> =
        MonotonicBy(this, keySelector, object : Comparator<K> {
            override fun compare(o1: K, o2: K): Int {
                return o2.compareTo(o1)
            }
        })