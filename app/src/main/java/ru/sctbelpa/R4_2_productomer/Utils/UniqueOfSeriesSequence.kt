package ru.sctbelpa.R4_2_productomer.Utils

import java.util.NoSuchElementException


/** if foundf duplicates in sequence collapse it to 1 element
 * listOf(1, 2, 2, 3).asSequence().collapseSeries() -> 1, 2, 3
 **/
class UniqueOfSeriesSequence<T>(private val sequence: Sequence<T>) : Sequence<T> {

    override fun iterator(): Iterator<T> = object : Iterator<T> {
        val iterator = sequence.iterator()
        var nextState: Int = -1 // -1 for unknown, 0 for done, 1 for continue
        var nextItem: T? = null

        private fun calcNext() {
            while (iterator.hasNext()) {
                val item = iterator.next()
                if (nextItem != item) {
                    nextItem = item
                    nextState = 1
                    return
                }
            }
            nextState = 0
        }

        override fun next(): T {
            if (nextState == -1)
                calcNext()
            if (nextState == 0)
                throw NoSuchElementException()
            val result = nextItem
            nextItem = null
            nextState = -1

            @Suppress("UNCHECKED_CAST")
            return result as T
        }

        override fun hasNext(): Boolean {
            if (nextState == -1)
                calcNext()
            return nextState == 1
        }
    }
}

fun <T> Sequence<T>.collapseSeries(): Sequence<T> = UniqueOfSeriesSequence(this)