package ru.sctbelpa.R4_2_productomer.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

public class ProductomerService extends Service {
    static final String LOG_TAG = "ProductomerService";
    public static final UUID defaultUUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    BluetoothAdapter adapter;
    BluetoothDevice device;
    UUID targetUUID;
    BluetoothSocket bt_socket;
    Dispatcher dispatcher = null;



    @Override
    public void onCreate() throws NullPointerException {
        super.onCreate();
        Log.d(LOG_TAG, "onCreate");
        adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter == null)
            throw new NullPointerException("No bluetooth Adapters present in system");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        return new LocalBinder();
    }

    public final BluetoothAdapter getAdapter() {
        return adapter;
    }

    public void setTargetServer(BluetoothDevice device, UUID uuid) {
        if (bt_socket != null && bt_socket.isConnected()) {
            disconnect();
        }
        if (device == null)
            throw new NullPointerException("Device == null");

        this.device = device;
        this.targetUUID = uuid == null ? defaultUUID : uuid;
    }

    private void disconnect() {
        if (bt_socket != null) {
            try {
                bt_socket.close();
            } catch (IOException e) {
                Log.e(LOG_TAG, String.format("IOException while closing BluetoothSocket (%s)",
                        e.getMessage()));
                stopDispatcher();
            }
        }
        targetUUID = null;
        device = null;
        bt_socket = null;
    }

    public void connect() throws IOException {
        adapter.cancelDiscovery();
        try {
            bt_socket = device.createRfcommSocketToServiceRecord(targetUUID);
            bt_socket.connect();
            stopDispatcher();
            startDispatcher();
        } catch (IOException e) {
            Log.e(LOG_TAG, String.format("Can't open connection to %s (%s)",
                    device.toString(), e.getMessage()));
            throw e;
        }
    }

    private void stopDispatcher() {
        if (dispatcher != null)
            try {
                dispatcher.interrupt();
                dispatcher.join();
            } catch (InterruptedException e) { /* no problem there */ }
        dispatcher = null;
    }

    private void startDispatcher() throws IOException {
        dispatcher = new Dispatcher(bt_socket.getInputStream(), bt_socket.getOutputStream());
        dispatcher.start();
    }

    public void ensureConnected() throws IOException {
        if (bt_socket == null || !bt_socket.isConnected() ||
                dispatcher == null || !dispatcher.isRunning())
            connect();
    }

    public boolean isConnected() {
        return (bt_socket != null) && (bt_socket.isConnected());
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public UUID getUUID() {
        return targetUUID == null ? defaultUUID : targetUUID;
    }

    public CommandBuilder newCommandBuilder() {
        return new CommandBuilder(this);
    }

    public class LocalBinder extends Binder {
        public ProductomerService getService() {
            return ProductomerService.this;
        }
    }
}
