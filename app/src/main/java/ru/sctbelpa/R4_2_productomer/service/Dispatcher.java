package ru.sctbelpa.R4_2_productomer.service;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import ru.sctbelpa.MagickDelimitedProtobuf.EndOfStreamException;
import ru.sctbelpa.MagickDelimitedProtobuf.MagickDelimitedProtobufOutputStream;
import ru.sctbelpa.MagickDelimitedProtobuf.MagickDelimitedProtobufReader;
import ru.sktbelpa.r4_2_prodm.ProtobufDevice0000E004;


public class Dispatcher extends Thread {
    private static final String LOG_TAG = "CommandDispatcher";
    // Позволяет защититься от "заспамливания" устройства запросам, меньше делать не стоит,
    // ибо значения <25 вообще не приводят к желаемумому эффекту
    public static final int COMMAND_COOLDOWN = 25;

    /// Commands, what now in progress
    private final Map<Integer, ProductomerCommand> pendingCommands = new HashMap<>();

    @NonNull
    private final InputStream input;
    @NonNull
    private final OutputStream output;

    private final Timer sendTimer = new Timer();
    private final Queue<ProductomerCommand> commandQueue = new ArrayDeque<>();

    private MagickDelimitedProtobufReader reader;
    private MagickDelimitedProtobufOutputStream outputStream;

    public Dispatcher(@NonNull InputStream input, @NonNull OutputStream output) {
        this.input = input;
        this.output = output;
        reader = new MagickDelimitedProtobufReader(input);
        outputStream = new MagickDelimitedProtobufOutputStream(output);

        sendTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                final ProductomerCommand command = commandQueue.poll();
                if ((command == null) ||
                        (command.getState() != IProductomerCommand.STATE.INITIALISED)) {
                    return;
                }
                try {
                    outputStream.write(command.getRequest());
                } catch (Exception e) {
                    command.Fail(e);
                }

                command.setState(IProductomerCommand.STATE.SENT);
                command.StartLiveCycle();
                synchronized (pendingCommands) {
                    pendingCommands.put(command.getId(), command);
                }
            }
        }, COMMAND_COOLDOWN, COMMAND_COOLDOWN);
    }

    public void executeCommand(ProductomerCommand command) {
        commandQueue.add(command);
    }

    public boolean isRunning() {
        return isAlive();
    }

    public Collection<? extends IProductomerCommand> getPendingCommands() {
        return pendingCommands.values();
    }

    public boolean removeCommandFromPendingSet(IProductomerCommand cmd) {
        synchronized (pendingCommands) {
            return pendingCommands.remove(cmd.getId()) != null;
        }
    }

    public boolean isCommandPending(IProductomerCommand cmd) {
        return pendingCommands.containsValue(cmd);
    }

    @Override
    public void interrupt() {
        try {
            input.close();
            output.close();
        } catch (IOException e) {
        }
        super.interrupt();
    }

    @Override
    public void run() {
        while (true) {
            ProtobufDevice0000E004.Response response;
            try {
                response = reader.read(ProtobufDevice0000E004.Response.class);
            } catch (InterruptedException ex) {
                Log.w(LOG_TAG, "Reader thread was closed");
                cancelAllPendingCommands();
                return;
            } catch (InterruptedIOException ex) {
                Log.w(LOG_TAG, "Reader was closed while IO operation");
                cancelAllPendingCommands();
                return;
            } catch (EndOfStreamException ex) {
                Log.w(LOG_TAG, "input stream eas closed");
                cancelAllPendingCommands();
                return;
            } catch (IOException ex) {
                Log.e(LOG_TAG, ex.getMessage());
                return;
            } catch (Exception ex) {
                ex.printStackTrace();
                continue; /* unknown error */
            }

            ProductomerCommand pendingCommand = findPendingCommandById(response.getId());
            if (pendingCommand != null) {
                pendingCommand.Success(response);
            } else {
                Log.w(LOG_TAG, "Received correct message, bun not pending");
            }
        }
    }

    private void cancelAllPendingCommands() {
        for (IProductomerCommand cmd : pendingCommands.values())
            cmd.cancel();
    }

    @Nullable
    private ProductomerCommand findPendingCommandById(int id) {
        synchronized (pendingCommands) {
            return pendingCommands.containsKey(id) ? pendingCommands.get(id) : null;
        }
    }

    public int genNewUnusedId() {
        Set<Integer> usedIDs = pendingCommands.keySet();
        Random random = new Random();
        while (true) {
            int rndID = random.nextInt();
            if (!usedIDs.contains(rndID))
                return rndID;
        }
    }
}
