package ru.sctbelpa.R4_2_productomer.service;


import ru.sktbelpa.r4_2_prodm.ProtobufDevice0000E004;

public interface IProductomerCommand {
    enum STATE {
        UNINITIALISED,
        INITIALISED,
        SENT,
        ERROR,
        DONE
    }

    STATE getState();

    void setState(STATE newState);

    IProductomerCommand setRequest(ProtobufDevice0000E004.Request request) throws IllegalStateException;
    IProductomerCommand onSuccess(ISuccessCallback cb);
    IProductomerCommand onError(IFailCallback cb);

    IProductomerCommand setTimeout(long timeout) throws IllegalStateException;
    IProductomerCommand execute() throws Exception;

    boolean cancel();
    int getId() throws NullPointerException;

    String toString();

    long getTimeout();
}
