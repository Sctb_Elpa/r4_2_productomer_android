package ru.sctbelpa.R4_2_productomer.service

import ru.sktbelpa.r4_2_prodm.ProtobufDevice0000E004


interface ISuccessCallback {
    fun processResult(response: ProtobufDevice0000E004.Response)
}

