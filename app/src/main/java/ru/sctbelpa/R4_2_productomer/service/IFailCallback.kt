package ru.sctbelpa.R4_2_productomer.service


interface IFailCallback {
    fun processError(command: IProductomerCommand, ex: Exception)
}
