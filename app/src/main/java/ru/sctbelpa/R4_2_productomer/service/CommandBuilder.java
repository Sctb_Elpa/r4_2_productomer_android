package ru.sctbelpa.R4_2_productomer.service;

import android.support.annotation.NonNull;

import java.io.IOException;

import ru.sktbelpa.r4_2_prodm.ProtobufDevice0000E004;

class CommandBuilder {
    // may use one builder for create many messages

    private final ProductomerService productomerService;
    private final ProtobufDevice0000E004.Request.Builder requestBuilder;

    public ISuccessCallback successCallback = null;
    public IFailCallback failCallback = null;

    CommandBuilder(ProductomerService service) {
        productomerService = service;
        requestBuilder = CreateDefaultCommandBuilder();
    }

    public CommandBuilder settingsOperation(@NonNull ProtobufDevice0000E004.WriteSettingsReq newSettings) {
        if (newSettings != null) {
            requestBuilder.setWriteSettingsReq(newSettings);
        } else {
            requestBuilder.setWriteSettingsReq(
                    ProtobufDevice0000E004.WriteSettingsReq.getDefaultInstance());
        }
        return this;
    }

    public CommandBuilder settingsOperation(ProtobufDevice0000E004.WriteSettingsReq.Builder newSettingsBuilder) {
        if (newSettingsBuilder != null) {
            requestBuilder.setWriteSettingsReq(newSettingsBuilder);
        } else {
            requestBuilder.setWriteSettingsReq(
                    ProtobufDevice0000E004.WriteSettingsReq.getDefaultInstance());
        }
        return this;
    }

    public CommandBuilder getHistoryOperation(ProtobufDevice0000E004.HistoryReq HistoryReq) {
        requestBuilder.setGetHistory(HistoryReq);
        return this;
    }

    public CommandBuilder getHistoryOperation(ProtobufDevice0000E004.HistoryReq.Builder HistoryReqBuilder) {
        requestBuilder.setGetHistory(HistoryReqBuilder);
        return this;
    }

    public IProductomerCommand build(int targetDeviceID) throws IOException {
        productomerService.ensureConnected();
        requestBuilder.setId(productomerService.dispatcher.genNewUnusedId()).setDeviceID(targetDeviceID);
        return new ProductomerCommand(productomerService.dispatcher, requestBuilder.build())
                .onSuccess(successCallback).onError(failCallback);
    }

    public IProductomerCommand buildBroadcast() throws IOException {
        return build(ProtobufDevice0000E004.INFO.ID_DISCOVER_VALUE);
    }

    private ProtobufDevice0000E004.Request.Builder CreateDefaultCommandBuilder() {
        return ProtobufDevice0000E004.Request.newBuilder()
                .setProtocolVersion(ProtobufDevice0000E004.INFO.PROTOCOL_VERSION_VALUE);
    }
}