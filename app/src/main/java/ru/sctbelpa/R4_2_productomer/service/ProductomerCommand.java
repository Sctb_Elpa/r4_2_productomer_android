package ru.sctbelpa.R4_2_productomer.service;

import android.support.annotation.NonNull;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeoutException;

import ru.sktbelpa.r4_2_prodm.ProtobufDevice0000E004;

public class ProductomerCommand implements IProductomerCommand {

    public static final long DEFAULT_TIMEOUT = 10;

    private final Dispatcher dispatcher;
    private STATE state;
    private ProtobufDevice0000E004.Request request;
    private ISuccessCallback onSuccess = null;
    private IFailCallback onFail = null;
    private Timer timeoutTimer = new Timer();
    private long timeout = DEFAULT_TIMEOUT;

    public ProductomerCommand(@NonNull Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
        state = STATE.UNINITIALISED;
    }

    public ProductomerCommand(@NonNull Dispatcher dispatcher, ProtobufDevice0000E004.Request request) {
        this.dispatcher = dispatcher;
        this.request = request;
        state = request == null ? STATE.UNINITIALISED : STATE.INITIALISED;
    }

    @Override
    public long getTimeout() {
        return timeout;
    }

    @Override
    public IProductomerCommand setTimeout(long timeout) throws IllegalStateException {
        if (state != STATE.UNINITIALISED && state != STATE.INITIALISED)
            throw new IllegalStateException("Can't set timeout on-the-fly");
        this.timeout = timeout;
        return this;
    }

    @Override
    public STATE getState() {
        return state;
    }

    @Override
    public void setState(STATE newstate) {
        this.state = newstate;
    }

    @Override
    public IProductomerCommand setRequest(ProtobufDevice0000E004.Request request) throws IllegalStateException {
        if (state != STATE.UNINITIALISED)
            throw new IllegalStateException("Request may be assigned only on unused command");
        this.request = request;
        state = STATE.INITIALISED;
        return this;
    }

    @Override
    public IProductomerCommand onSuccess(ISuccessCallback cb) {
        onSuccess = cb;
        return this;
    }

    @Override
    public IProductomerCommand onError(IFailCallback cb) {
        onFail = cb;
        return this;
    }

    @Override
    public IProductomerCommand execute() throws Exception {
        if (state != STATE.INITIALISED)
            throw new IllegalStateException("Trying executeCommand uninitialized request");

        if (!dispatcher.isRunning()) {
            Fail(new InterruptedException("Dispatcher in illegal state"));
        } else {
            dispatcher.executeCommand(this);
        }

        return this;
    }

    @Override
    public boolean cancel() {
        if (state == STATE.SENT) {
            boolean canceled = dispatcher.isCommandPending(this);
            Fail(new CancellationException("Command was canceled"));
            return canceled;
        } else if (state == STATE.INITIALISED) {
            state = STATE.ERROR;
            Fail(new CancellationException());
            return false;
        } else {
            return false;
        }
    }

    @Override
    public int getId() throws NullPointerException {
        if (request == null) throw new NullPointerException("No request assigned");
        return request.getId();
    }

    public void StartLiveCycle() {
        timeoutTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Fail(new TimeoutException(
                        String.format("No response has been received in %d", timeout)));
            }
        }, timeout);
    }

    protected void FinishLiveCycle() {
        timeoutTimer.cancel();
        dispatcher.removeCommandFromPendingSet(this);
    }

    void Success(ProtobufDevice0000E004.Response response) {
        FinishLiveCycle();
        state = STATE.DONE;
        if (onSuccess != null) {
            onSuccess.processResult(response);
        }
    }

    void Fail(Exception ex) {
        FinishLiveCycle();
        state = STATE.ERROR;
        if (onFail != null) {
            onFail.processError(this, ex);
        }
    }

    ProtobufDevice0000E004.Request getRequest() {
        return request;
    }
}
