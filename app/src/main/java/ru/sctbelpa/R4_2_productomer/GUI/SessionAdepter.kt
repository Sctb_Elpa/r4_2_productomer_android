package ru.sctbelpa.R4_2_productomer.GUI

import android.content.Context
import android.content.res.Resources
import android.support.transition.TransitionManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ru.sctbelpa.R4_2_ProductomerService.ISessionID
import ru.sctbelpa.R4_2_productomer.DataStorage.ISession
import ru.sctbelpa.R4_2_productomer.R
import ru.sctbelpa.R4_2_productomer.Utils.LOG_TAG

class SessionAdepter(private val context: Context, private val sessions: List<ISession>) :
        RecyclerView.Adapter<SessionViewHolder>() {

    private val resources: Resources = context.resources
    private var prev_expanded: SessionViewHolderBehavior? = null
    private var recycler: RecyclerView? = null

    private var onItemRemovedListener: OnItemRemovedListener? = null

    interface OnItemRemovedListener {
        fun itemRemoved(sessionID: ISessionID?)
    }

    fun setOnItemRemovedListener(onItemRemovedListener: OnItemRemovedListener) {
        this.onItemRemovedListener = onItemRemovedListener
    }

    // количество элементов в списке
    override fun getItemCount(): Int = sessions.size

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        val swipeCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
                return ItemTouchHelper.Callback.makeMovementFlags(0, // no vertical drag
                        ItemTouchHelper.START or ItemTouchHelper.END // swipe both
                )
            }

            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition //get position which is swipe

                try {
                    onRemoveElement(sessions[position].id)
                    this@SessionAdepter.notifyItemRemoved(position)    //item removed from recylcerview
                } catch (ex: IndexOutOfBoundsException) {
                    Log.e(LOG_TAG, "BUG, remove session", ex)
                }
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeCallback)
        itemTouchHelper.attachToRecyclerView(recyclerView) //set swipe to recylcerview

        this.recycler = recyclerView
    }

    private fun onRemoveElement(id: ISessionID?) {
        if (onItemRemovedListener != null) {
            onItemRemovedListener!!.itemRemoved(id)
        }
    }

    // "Надуть" макеты для каждого элемента
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SessionViewHolder {
        val v = LayoutInflater.from(
                viewGroup.context).inflate(R.layout.sessioncard, viewGroup, false)
        return SessionViewHolder(v)
    }

    // наполнить каждый макет данными
    override fun onBindViewHolder(holder: SessionViewHolder, position: Int) {
        val s = sessions[position]

        with(SessionViewHolderBehavior(holder)) {
            update(s, resources, context)
            isActivated = false
            setOnCardClickListener(View.OnClickListener {
                // Получаем текущий статус карточки
                val activated = isActivated

                if (!activated) {
                    // развернуть
                    isActivated = true

                    // если открыта какая-то другая карточка - скрыть её
                    if (prev_expanded != null && prev_expanded !== this@with) {
                        prev_expanded!!.isActivated = false
                    }
                    prev_expanded = this@with
                } else {
                    isActivated = false
                    prev_expanded = this@with
                }

                TransitionManager.beginDelayedTransition(recycler!!)
            })
        }
    }
}