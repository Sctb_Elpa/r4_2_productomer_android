package ru.sctbelpa.R4_2_productomer.GUI

import android.content.Intent
import android.content.res.ColorStateList
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.TextView
import kotlinx.coroutines.*
import ru.sctbelpa.R4_2_ProductomerService.*
import ru.sctbelpa.R4_2_productomer.DataStorage.ISession
import ru.sctbelpa.R4_2_productomer.R
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    val sessionUpdateListener = object : ISession.SessionUpdateListener {
        override fun SessionUpdated(session: ISession) {
            launchUI {
                val res: Bundle?
                try {
                    res = ProductomerResultReceiver.awaitCallback {
                        with(newServiceIntent()) {
                            action = ServiceControl.ACTIONS.SAVE_DATASESSION.name
                            putExtra(ServiceControl.PARAM.SESSION_DATA.name, session)
                            putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, newResultReceiver(it))
                            startService(this)
                        }
                    }
                } catch (ex: Exception) {
                    Log.e(LOG_TAG, "Failed to save session", ex)
                    return@launchUI
                }
                val id = res.getResult<ISessionID>()
                session.id = id
            }
        }
    }

    private val serviceStatusReceiver by lazy {
        newResultReceiver(
                object : ProductomerResultReceiver.ResultReceiverCallBack {
                    override fun onSuccess(data: Bundle?) {
                        val resType = data?.getInt(ServiceControl.PARAM.RESULT_TYPE.name)

                        when (resType) {
                            ServiceControl.RESULT_TYPE.STATE_CHANGED.ordinal ->
                                launchUI { onServiceBusy(haveAnyBusySession()) }

                            ServiceControl.RESULT_TYPE.STORAGE_UPDATED.ordinal -> updateSessionList()
                        }
                    }

                    override fun onError(e: Exception?) {
                        Log.e(LOG_TAG, "Service watcher error", e)
                    }
                })
    }

    private val sessions = ArrayList<ISession>()

    private lateinit var recyclerView: RecyclerView
    private lateinit var List_replacmentText: TextView
    private lateinit var adepter: SessionAdepter
    private lateinit var fab: FloatingActionButton

    private val watcherId: Int by lazy { Random.nextInt() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.homescreen)

        // настрока RecyclerView
        recyclerView = findViewById(R.id.stored_sessions_list)
        List_replacmentText = findViewById(R.id.List_replacmentText)
        fab = findViewById(R.id.fab)

        ShowNewSessionPrompt()

        // установить менеджер слоёв, пока сойдет дефолтный
        val llm = LinearLayoutManager(baseContext)
        recyclerView.layoutManager = llm

        // установить адептер
        adepter = SessionAdepter(this, sessions)

        adepter.setOnItemRemovedListener(object : SessionAdepter.OnItemRemovedListener {
            override fun itemRemoved(sessionID: ISessionID?) {
                val index = (0..sessions.size).firstOrNull { sessions[it].id == sessionID }
                        ?: return
                requestRemoveFromStorage(sessionID, index)
            }
        })

        recyclerView.adapter = adepter

        fab.setOnClickListener {
            val intent = Intent(this,
                    ru.sctbelpa.R4_2_productomer.GUI.DataCollectionActivity::class.java)
            startActivity(intent)
        }

        registerServiceStatusWatcher()
    }

    override fun onDestroy() {
        super.onDestroy()

        unRegisterServiceStatusWatcher()
    }

    private fun newServiceIntent(): Intent = Intent(this@MainActivity,
            ru.sctbelpa.R4_2_ProductomerService.Service::class.java)

    private suspend fun haveAnyBusySession(): Boolean {
        val res = ProductomerResultReceiver.awaitCallback {
            with(newServiceIntent()) {
                action = ServiceControl.ACTIONS.FIRST_BUSY_SESSION.name
                putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, newResultReceiver(it))
                startService(this)
            }
        }
        return res.getResult<IWorkSessionID>() != null
    }

    private fun registerServiceStatusWatcher() = with(newServiceIntent()) {
        action = ServiceControl.ACTIONS.REGISTER_WATCHER.name
        putExtra(ServiceControl.PARAM.WATCHER_ID.name, watcherId)
        putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, serviceStatusReceiver)
        startService(this)
    }

    private fun unRegisterServiceStatusWatcher() = with(newServiceIntent()) {
        action = ServiceControl.ACTIONS.UNREGISTER_WATCHER.name
        putExtra(ServiceControl.PARAM.WATCHER_ID.name, watcherId)
        startService(this)
    }

    private fun showStoragePermissionsRequest() = launchUI {
        with(AlertDialog.Builder(this@MainActivity)) {
            setTitle(R.string.insufficient_storage_permissions)
            setIcon(R.drawable.ic_vegetables2)
            setMessage(Html.fromHtml(getText(R.string.app_storage_premissions_request).toString()))
            setPositiveButton(R.string.go_forward) { dialog, which ->
                // https://stackoverflow.com/a/32983128/8065921
                with(Intent()) {
                    action = android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    data = Uri.fromParts("package", packageName, null)
                    startActivity(this)
                }
            }
            create().show()
        }
    }

    private suspend fun loadSessionList(): List<ISessionID> {
        val res: Bundle?
        try {
            res = ProductomerResultReceiver.awaitCallback {
                with(newServiceIntent()) {
                    action = ServiceControl.ACTIONS.DATASESSION_LIST.name
                    putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, newResultReceiver(it))
                    startService(this)
                }
            }
        } catch (ex: IllegalStateException) {
            showStoragePermissionsRequest()
            return emptyList()
        } catch (ex: Exception) {
            showLoadSessionError(ex)
            return emptyList()
        }
        return res.getResult<List<ISessionID>>()!!
    }

    private fun updateSessionList() = launchUI {
        val sessionlist = loadSessionList()

        for (s in sessions.filterNot { it.id in sessionlist })
            sessions.remove(s)

        val existid = sessions.map { it.id }
        val notExisted = sessionlist.filterNot { it in existid }
        notExisted.forEach {
            async { loadSession(it) }
        }
    }

    private fun onServiceBusy(busy: Boolean) = with(fab) {
        if (busy) {
            setImageResource(R.drawable.ic_timeline_black_24dp)
            backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(
                    this@MainActivity, android.R.color.holo_red_light))
        } else {
            setImageResource(R.drawable.ic_add_black_24dp)
            backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(
                    this@MainActivity, R.color.colorPrimaryDark))
        }
    }

    private fun newRemoveFromStorageCanceler(cancelID: Int?, sessionID: ISessionID?,
                                             index: Int, snackbar: Snackbar): View.OnClickListener =
            View.OnClickListener {
                snackbar.dismiss()
                launchUI {
                    try {
                        ProductomerResultReceiver.awaitCallback {
                            with(newServiceIntent()) {
                                action = ServiceControl.ACTIONS.REMOVE_DATASESSION_CANCEL.name
                                putExtra(ServiceControl.PARAM.CANCEL_ID.name, cancelID)
                                putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, newResultReceiver(it))
                                startService(this)
                            }
                        }

                        loadSession(sessionID, index)
                    } catch (ex: Exception) {
                        RemoveSessionFailed_ShowSnackBar(ex)
                        return@launchUI
                    }
                }
            }

    private fun RemoveSessionFailed_ShowSnackBar(ex: Exception) {
        Snackbar
                .make(findViewById(R.id.fab), String.format(
                        getString(R.string.Cancel_session_remove_failed), ex.message),
                        Snackbar.LENGTH_LONG)
                .show()
    }

    private fun removeSession(sid: ISessionID?) {
        val toRemove = sessions.indexOfFirst { it.id == sid }
        if (toRemove >= 0)
            sessions.removeAt(toRemove)
    }

    private fun requestRemoveFromStorage(sessionID: ISessionID?, index: Int) {
        removeSession(sessionID)

        launchUI {
            val session_removed_rr = newResultReceiver(
                    object : ProductomerResultReceiver.ResultReceiverCallBack {
                        override fun onSuccess(data: Bundle?) {}
                        override fun onError(e: Exception?) = RemoveSessionFailed_ShowSnackBar(e!!)
                    })

            val res: Bundle?
            try {
                res = ProductomerResultReceiver.awaitCallback {
                    with(newServiceIntent()) {
                        action = ServiceControl.ACTIONS.REMOVE_DATASESSION.name
                        putExtra(ServiceControl.PARAM.SESSION_ID.name, sessionID)
                        putExtra(ServiceControl.PARAM.ON_OPERATION_FINISHED.name, session_removed_rr)
                        putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, newResultReceiver(it))
                        startService(this)
                    }
                }
            } catch (ex: Exception) {
                showLoadSessionError(ex)
                return@launchUI
            }

            val shaduledID = res.getResult<Int>()

            Log.i(LOG_TAG, "ShaduledID=$shaduledID : remove session $sessionID")

            val bar = Snackbar.make(findViewById(R.id.fab), R.string.Session_removed,
                    Snackbar.LENGTH_LONG)

            bar.setAction(android.R.string.cancel, newRemoveFromStorageCanceler(
                    shaduledID, sessionID, index, bar)).show()
        }
    }

    private fun ShowNewSessionPrompt() {
        SetListReplacement(getString(R.string.No_data_available))
    }

    private fun SetListReplacement(text: String?) {
        if (text == null || text.isEmpty()) {
            recyclerView.visibility = View.VISIBLE
            List_replacmentText.visibility = View.GONE
        } else {
            recyclerView.visibility = View.GONE
            List_replacmentText.text = text
            List_replacmentText.visibility = View.VISIBLE
        }
    }

    override fun onResume() {
        if (sessions.isEmpty()) {
            loadSessions()
        }
        super.onResume()
    }

    private fun newResultReceiver(ressiver: ProductomerResultReceiver.ResultReceiverCallBack): ProductomerResultReceiver {
        return ProductomerResultReceiver(Handler(mainLooper), ressiver)
    }

    private fun launchUI(block: suspend CoroutineScope.() -> Unit) = GlobalScope.launch(Dispatchers.Main, block = block)

    private fun loadSessions() = GlobalScope.launch(Dispatchers.IO) {
        val sessionlist = loadSessionList()
        sessionlist.forEach {
            GlobalScope.launch(Dispatchers.Default) { loadSession(it) }
        }
    }

    private fun showLoadSessionError(exception: Exception) {
        SetListReplacement(String.format(getString(R.string.storage_unaccessable_format),
                exception.message))
    }

    private suspend fun loadSession(sid: ISessionID?, index: Int = -1) {
        val res: Bundle?
        try {
            res = ProductomerResultReceiver.awaitCallback {
                with(newServiceIntent()) {
                    action = ServiceControl.ACTIONS.GET_DATASESSION.name
                    putExtra(ServiceControl.PARAM.SESSION_ID.name, sid)
                    putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, newResultReceiver(it))
                    startService(this)
                }
            }
        } catch (ex: Exception) {
            Log.e(LOG_TAG, "Failed to load session " + sid!!.toString(), ex)
            return
        }

        val session = res.getResult<ISession>()
        launchUI { addSession(session!!, index) }
    }

    private fun addSession(session: ISession, index: Int) {
        SetListReplacement("")

        session.OnSessionChanged = sessionUpdateListener

        if (index < 0) {
            sessions.add(session)
            adepter.notifyItemInserted(sessions.size - 1)
        } else {
            sessions.add(index, session)
            adepter.notifyItemInserted(index)
        }
    }

    companion object {
        private const val LOG_TAG = "MainActivity"
    }
}
