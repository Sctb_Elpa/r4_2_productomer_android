package ru.sctbelpa.R4_2_productomer.GUI

import android.content.Context
import android.content.res.Resources
import android.os.Handler
import android.os.Looper
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.YAxis
import ru.sctbelpa.R4_2_productomer.DataStorage.IResult
import ru.sctbelpa.R4_2_productomer.DataStorage.ISession
import ru.sctbelpa.R4_2_productomer.R
import ru.sctbelpa.R4_2_productomer.Utils.GraphData
import ru.sctbelpa.R4_2_productomer.Utils.millisToShortDHMS
import ru.sctbelpa.R4_2_productomer.Utils.resultValues
import ru.sctbelpa.R4_2_productomer.Utils.temperatureFormat
import java.text.SimpleDateFormat
import java.util.*

class SessionViewHolderBehavior(private val holder: SessionViewHolder) {

    val StartEditFieldCB: View.OnClickListener = View.OnClickListener { v ->
        if (isActivated) {
            setEditable(v as EditText, true)
        } else {
            holder.card.callOnClick()
        }
    }

    internal var isActivated: Boolean
        get() = holder.itemView.isActivated
        set(activated) = with(holder) {
            itemView.isActivated = activated

            val summary_visibility = if (activated) View.GONE else View.VISIBLE
            val minutely_visibility = if (activated) View.VISIBLE else View.GONE

            session_card_summary.visibility = summary_visibility
            session_card_extend.visibility = summary_visibility
            startDate.visibility = summary_visibility

            sessioncard_speciality_table.visibility = minutely_visibility

            sessioncard_edit_title.visibility = minutely_visibility
            sessioncard_edit_description.visibility = minutely_visibility
            session_card_collapse.visibility = minutely_visibility

            if (!activated) {
                setEditable(session_title, false)
                setEditable(session_card_description, false)

                hideKeyboard()
            } else {
                animateGraph()
            }
        }

    private fun hideKeyboard() {
        val imm = holder.card.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(holder.card.windowToken, 0)
    }

    // FIXME : async
    private fun animateGraph() {
        val timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                // вызывать это следует в GUI потоке
                Handler(Looper.getMainLooper()).post {
                    for (view in holder.chanelView) {
                        view.findViewById<LineChart>(R.id.graph).animateX(animationDuration)
                    }
                }
                cancel()
            }
        }, animationDelay.toLong())
    }

    private fun setupHeader(session: ISession, formatted_date: String, durationFormatted: String) = with(holder) {
        session_title.setText(session.Title)
        startDate.text = formatted_date
        session_card_description.setText(session.Description)

        session_card_speciality_date.text = formatted_date
        session_card_experiment_duration.text = durationFormatted
    }

    fun update(session: ISession, resources: Resources, context: Context) {

        holder.associatedSession = session

        val formatted_date = SimpleDateFormat.getDateInstance().format(session.date)
        val durationFormatted = millisToShortDHMS(session.SessionDuration)

        setupHeader(session, formatted_date, durationFormatted)

        val stringBuilder = StringBuilder()
        val locale = Locale.US
        val formatter = Formatter(stringBuilder, locale)
        val chanel = resources.getString(R.string.Chanel)
        val inflater = LayoutInflater.from(context)

        session.Results.forEachIndexed { i, v ->
            val textResults = resultValues(v, resources, locale)

            appendSumryString(formatter, chanel, i == session.Results.count() - 1, v, textResults)
            fillChanelDetails(i, resources, locale, chanel, inflater, v, textResults)
        }
        // если в этой карточке больше созданых результатов - удалить лишние
        removeUnusedViews(session)

        setCardSummary(stringBuilder.toString())

        prepare_edit_actions(holder.session_title, HEADER_ELEMENT)
        prepare_edit_actions(holder.session_card_description, DESCRIPTION_ELEMENT)
    }

    private fun removeUnusedViews(session: ISession) {
        if (session.Results.count() < holder.chanelView.count()) {
            (session.Results.count() until holder.chanelView.count())
                    .forEach {
                        val view = holder.chanelView[it]
                        holder.sessioncard_speciality_table.removeView(view)
                        holder.chanelView.removeAt(it)
                    }
        }
    }

    private fun fillChanelDetails(chanelelNumber: Int, resources: Resources, locale: Locale,
                                  chanel: String, inflater: LayoutInflater, res: IResult,
                                  results: Pair<String, String>) {

        val chanelInfo: View
        if (holder.chanelView.count() < chanelelNumber + 1) {
            chanelInfo = inflater.inflate(R.layout.chanel_info_chart, null)
            holder.chanelView.add(chanelInfo)
            appendChanelInfo(chanelInfo)
        } else {
            chanelInfo = holder.chanelView[chanelelNumber]
        }

        fillTables(chanelInfo, locale, chanel, res, results)

        val graph = chanelInfo.findViewById<LineChart>(R.id.graph)
        configureViewport(graph)

        // fill data
        graph.data = GraphData(res, resources, ROImin)

        // start draw когда анимация - не нужно
        //holder.graph.invalidate();
    }

    private fun prepare_edit_actions(editor: EditText, fieldID: Int) {
        editor.setOnClickListener(StartEditFieldCB)
        editor.onFocusChangeListener = getOnEditFinishedCB(fieldID)
    }

    private fun getOnEditFinishedCB(fieldID: Int): View.OnFocusChangeListener {
        return View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val editor = v as EditText
                setEditable(editor, false)

                val associatedSession = holder.associatedSession
                if (associatedSession != null) {
                    val text = editor.text.toString()

                    when (fieldID) {
                        HEADER_ELEMENT -> associatedSession.Title = text
                        DESCRIPTION_ELEMENT -> associatedSession.Description = text
                    }
                }
            }
        }
    }

    private fun setCardSummary(htmlcontent: String) {
        holder.session_card_summary.text = Html.fromHtml(htmlcontent)
    }

    private fun appendChanelInfo(chanelInfo: View) {
        val childCount = holder.sessioncard_speciality_table.childCount

        holder.sessioncard_speciality_table.addView(chanelInfo, childCount)
    }

    private fun fillTables(chanelInfo: View, locale: Locale, chanel: String, res: IResult,
                           results: Pair<String, String>) {

        val header = chanelInfo.findViewById<TextView>(R.id.chanel_table_header)
        header.text = String.format(locale, "%s %d", chanel, res.chanelNumber)

        val chanel_table_experiment_start_T = chanelInfo.findViewById<TextView>(R.id.chanel_table_experiment_start_T)
        chanel_table_experiment_start_T.text = String.format(locale, temperatureFormat, res.startTemperature)

        val chanel_table_experiment_end_T = chanelInfo.findViewById<TextView>(R.id.chanel_table_experiment_end_T)
        chanel_table_experiment_end_T.text = String.format(locale, temperatureFormat, res.endTemperature)

        val chanel_table_experiment_cristalisation_point = chanelInfo.findViewById<TextView>(R.id.chanel_table_experiment_cristalisation_point)
        chanel_table_experiment_cristalisation_point.text = results.first

        val chanel_table_experiment_aW_value = chanelInfo.findViewById<TextView>(R.id.aW_value)
        chanel_table_experiment_aW_value.text = results.second
    }

    private fun appendSumryString(formatter: Formatter, chanel: String, last: Boolean,
                                  res: IResult, results: Pair<String, String>) {
        formatter.format("<b>%s %d:</b> <tt>%s</tt>", chanel, res.chanelNumber, results.second)
        if (!last) {
            formatter.format("<br>")
        }
    }

    private fun configureViewport(graph: LineChart) {

        with(graph) {
            setDrawGridBackground(false)
            setDrawBorders(false)
            legend.isEnabled = false
            axisLeft.setDrawLabels(false)
            axisRight.setDrawLabels(false)
            xAxis.setDrawLabels(false)
            description.isEnabled = false
            setTouchEnabled(false)
        }

        with(graph.xAxis) {
            setDrawAxisLine(false)
            setDrawGridLines(false)
        }

        val YAxisConfigurator: YAxis.() -> Unit = {
            setDrawZeroLine(true)
            setDrawAxisLine(false)
            setDrawGridLines(false)
        }

        with(graph.axisLeft, YAxisConfigurator)
        with(graph.axisRight, YAxisConfigurator)
    }



    private fun setEditable(editor: EditText, editable: Boolean) {
        if (editable) {
            val context = editor.context

            with(editor) {
                isCursorVisible = true //enables the edit text which we disabled in layout file
                isFocusable = true
                isFocusableInTouchMode = true // без этого фокус не получить
                requestFocus()
            }

            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT) //shows keyboard

            editor.selectAll()
        } else {
            with(editor) {
                //setText(s);
                setSelection(0, 0)
                isCursorVisible = false
                isFocusable = false
            }
        }
    }

    fun setOnCardClickListener(onClickListener: View.OnClickListener) =
            holder.card.setOnClickListener(onClickListener)

    companion object {

        const val HEADER_ELEMENT = 0
        const val DESCRIPTION_ELEMENT = 1

        private const val ROImin = 10
        private const val LOG_TAG = "SessionViewHolderBh"
        private const val FORCE_MAX_ROI = false

        private const val animationDelay = 125
        private const val animationDuration = 750
    }
}
