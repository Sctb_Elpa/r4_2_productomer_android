package ru.sctbelpa.R4_2_productomer.GUI

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import ru.sctbelpa.R4_2_productomer.DataStorage.IRawValue
import ru.sctbelpa.R4_2_productomer.R
import java.util.*

open class DisplayFragment : Fragment() {

    private val temperatureFormat = "%.3f °C"

    protected lateinit var chart: LineChart
    protected lateinit var T1_Display: TextView
    protected lateinit var T2_Display: TextView

    lateinit var C1_enable: CheckBox
    lateinit var C2_enable: CheckBox

    protected lateinit var off: String

    protected lateinit var T1DataSet: LineDataSet
    protected lateinit var T2DataSet: LineDataSet

    protected val T1_chartData: MutableList<Entry> = mutableListOf(Entry())
    protected val T2_chartData: MutableList<Entry> = mutableListOf(Entry())

    protected lateinit var chanel_data_collection1: View
    protected lateinit var chanel_data_collection2: View

    protected val dateOffset = Date().time

    var ready = false
        protected set(value) {
            field = value
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val layout = inflater.inflate(R.layout.fragment_display, container, false)

        val chanels_info_container = layout.findViewById<LinearLayout>(R.id.chanels_info_container)

        chart = layout.findViewById(R.id.data_collecting_chart)

        chanel_data_collection1 = inflater.inflate(R.layout.chanel_data_collection, container, false)
        C1_enable = setCheckboxListener(chanel_data_collection1, 1)
        T1_Display = getChanelTDisplay(chanel_data_collection1)
        chanels_info_container.addView(chanel_data_collection1)

        chanel_data_collection2 = inflater.inflate(R.layout.chanel_data_collection, container, false)
        C2_enable = setCheckboxListener(chanel_data_collection2, 2)
        T2_Display = getChanelTDisplay(chanel_data_collection2)
        chanels_info_container.addView(chanel_data_collection2)

        off = getText(R.string.off).toString()

        prepareChart()
        createDataSets()

        return layout
    }

    private fun prepareChart() {

        with(chart) {
            isAutoScaleMinMaxEnabled = true // втонастройка по оси Y
            setDrawGridBackground(false)
            setDrawBorders(false)
            legend.isEnabled = false
            axisLeft.setDrawLabels(true)
            axisRight.setDrawLabels(false)
            xAxis.setDrawLabels(false)
            description.isEnabled = false
            setTouchEnabled(false)
        }

        with(chart.xAxis) {
            setDrawAxisLine(true)
            setDrawGridLines(true)
        }

        val YAxisConfigurator: YAxis.() -> Unit = {
            setDrawZeroLine(true)
            setDrawAxisLine(true)
            setDrawGridLines(true)
        }

        with(chart.axisLeft, YAxisConfigurator)
        with(chart.axisRight, YAxisConfigurator)
    }

    protected open fun createDataSets() {
        T1DataSet = lineDataSet(T1_chartData, "T1_chartData") {
            setDrawFilled(false)
            setDrawValues(false)
            setDrawCircles(false)
            mode = LineDataSet.Mode.LINEAR
            color = resources.getColor(R.color.line1_color)
            axisDependency = YAxis.AxisDependency.LEFT
        }
        T2DataSet = lineDataSet(T2_chartData, "T2_chartData") {
            setDrawFilled(false)
            setDrawValues(false)
            setDrawCircles(false)
            mode = LineDataSet.Mode.LINEAR
            color = resources.getColor(R.color.line2_color)
            axisDependency = YAxis.AxisDependency.LEFT
        }

        chart.data = LineData(T1DataSet, T2DataSet)
    }

    private fun getChanelTDisplay(chanel_data_collection: View): TextView =
            chanel_data_collection.findViewById(R.id.chanel_data_collect_header_temperature_display)!!

    private fun setCheckboxListener(parent: View, chanel: Int): CheckBox = with(
            parent.findViewById<CheckBox>(R.id.chanel_data_collect_header_checkbox)) {
        text = getString(R.string.chanel) + " " + chanel
        setOnCheckedChangeListener { buttonView, isChecked ->
            chanelEnabled(chanel, isChecked)
        }
        this
    }

    protected open fun chanelEnabled(chanel: Int, isEnabled: Boolean) {
        when (chanel) {
            1 -> {
                T1DataSet.isVisible = isEnabled
                if (!isEnabled)
                    T1_Display.text = off
            }
            2 -> {
                T2DataSet.isVisible = isEnabled
                if (!isEnabled)
                    T2_Display.text = off
            }
        }
        (activity as DataCollectionActivity)
                .configureEnabledChanels(arrayOf(C1_enable.isChecked, C2_enable.isChecked))
    }

    private fun splitDataByChannels(data: List<IRawValue>?): Map<Int, List<IRawValue>> {
        if (data.isNullOrEmpty())
            return emptyMap()

        return data.groupBy { it.chanelNumber }
    }

    private fun ms2Drawable(ms: Long): Float = (ms - dateOffset).toFloat()

    fun updateChannel(display: TextView, dataSet: MutableList<Entry>, data: List<IRawValue>) {
        if (!data.isEmpty()) {
            dataSet.clear()
            display.text = temperatureFormat.format(data.last().temperature)
            dataSet.addAll(data.map { Entry(ms2Drawable(it.timestamp.time), it.temperature) })
        } else {
            dataSet.add(Entry())
        }
    }

    fun dataUpdated(data: LinkedList<IRawValue>?) {
        if (ready && data != null) {
            // Клонирование нужно, иначе исходная коллекция может быть изменена и все крашится
            // Да, костыль.
            val d = splitDataByChannels(data.clone() as List<IRawValue>)

            if (C1_enable.isChecked && d.containsKey(1)) {
                updateChannel(T1_Display, T1_chartData, d[1]!!)
            }
            T1DataSet.notifyDataSetChanged()

            if (C2_enable.isChecked && d.containsKey(2)) {
                updateChannel(T2_Display, T2_chartData, d[2]!!)
            }
            T2DataSet.notifyDataSetChanged()

            // обновить график
            chart.data.notifyDataChanged()
            chart.notifyDataSetChanged()
            chart.invalidate()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        ready = true
    }

    override fun onDetach() {
        super.onDetach()
        ready = false
    }
}
