package ru.sctbelpa.R4_2_productomer.GUI

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_data_collection.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.sctbelpa.R4_2_ProductomerService.*
import ru.sctbelpa.R4_2_productomer.DataStorage.IRawValue
import ru.sctbelpa.R4_2_productomer.DataStorage.ISession
import ru.sctbelpa.R4_2_productomer.R
import java.util.*
import kotlin.random.Random

class DataCollectionActivity : AppCompatActivity() {

    private val promptFragment = DataCollectionPromptFragment()
    private val connectingFragment = DataCollectionConnectingFragment()
    private val errorFragment = DataCollectionErrorFragment()
    private val displayFragment = DisplayFragment()
    private val dataCollectingFragment = DataCollectingFragment()

    private var workfragment: DisplayFragment? = null

    private lateinit var start_data_collection_mi: MenuItem
    private lateinit var stop_data_collection_mi: MenuItem

    var currentSession: IWorkSessionID? = null

    private val watcherID: Int by lazy { Random.nextInt() }

    private val serviceStatusReceiver: ProductomerResultReceiver by lazy {
        newResultReceiver(
                object : ProductomerResultReceiver.ResultReceiverCallBack {
                    override fun onSuccess(data: Bundle?) {
                        val resType = data?.getInt(ServiceControl.PARAM.RESULT_TYPE.name)

                        when (resType) {
                            ServiceControl.RESULT_TYPE.STATE_CHANGED.ordinal -> {
                                val wsession = data.getResultKey<IWorkSessionID>(
                                        ServiceControl.PARAM.SESSION_ID.name)
                                val state = data.getResultKey<Int>(
                                        ServiceControl.PARAM.CONNECTION_STATE.name)

                                if (wsession != null) {
                                    // просто пропустить вызов, который пройдет при регистрации лисенера
                                    changeState(state!!, wsession)
                                }
                            }
                            ServiceControl.RESULT_TYPE.DATA_UPDATED.ordinal -> {
                                val wsession = data.getResultKey<IWorkSessionID>(
                                        ServiceControl.PARAM.SESSION_ID.name)
                                val rawdata = data.getResultKey<LinkedList<IRawValue>>(
                                        ServiceControl.PARAM.SESSION_DATA.name)

                                newDataReceived(wsession!!, rawdata)
                            }
                        }
                    }

                    override fun onError(e: Exception?) {}
                })
    }

    private fun newDataReceived(wsessionId: IWorkSessionID, data: LinkedList<IRawValue>?) {
        if (wsessionId == currentSession) {
            workfragment?.dataUpdated(data)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_collection)

        setSupportActionBar(toolbar)

        device_list.layoutManager = LinearLayoutManager(baseContext)
        with(BTListAdepter(baseContext)) {
            onDeviceSelected = object : BTListAdepter.IOnAdapterSelectedListener {
                override fun selected(id: IWorkSessionID) {
                    currentSession = id
                }
            }
            device_list.adapter = this
        }

        supportActionBar!!.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            var drawable = ResourcesCompat.getDrawable(resources,
                    ru.sctbelpa.R4_2_productomer.R.drawable.ic_menu, null)!!
            drawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(drawable, ContextCompat.getColor(this@DataCollectionActivity,
                    ru.sctbelpa.R4_2_productomer.R.color.abc_primary_text_material_dark))
            setHomeAsUpIndicator(drawable)
        }

        registerStateListener()
    }

    override fun onResume() {
        super.onResume()
        updateState()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        if (currentSession == null) {
            currentSession = savedInstanceState?.getSerializable(Session) as IWorkSessionID?
        }

        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterStateListener()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putSerializable(Session, currentSession)
    }

    private suspend fun findFirstCollectingDataSession(): IWorkSessionID? {
        val res: Bundle?
        try {
            res = ProductomerResultReceiver.awaitCallback {
                with(newServiceIntent()) {
                    action = ServiceControl.ACTIONS.FIRST_BUSY_SESSION.name
                    putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, newResultReceiver(it))
                    startService(this)
                }
            }
        } catch (ex: Exception) {
            Log.e(LOG_TAG, "Failed to perform findFirstCollectingDataSession request", ex)
            return null
        }
        return res.getResult<IWorkSessionID>()
    }

    private fun updateState() = GlobalScope.launch {
        currentSession = findFirstCollectingDataSession()

        // послать сервису "пинок", чтобы он дернул за колбэк обновления статуса
        with(newServiceIntent()) {
            action = ServiceControl.ACTIONS.FORCE_UPDATE_STATE.name
            startService(this)
        }
    }

    fun launchUI(block: suspend CoroutineScope.() -> Unit) = GlobalScope.launch(Dispatchers.Main, block = block)

    private fun changeState(state: Int, sessionID: IWorkSessionID?) {
        // close drawer
        drawer_layout.closeDrawers()

        if (!isFinishing && !isDestroyed) { // https://stackoverflow.com/a/38826864/8065921
            launchUI {
                val fragmentTransaction = supportFragmentManager.beginTransaction()

                if (currentSession == null) {
                    prepare(fragmentTransaction) // Первый запуск
                    updateMenu(IWorkSession.State.IDLE.ordinal)
                } else if (currentSession == sessionID) {
                    updateMenu(state)
                    when (state) {
                        IWorkSession.State.IDLE.ordinal -> Unit// сбор завершен, игнор
                        IWorkSession.State.CONNECTING.ordinal -> {
                            connectingFragment.target = sessionID!!.name
                            fragmentTransaction.replace(R.id.fragment_container, connectingFragment)
                        }
                        IWorkSession.State.ERROR.ordinal -> {
                            errorFragment.target = sessionID!!.name
                            fragmentTransaction.replace(R.id.fragment_container, errorFragment)
                        }
                        IWorkSession.State.DISPLAYING_DATA.ordinal -> {
                            fragmentTransaction.replace(R.id.fragment_container, displayFragment)
                            workfragment = displayFragment
                        }
                        IWorkSession.State.COLLECTING_DATA.ordinal -> {
                            fragmentTransaction.replace(R.id.fragment_container, dataCollectingFragment)
                            workfragment = dataCollectingFragment
                            dataCollectingFragment.updateControls(this@DataCollectionActivity, displayFragment)
                        }
                    }
                }
                // https://stackoverflow.com/a/10261438/8065921
                fragmentTransaction.commitAllowingStateLoss()
            }
        }
    }

    private fun updateMenu(state: Int) {
        when (state) {
            IWorkSession.State.IDLE.ordinal,
            IWorkSession.State.CONNECTING.ordinal,
            IWorkSession.State.ERROR.ordinal -> {
                start_data_collection_mi.isVisible = false
                stop_data_collection_mi.isVisible = false
            }
            IWorkSession.State.DISPLAYING_DATA.ordinal -> {
                start_data_collection_mi.isVisible = true
                stop_data_collection_mi.isVisible = false
            }
            IWorkSession.State.COLLECTING_DATA.ordinal -> {
                start_data_collection_mi.isVisible = false
                stop_data_collection_mi.isVisible = true
            }
        }
    }

    private fun prepare(fragmentTransaction: FragmentTransaction) {
        fragmentTransaction.replace(R.id.fragment_container, promptFragment)
    }

    override fun onNewIntent(intent: Intent?) {
        // Эта активити была переоткрыта, с новым интентом, но не пересоздана.
        super.onNewIntent(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.data_collect_menu, menu)

        with(menu!!) {
            start_data_collection_mi = findItem(R.id.start_data_collection_mi)!!
            stop_data_collection_mi = findItem(R.id.stop_data_collection_mi)!!
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        var called = super.onOptionsItemSelected(item)

        if (!called && item != null) {
            val id = item.itemId
            called = true
            when (id) {
                R.id.start_data_collection_mi -> startWorkSession()
                R.id.stop_data_collection_mi -> finishWorkSession()
                android.R.id.home -> drawer_layout.openDrawer(GravityCompat.START)
                else -> called = false
            }
        }

        return called
    }

    private fun startWorkSession() = launchUI {
        val res: Bundle?
        try {
            res = ProductomerResultReceiver.awaitCallback {
                with(newServiceIntent()) {
                    action = ServiceControl.ACTIONS.START_COLLECTING_DATA.name
                    putExtra(ServiceControl.PARAM.SESSION_ID.name, currentSession)
                    putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, newResultReceiver(it))
                    startService(this)
                }
            }
        } catch (ex: Exception) {
            Log.e(LOG_TAG, "Failed start collecting data", ex)
            return@launchUI
        }

        changeState(IWorkSession.State.COLLECTING_DATA.ordinal, res.getResult<IWorkSessionID>())
    }

    private suspend fun loadSession(sid: ISessionID?): ISession? {
        if (sid == null) {
            return null
        }

        val res: Bundle?
        try {
            res = ProductomerResultReceiver.awaitCallback {
                with(newServiceIntent()) {
                    action = ServiceControl.ACTIONS.GET_DATASESSION.name
                    putExtra(ServiceControl.PARAM.SESSION_ID.name, sid)
                    putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, newResultReceiver(it))
                    startService(this)
                }
            }
        } catch (ex: Exception) {
            Log.e(LOG_TAG, "Failed to load session $sid", ex)
            return null
        }

        return res.getResult()
    }

    private fun finishWorkSession() = launchUI {
        val bundle: Bundle?
        try {
            bundle = ProductomerResultReceiver.awaitCallback {
                with(newServiceIntent()) {
                    action = ServiceControl.ACTIONS.FINALISE_WORKSESSION.name
                    putExtra(ServiceControl.PARAM.SESSION_ID.name, currentSession)
                    putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, newResultReceiver(it))
                    startService(this)
                }
            }
        } catch (e: Exception) {
            collectingError_ShowSnackBar()
            return@launchUI
        }

        stop_data_collection_mi.isVisible = false

        val sessionID = bundle!!.getResult<ISessionID>()

        val dataSession = loadSession(sessionID)
        if (dataSession != null) {
            dataCollectingFragment.showResult(dataSession)
            collectingFinished_ShowSnackBar(dataSession.Title)
        } else {
            collectingError_ShowSnackBar()
        }
    }

    private fun collectingFinished_ShowSnackBar(title: String) {
        Snackbar
                .make(findViewById(R.id.drawer_layout),
                        getString(R.string.Session_finished).format(title), Snackbar.LENGTH_LONG)
                .show()
    }

    private fun collectingError_ShowSnackBar() {
        Snackbar
                .make(findViewById(R.id.drawer_layout), getString(R.string.DataCollectionFauiled),
                        Snackbar.LENGTH_LONG)
                .show()
    }

    fun newResultReceiver(receiver: ProductomerResultReceiver.ResultReceiverCallBack): ProductomerResultReceiver = ProductomerResultReceiver(Handler(mainLooper), receiver)

    fun newServiceIntent() = Intent(this@DataCollectionActivity,
            ru.sctbelpa.R4_2_ProductomerService.Service::class.java)

    private fun registerStateListener() = with(newServiceIntent()) {
        action = ServiceControl.ACTIONS.REGISTER_WATCHER.name
        putExtra(ServiceControl.PARAM.WATCHER_ID.name, watcherID)
        putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, serviceStatusReceiver)
        startService(this)
    }

    private fun unregisterStateListener() = with(newServiceIntent()) {
        action = ServiceControl.ACTIONS.UNREGISTER_WATCHER.name
        putExtra(ServiceControl.PARAM.WATCHER_ID.name, watcherID)
        startService(this)
    }

    fun configureEnabledChanels(config: Array<Boolean>) = with(newServiceIntent()) {
        action = ServiceControl.ACTIONS.CHANEL_CONTROL.name
        putExtra(ServiceControl.PARAM.SESSION_ID.name, currentSession)
        putExtra(ServiceControl.PARAM.CHANEL_ENABLE_STATE.name, config)
        startService(this)
    }

    override fun onPause() {
        super.onPause()

        finaliseDisplyingSessions()
    }

    private fun finaliseDisplyingSessions() = with(newServiceIntent()) {
        action = ServiceControl.ACTIONS.FINALISE_DISPLYING_SESSIONS.name
        startService(this)
    }

    companion object {
        private const val LOG_TAG = "DataCollectionActivity"
        private const val Session = "Session"
    }
}
