package ru.sctbelpa.R4_2_productomer.GUI

import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet

fun lineDataSet(yVals: List<Entry>, label: String, lambda: LineDataSet.() -> Unit): LineDataSet = with(LineDataSet(yVals, label)) {
    lambda()
    this
}
