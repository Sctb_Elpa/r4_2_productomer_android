package ru.sctbelpa.R4_2_productomer.GUI

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.sctbelpa.R4_2_productomer.R

class DataCollectionErrorFragment : Fragment() {
    private var text: TextView? = null
    private var atached = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val layout = inflater.inflate(R.layout.fragment_error_connecting, container, false)
        text = layout.findViewById(R.id.device_text)
        setText()
        return layout
    }

    var target: String = ""
        set(value) {
            field = value
            setText()
        }

    private fun setText() {
        if (atached) {
            text?.text = getString(R.string.device_error_text).format(target)
        }
    }

    override fun onAttach(activity: Activity?) {
        atached = true
        super.onAttach(activity)
    }

    override fun onDetach() {
        atached = false
        super.onDetach()
    }
}