package ru.sctbelpa.R4_2_productomer.GUI

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.sctbelpa.R4_2_ProductomerService.ProductomerResultReceiver
import ru.sctbelpa.R4_2_ProductomerService.ServiceControl
import ru.sctbelpa.R4_2_ProductomerService.getResult
import ru.sctbelpa.R4_2_productomer.DataStorage.IResult
import ru.sctbelpa.R4_2_productomer.DataStorage.ISession
import ru.sctbelpa.R4_2_productomer.R
import ru.sctbelpa.R4_2_productomer.Utils.graphMarkers
import ru.sctbelpa.R4_2_productomer.Utils.resultValues
import ru.sctbelpa.R4_2_productomer.Utils.selectROI
import java.util.*

class DataCollectingFragment : DisplayFragment() {

    private var _C1_savedEnableState: Boolean = false
    private var _C2_savedEnableState: Boolean = false

    private var C1_savedEnableState
        get() = _C1_savedEnableState
        set(value) {
            _C1_savedEnableState = value
            if (ready) {
                C1_enable.isChecked = value
                if (value) {
                    chart.data.addDataSet(T1DataSet)
                }
            }
        }

    private var C2_savedEnableState
        get() = _C2_savedEnableState
        set(value) {
            _C2_savedEnableState = value
            if (ready) {
                C2_enable.isChecked = value
                if (value) {
                    chart.data.addDataSet(T2DataSet)
                }
            }
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val res = super.onCreateView(inflater, container, savedInstanceState)

        C1_enable.isEnabled = false
        C2_enable.isEnabled = false

        return res
    }

    // вызывается после onCreateView()
    override fun onResume() {
        super.onResume()

        C1_enable.isChecked = C1_savedEnableState
        C2_enable.isChecked = C2_savedEnableState
    }

    // вызывается ДО onCreateView()
    //override fun onAttach(context: Context?)

    fun showResult(session: ISession) {
        // сюда придет уже сесся, отобразить её

        val locale = Locale.US

        val ROIs = session.Results.mapIndexed { i, res ->
            val textResults = resultValues(res, resources, locale)

            fillChanelDetails(locale, i, res, textResults)
            redrawChart(i, res)
        }
        chart.apply {
            val ROI = ROIs.combine()

            xAxis.apply {
                axisMinimum = (ROI.first - dateOffset).toFloat()
                axisMaximum = (ROI.second - dateOffset).toFloat()
            }

            data.notifyDataChanged()
            invalidate()
        }
    }

    private fun <T : Comparable<T>, U : Comparable<U>> Collection<Pair<T, U>>.combine(): Pair<T, U> {
        val min = this.map { it.first }.min()!!
        val max = this.map { it.second }.max()!!
        return Pair(min, max)
    }

    private fun redrawChart(chanelIndex: Int, res: IResult): Pair<Long, Long> {
        return try {
            val ROI = selectROI(res.rawData, res.resultTimestamp, ROItailsMax)
            when (chanelIndex) {
                0 -> {
                    graphMarkers(ROI, res, dateOffset, res.resultTimestamp, resources, chart.data, T1DataSet.color)
                }
                1 -> {
                    graphMarkers(ROI, res, dateOffset, res.resultTimestamp, resources, chart.data, T2DataSet.color)
                }
                else -> throw Exception()
            }
            ROI
        } catch (ex: Exception) {
            // incomplete data - ignore exception
            Pair(res.rawData.first().timestamp.time, res.rawData.last().timestamp.time)
        }
    }

    private fun fillChanelDetails(locale: Locale, chanelNumber: Int, res: IResult,
                                  textResults: Pair<String, String>) {

        val chanelTable = when (chanelNumber) {
            0 -> {
                T1_Display.visibility = View.GONE
                chanel_data_collection1.findViewById<View>(R.id.chanel_data_collect_results)
            }
            1 -> {
                T2_Display.visibility = View.GONE
                chanel_data_collection2.findViewById<View>(R.id.chanel_data_collect_results)
            }
            else -> throw Exception()
        }
        chanelTable.findViewById<TextView>(R.id.chanel_table_experiment_start_T).text =
                String.format(locale, ru.sctbelpa.R4_2_productomer.Utils.temperatureFormat, res.startTemperature)
        chanelTable.findViewById<TextView>(R.id.chanel_table_experiment_end_T).text =
                String.format(locale, ru.sctbelpa.R4_2_productomer.Utils.temperatureFormat, res.endTemperature)
        chanelTable.findViewById<TextView>(R.id.chanel_table_experiment_cristalisation_point).text =
                textResults.first
        chanelTable.findViewById<TextView>(R.id.aW_value).text = textResults.second

        chanelTable.visibility = View.VISIBLE
    }

    // возможно, этот мето вызывается при восстановлении, тогда, разумеется референса нет, придется загружать
    // инфу из сессии.
    fun updateControls(parent: DataCollectionActivity, display: DisplayFragment) {
        if (display.ready) {
            // разрешение работы каналов
            _C1_savedEnableState = display.C1_enable.isChecked
            _C2_savedEnableState = display.C2_enable.isChecked
        } else {
            getChannelsEnabledFromSession(parent)
        }
    }

    private fun getChannelsEnabledFromSession(parent: DataCollectionActivity) {
        parent.launchUI {
            val res = ProductomerResultReceiver.awaitCallback {
                with(parent.newServiceIntent()) {
                    action = ServiceControl.ACTIONS.GET_WORK_SESSION_ENABLED_CHANNELS.name
                    putExtra(ServiceControl.PARAM.SESSION_ID.name, parent.currentSession)
                    putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, parent.newResultReceiver(it))
                    parent.startService(this)
                }
            }
            val enabled = res.getResult<Array<Boolean>>()!!
            C1_savedEnableState = enabled[0]
            C2_savedEnableState = enabled[1]
        }
    }

    override fun createDataSets() {
        super.createDataSets()

        if (!C1_savedEnableState) {
            chart.data.removeDataSet(T1DataSet)
        }

        if (!C2_savedEnableState) {
            chart.data.removeDataSet(T2DataSet)
        }
    }

    override fun chanelEnabled(chanel: Int, isEnabled: Boolean) {
        when (chanel) {
            1 -> {
                T1DataSet.isVisible = isEnabled
                if (!isEnabled)
                    T1_Display.text = off
            }
            2 -> {
                T2DataSet.isVisible = isEnabled
                if (!isEnabled)
                    T2_Display.text = off
            }
        }
        // исключить, то что в паренте тут делается
    }

    companion object {
        private const val ROItailsMax = 10
    }

}
