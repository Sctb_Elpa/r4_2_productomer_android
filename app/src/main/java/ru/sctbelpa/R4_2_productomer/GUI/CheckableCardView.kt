package ru.sctbelpa.R4_2_productomer.GUI

import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import android.widget.Checkable

class CheckableCardView @JvmOverloads constructor
(context: Context, attributes: AttributeSet? = null, defStyleAttr: Int = 0)
    : CardView(context, attributes, defStyleAttr), Checkable {

    private companion object {
        val CHECKED_STATE_SET = IntArray(1).apply { this[0] = android.R.attr.state_checked }
    }

    private var m_isChecked = true

    override fun isChecked(): Boolean = m_isChecked

    override fun toggle() {
        isChecked = !isChecked
    }

    override fun setChecked(checked: Boolean) {
        m_isChecked = checked
    }

    override fun onCreateDrawableState(extraSpace: Int): IntArray {
        val drawableState = super.onCreateDrawableState(extraSpace + 1)
        if (isChecked) {
            View.mergeDrawableStates(drawableState, CHECKED_STATE_SET)
        }
        return drawableState
    }
}