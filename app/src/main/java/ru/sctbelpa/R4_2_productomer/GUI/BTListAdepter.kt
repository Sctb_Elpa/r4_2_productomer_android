package ru.sctbelpa.R4_2_productomer.GUI

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.sctbelpa.R4_2_ProductomerService.IWorkSessionID
import ru.sctbelpa.R4_2_ProductomerService.ProductomerResultReceiver
import ru.sctbelpa.R4_2_ProductomerService.Service
import ru.sctbelpa.R4_2_ProductomerService.ServiceControl
import ru.sctbelpa.R4_2_productomer.R

class BTListAdepter(private val context: Context) : RecyclerView.Adapter<BTListAdepter.DeviceViewHolder>() {

    companion object {
        const val CARD_INVALID = 0
        const val CARD_NORMAL = 1
    }

    interface IOnAdapterSelectedListener {
        fun selected(id: IWorkSessionID)
    }

    var onDeviceSelected: IOnAdapterSelectedListener? = null

    class DeviceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val card = itemView.findViewById<CardView>(R.id.device_item)
        val device_name = itemView.findViewById<TextView>(R.id.device_name)
        val device_MAC = itemView.findViewById<TextView>(R.id.device_status_descr)

        fun setOnCardClickListener(listener: View.OnClickListener) = card.setOnClickListener(listener)
    }

    private var sessionlist: List<IWorkSessionID> = emptyList()
        get() {
            if (field.isEmpty()) {
                startUpdateSessions()
            }
            return field
        }
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private fun newResultReceiver(ressiver: ProductomerResultReceiver.ResultReceiverCallBack) = ProductomerResultReceiver(Handler(context.mainLooper), ressiver)

    private fun startUpdateSessions() {
        val rr = newResultReceiver(
                object : ProductomerResultReceiver.ResultReceiverCallBack {
                    override fun onSuccess(data: Bundle?) {
                        val sessions = data?.getSerializable(
                                ProductomerResultReceiver.PARAM_RESULT) ?: return
                        sessionlist = sessions as ArrayList<IWorkSessionID>
                    }

                    override fun onError(e: Exception?) {}
                })

        val intent = with(Intent(context, Service::class.java)) {
            action = ServiceControl.ACTIONS.WORK_SESSION_ID_LIST.name
            putExtra(ServiceControl.PARAM.RESULT_RECEIVER.name, rr)
        }

        context.startService(intent)
    }

    override fun getItemViewType(position: Int): Int {
        /* TODO
        // тут тупро веряем название
        val s = sessionlist.getOrElse(position) { return CARD_INVALID }
        return if (s.device.name == "SCTB_R4-2_productomer") CARD_NORMAL else CARD_INVALID
        */
        return CARD_NORMAL
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceViewHolder {
        val layout = if (viewType == CARD_NORMAL)
            R.layout.device_item_correct
        else
            R.layout.device_item_incorrect

        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)!!
        return BTListAdepter.DeviceViewHolder(view)
    }

    override fun getItemCount(): Int = sessionlist.count()

    override fun onBindViewHolder(holder: DeviceViewHolder, position: Int) {
        val s = sessionlist.getOrNull(position) ?: return

        with(holder) {
            device_name.text = s.name
            device_MAC.text = s.identity
        }

        with(BTListBehavior(holder, s)) {
            onWorkSessionSelected = object : BTListBehavior.OnWorkSessionSelectedListener {
                override fun selected(sessionID: IWorkSessionID) {
                    with(Intent(context,
                            ru.sctbelpa.R4_2_ProductomerService.Service::class.java)) {
                        action = ServiceControl.ACTIONS.ACTIVATE_WORK_SESSION.name
                        putExtra(ServiceControl.PARAM.SESSION_ID.name, sessionID)
                        // Рессивера нет, ибо все изменения вернутся в GUI через зарегистрированный
                        // активностью коллбэк
                        context.startService(this)
                    }

                    onDeviceSelected?.selected(sessionID)
                }
            }
        }
    }
}
