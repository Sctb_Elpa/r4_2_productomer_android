package ru.sctbelpa.R4_2_productomer.GUI

import android.view.View
import ru.sctbelpa.R4_2_ProductomerService.IWorkSessionID

class BTListBehavior(val holder: BTListAdepter.DeviceViewHolder,
                     private val sessionID: IWorkSessionID) {

    interface OnWorkSessionSelectedListener {
        fun selected(sessionID: IWorkSessionID)
    }

    var onWorkSessionSelected: OnWorkSessionSelectedListener? = null

    init {
        holder.setOnCardClickListener(View.OnClickListener {
            if (isCompatible()) {
                onWorkSessionSelected?.selected(sessionID)
            }
        })
    }

    private fun isCompatible() = holder.card.isClickable
}