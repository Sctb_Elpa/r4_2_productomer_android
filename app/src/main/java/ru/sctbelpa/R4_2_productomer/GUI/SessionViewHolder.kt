package ru.sctbelpa.R4_2_productomer.GUI

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.*
import ru.sctbelpa.R4_2_productomer.DataStorage.ISession
import ru.sctbelpa.R4_2_productomer.R

// класс содержит указатели на все изменяемые поля макета
class SessionViewHolder(cv: View) : RecyclerView.ViewHolder(cv) {
    val card: CardView = cv.findViewById(R.id.SessionCard)
    val startDate: TextView = card.findViewById(R.id.session_card_title_date)
    val session_title: EditText = card.findViewById(R.id.session_card_title)
    val session_card_description: EditText = card.findViewById(R.id.session_card_description)
    val session_card_summary: TextView = card.findViewById(R.id.session_card_summary)
    val session_card_extend: ImageView = card.findViewById(R.id.session_card_extend)

    val sessioncard_speciality_table: TableLayout = card.findViewById(R.id.sessioncard_speciality_table)
    val session_card_speciality_date: TextView = card.findViewById(R.id.session_card_speciality_date)
    val session_card_experiment_duration: TextView = card.findViewById(R.id.session_card_experiment_duration)

    val sessioncard_edit_title: ImageButton = card.findViewById(R.id.sessioncard_edit_title)
    val sessioncard_edit_description: ImageButton = card.findViewById(R.id.sessioncard_edit_description)
    val session_card_collapse: ImageView = card.findViewById(R.id.session_card_collapse)

    var associatedSession: ISession? = null

    val chanelView: MutableList<View> = mutableListOf()
}
