package ru.sctbelpa.R4_2_productomer.DataStorage

import android.os.Environment
import android.util.Log
import ru.sctbelpa.R4_2_ProductomerService.ISessionID
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CSVDataStorage @JvmOverloads constructor(
        private var StorageDirectory: String,
        private var Readonly: Boolean = false) : IDataStorage {

    private val LOG_TAG = "CSVDataStorage"
    private val DefaultStorageDir = "Productomer"
    private val CSV_EXTENSION = ".csv"

    @Throws(IOException::class)
    constructor() : this("", false) {
        if (!isExternalStorageAvailable()) {
            Log.e(LOG_TAG, "No external storage avalable on device, cant' use _CSVDataStorage")
            throw IOException()
        }
        Readonly = isExternalStorageReadOnly()
        if (Readonly) {
            Log.w(LOG_TAG, "External storage read only, write not possibe")
        }

        this.StorageDirectory = File(Environment.getExternalStorageDirectory(),
                DefaultStorageDir).toString()
    }

    override fun getSessionCount(): Int = getStoredFiles().size

    override fun getSessionList(): ArrayList<ISessionID> =
            ArrayList(getStoredFiles()
                    .map { FileSessionID(it) })

    override fun GetSessionByID(i: Int): ISession {
        val files = getStoredFiles()
        if (i < 0 || i >= files.size) {
            throw IndexOutOfBoundsException("No session with ID = $i")
        }
        val filename = files[i]

        return ParseSessionFile(filename).apply { simplfydata() }
    }

    override fun GetSessionByID(id: ISessionID?): ISession? {
        if (id is FileSessionID) {
            val session = ParseSessionFile(id.sessionFileName).apply { simplfydata() }
            session.id = id
            return session
        } else {
            val name = id?.javaClass?.name
            throw IOException("Unsupported id type $name")
        }
    }

    @Throws(IllegalAccessException::class, NullPointerException::class)
    override fun SaveSession(session: ISession?): ISessionID? {
        if (Readonly) {
            throw IllegalAccessException("Internal storage is readonly")
        }

        if (session == null) {
            throw NullPointerException("DataSession has empty ID")
        }

        val file: File
        var id = session.id
        if (id !is FileSessionID) {
            // new session
            val safeFilename = getSafeFilenameForSession(session)
            file = File(StorageDirectory, safeFilename)
            id = FileSessionID(file.absolutePath)
        } else {
            // rewrite existing session
            val safeFilename = id.sessionFileName
            file = File(StorageDirectory, getSafeFilenameForSession(session))
            val newName = file.absolutePath
            id = FileSessionID(newName)

            if (newName != safeFilename) {
                val oldfile = File(safeFilename)
                if (oldfile.exists()) {
                    if (!oldfile.delete()) {
                        Log.e(LOG_TAG, "Failed to remove file $safeFilename")
                    }
                }
            }
        }

        val fileOutputStream = FileOutputStream(file)
        val csvSessionSerializer = CSVSessionSerializer(fileOutputStream)

        csvSessionSerializer.serialise(session)
        fileOutputStream.close()

        return id
    }

    override fun close() {}

    override fun removeSession(id: ISessionID?) {
        if (id is FileSessionID) {
            if (Readonly) {
                throw IOException("Storage not writable")
            }

            val safeFilename = id.sessionFileName

            val file = File(safeFilename)

            if (!file.exists()) {
                throw IOException(String.format("File '%s' not exists", safeFilename))
            }

            if (!file.delete()) {
                throw IOException(String.format("Can't delete file '%s'", safeFilename))
            }
        } else {
            val name = id?.javaClass?.name
            throw IOException("Unsupported id type $name")
        }
    }

    private fun getStoredFiles(): Array<String> {
        val storageDir: File
        try {
            storageDir = getStorageDirectory()
        } catch (ex: IOException) {
            Log.e(LOG_TAG, ex.message)
            return arrayOf()
        }

        return storageDir
                .listFiles { pathname ->
                    if (!pathname.exists()) {
                        return@listFiles false
                    }

                    val name = pathname.name
                    val indexOfDot = name.lastIndexOf(".")
                    if (indexOfDot < 0) {
                        return@listFiles false
                    }
                    val ext = name.substring(indexOfDot)
                    return@listFiles ext == CSV_EXTENSION
                }
                .map { it.path!! }
                .toTypedArray()
    }

    @Throws(IOException::class)
    private fun getStorageDirectory(): File {
        val storageDir = File(StorageDirectory)
        if (!storageDir.exists()) {
            if (Readonly) {
                throw IOException("Storage not exists and can't be created")
            }
            initStorage(storageDir)
        }
        if (!storageDir.isDirectory) {
            if (Readonly) {
                throw IOException("Storage is file can't create, readonly storage")
            }
            if (!storageDir.delete()) { // delete file
                throw IOException("Can't delete invalid file")
            }

            // create directory
            initStorage(storageDir)
        }

        return storageDir
    }

    @Throws(IOException::class)
    private fun initStorage(storage: File) {
        if (!storage.mkdir()) {
            Readonly = true
            throw IOException("Can't create storage directory")
        }
    }

    @Throws(IOException::class, ParseException::class)
    private fun ParseSessionFile(filename: String): ISession {
        val fileInputStream = FileInputStream(filename)
        val parser = CSVSessionParser(fileInputStream)

        val start = System.nanoTime()
        val res = parser.parse()
        val toock = (System.nanoTime() - start) / 1000000
        Log.i(LOG_TAG, "Parse file $filename took $toock ms")

        return res
    }

}

internal fun isExternalStorageAvailable(): Boolean {
    val extStorageState = Environment.getExternalStorageState()
    return Environment.MEDIA_MOUNTED == extStorageState
}

internal fun isExternalStorageReadOnly(): Boolean {
    val extStorageState = Environment.getExternalStorageState()
    return Environment.MEDIA_MOUNTED_READ_ONLY == extStorageState
}

internal fun getSafeFilenameForSession(session: ISession): String {
    val dateformatter = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
    val title = session.Title
    val re = "[\\\\/:\"*?<>| ]".toRegex()

    val name = if (title.isEmpty())
        "Unnamed"
    else
        title.replace(re, "_")

    return "${name}_${dateformatter.format(session.date)}.csv"
}