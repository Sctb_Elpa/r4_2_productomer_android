package ru.sctbelpa.R4_2_productomer.DataStorage

import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class CSVgeneratorConfig {
    val endline = System.getProperty("line.separator");

    private val defaultCSVSplitter = ';'
    private val defaultNoResultsAvailable = "Некоректная измерительная сессия, результат отсутствует"

    private var m_TitleFormat = "Сессия%c%s"
    private var m_DescriptionFormat = "Описание%c%s"
    private var m_dateFormat = "Дата проведения%c%s"
    private var m_durationformat = "Продолжительность эксперимента [с]%c%.1g"
    private var m_ChanelIDFormat = "Канал %d"
    private var m_TresultFormat = "Температура в точке перегиба [°С]%c%f"
    private var m_aWResultFormat = "Активность воды (aW)%c%g"
    private var m_dataTableHeader = "Штамп времени;Канал;Температура [°С];Частота [Гц]"
    private var m_RawValueFormat = "%s;%d;%f;%f"

    //----------------------------------------------------------------------------------------------

    var TitleFormat: String
        get() = m_TitleFormat + endline
        set(value) {
            m_TitleFormat = value
        }

    val TitleParser: Pattern by lazy { Pattern.compile(transformPrintfPatternToRegEx(m_TitleFormat)) }

    var DescriptionFormat: String
        get() = m_DescriptionFormat + endline
        set(value) {
            m_DescriptionFormat = value
        }

    val DescriptionParser: Pattern by lazy { Pattern.compile(transformPrintfPatternToRegEx(m_DescriptionFormat)) }

    var dateFormat: String
        get() = m_dateFormat + endline
        set(value) {
            m_dateFormat = value
        }

    val dateParser: Pattern by lazy { Pattern.compile(transformPrintfPatternToRegEx(m_dateFormat)) }

    var durationformat: String
        get() = m_durationformat + endline
        set(value) {
            m_durationformat = value
        }

    val durationParser: Pattern by lazy {
        Pattern.compile(transformPrintfPatternToRegEx(m_durationformat)
                .replace("%.1g", "([-0-9 .,]+)"))
    }

    var ResultsFormat = "Результаты:"

    var ChanelIDFormat: String
        get() = m_ChanelIDFormat + endline
        set(value) {
            m_ChanelIDFormat = value
        }

    val ChanelIDParser: String
        get() = transformPrintfPatternToRegEx(m_ChanelIDFormat)

    var TresultFormat: String
        get() = m_TresultFormat + endline
        set(value) {
            m_TresultFormat = value
        }

    var aWResultFormat: String
        get() = m_aWResultFormat + endline
        set(value) {
            m_aWResultFormat = value
        }

    var experimentDataFormat = "Экспериментальные данные:"

    var dataTableHeaderFormat = "Штамп времени;Канал;Температура [°С];Частота [Гц]"

    var RawValueFormat: String
        get() = m_RawValueFormat + endline
        set(value) {
            m_RawValueFormat = value
        }

    val RawValueParser: Pattern by lazy {
        Pattern.compile(transformPrintfPatternToRegEx(m_RawValueFormat)
                .replace("%f", "([-0-9 .,]+|NaN)"))
    }

    //----------------------------------------------------------------------------------------------

    var CVSplitter = defaultCSVSplitter
    var dateFormatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault())
    var noResultAvailable = defaultNoResultsAvailable

    //----------------------------------------------------------------------------------------------

    private fun transformPrintfPatternToRegEx(pattern: String): String {
        return pattern
                .replace("%c", CVSplitter.toString())
                .replace("%s", "(.+)")
                .replace("%d", "(\\d+)")
                .replace("[", "\\[")
                .replace("]", "\\]")
    }

}