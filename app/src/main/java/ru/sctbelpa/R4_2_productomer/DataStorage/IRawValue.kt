package ru.sctbelpa.R4_2_productomer.DataStorage

import java.io.Serializable
import java.util.*

interface IRawValue : Serializable {
    val chanelNumber: Int
    val timestamp: Date
    val temperature: Float
    val ft: Float
}
