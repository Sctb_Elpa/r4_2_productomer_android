package ru.sctbelpa.R4_2_productomer.DataStorage

import java.io.OutputStream
import java.lang.Exception


class CSVSessionSerializer @JvmOverloads constructor(
        private val ostream: OutputStream,
        var config: CSVgeneratorConfig = CSVgeneratorConfig()) {

    fun serialise(session: ISession) {
        buildcsv(ostream, config) {
            title(session.Title)
            blankLine()
            description(session.Description)
            date(session.date)
            duration(session.SessionDuration)
            blankLine()
            results {
                for (res in session.Results) {
                    chanelnumber(res.chanelNumber)
                    try {
                        tresult(res.tresult)
                        aW(res.aw)
                    } catch (e: Exception) {
                        noResult()
                    }
                    blankLine()
                }
            }
            blankLine()
            experimantaldata {
                experimentalTableHeader()
                for (rw in session.RawData) {
                    experimentalResult(rw)
                }
            }
        }
    }

    companion object {
        const val LOG_TAG = "CSVSessionSerializer"
    }
}

