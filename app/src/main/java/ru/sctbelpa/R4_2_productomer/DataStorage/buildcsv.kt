package ru.sctbelpa.R4_2_productomer.DataStorage

import ru.sctbelpa.R4_2_productomer.Utils.ms2S
import java.io.OutputStream
import java.io.PrintWriter
import java.util.*

class CSVFormatContext(ostream: OutputStream, val config: CSVgeneratorConfig) {
    val printer: PrintWriter = PrintWriter(ostream)
}

inline fun buildcsv(ostream: OutputStream, config: CSVgeneratorConfig,
                    lambda: CSVFormatContext.() -> Unit) {
    CSVFormatContext(ostream, config).apply {
        lambda()
        printer.flush()
    }
}

fun CSVFormatContext.title(title: String) =
        printer.printf(config.TitleFormat, config.CVSplitter, title)

fun CSVFormatContext.blankLine() = printer.println()

fun CSVFormatContext.description(description: String) =
        printer.printf(config.DescriptionFormat, config.CVSplitter, description)

fun CSVFormatContext.date(date: Date) =
        printer.printf(config.dateFormat, config.CVSplitter, config.dateFormatter.format(date))

fun CSVFormatContext.duration(duration: Long) =
        printer.printf(config.durationformat, config.CVSplitter, ms2S(duration))

inline fun CSVFormatContext.results(lambda: CSVFormatContext.() -> Unit) {
    printer.println(config.ResultsFormat)
    lambda()
}

fun CSVFormatContext.chanelnumber(chanel: Int) = printer.printf(config.ChanelIDFormat, chanel)

fun CSVFormatContext.tresult(T: Float) = printer.printf(config.TresultFormat, config.CVSplitter, T)

fun CSVFormatContext.aW(aW: Float) = printer.printf(config.aWResultFormat, config.CVSplitter, aW)

inline fun CSVFormatContext.experimantaldata(lambda: CSVFormatContext.() -> Unit) {
    printer.println(config.experimentDataFormat)
    lambda()
}

fun CSVFormatContext.experimentalTableHeader() = printer.println(config.dataTableHeaderFormat)

fun CSVFormatContext.experimentalResult(rw: IRawValue) =
        printer.printf(config.RawValueFormat,
                config.dateFormatter.format(rw.timestamp),
                rw.chanelNumber,
                rw.temperature,
                rw.ft)

fun CSVFormatContext.noResult() = printer.println(config.noResultAvailable)