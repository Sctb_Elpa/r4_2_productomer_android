package ru.sctbelpa.R4_2_productomer.DataStorage

import ru.sctbelpa.R4_2_ProductomerService.ISessionID
import java.io.IOError
import java.io.IOException
import java.text.ParseException

class MOCDataStorage : IDataStorage {
    @Throws(IOError::class)
    override fun getSessionCount(): Int = 0

    @Throws(IOException::class, ParseException::class)
    override fun GetSessionByID(i: Int): ISession {
        throw IndexOutOfBoundsException("No session with id $i")
    }

    override fun close() {}

    @Throws(IOException::class)
    override fun removeSession(id: ISessionID?) {
    }

    @Throws(IOException::class)
    override fun getSessionList(): ArrayList<ISessionID> = ArrayList(emptyList())

    @Throws(IOException::class, ParseException::class)
    override fun GetSessionByID(id: ISessionID?): ISession? = null

    @Throws(IllegalAccessException::class, IOException::class)
    override fun SaveSession(session: ISession?): ISessionID? = null
}
