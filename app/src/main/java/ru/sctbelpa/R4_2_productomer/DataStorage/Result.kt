package ru.sctbelpa.R4_2_productomer.DataStorage

import ru.sctbelpa.libsmoothspline.Point
import ru.sctbelpa.libsmoothspline.SmoothSpline
import java.util.*

class Result(override val chanelNumber: Int) : IResult {

    internal data class ResultHolder(val offset: Double) {
        val spline = SmoothSpline()
        var result = Point()
        var falseKinksResults: List<Point> = emptyList()
    }

    companion object {
        private const val pg = 10.0
        private const val SPLINE_POINTS_COUNT = 50
        private const val Celsuis2KelvinDiff = 273.16F
    }

    private var result: ResultHolder? = null
    override var rawData: List<IRawValue> = emptyList()

    override val startTemperature: Float
        get() = if (rawData.isEmpty()) Float.NaN else rawData.first().temperature

    override val endTemperature: Float
        get() = if (rawData.isEmpty()) Float.NaN else rawData.last().temperature

    @get:Throws(IllegalStateException::class)
    override val tresult: Float
        get() {
            EnsireResultReady()
            return result!!.result.Y.toFloat()
        }

    @get:Throws(IllegalStateException::class)
    override val resultTimestamp: Long
        get() {
            EnsireResultReady()
            return result!!.result.X.toLong()
        }

    @get:Throws(IllegalStateException::class)
    override val aw: Float by lazy {
        val t = (tresult + Celsuis2KelvinDiff).toDouble()
        val r1 = 4.579 * Math.log(t) - 27.622
        val r2 = r1 + 528.373 / t

        return@lazy Math.exp(r2).toFloat()
    }

    @get:Throws(IllegalStateException::class)
    override val falseKinksResults: List<Point>
        get() {
            EnsireResultReady()
            return result?.falseKinksResults!!
        }

    private fun splineData(ssMethod: SmoothSpline.(v: Double) -> Double): List<Point> {
        EnsireResultReady()

        val offset = result!!.offset
        val spline = result!!.spline

        val range = rawData.last().timestamp.time - rawData.first().timestamp.time

        val step = ms2S(range / SPLINE_POINTS_COUNT.toDouble())

        return generateSequence(Point(0.0, spline.ssMethod(0.0))) {
            val timestamp = it.X + step
            return@generateSequence Point(timestamp, spline.ssMethod(timestamp))
        }
                .take(SPLINE_POINTS_COUNT)
                .map { Point(S2ms(it.X + offset), it.Y) }
                .toList()
    }

    private fun splineData(ROI: Pair<Long, Long>,
                           ssMethod: SmoothSpline.(v: Double) -> Double): List<Point> {
        EnsireResultReady()

        val offset = result!!.offset
        val spline = result!!.spline

        val ROIStart = rawData.last { it.timestamp.time <= ROI.first }.timestamp.time
        val ROIEnd = rawData.first { it.timestamp.time >= ROI.second }.timestamp.time

        val range = ROIEnd - ROIStart

        val step = ms2S(range / SPLINE_POINTS_COUNT.toDouble())

        val start_X = ms2S(ROIStart.toDouble()) - offset

        return generateSequence(Point(start_X, spline.ssMethod(start_X))) {
            val timestamp = it.X + step
            return@generateSequence Point(timestamp, spline.ssMethod(timestamp))
        }
                .take(SPLINE_POINTS_COUNT)
                .map { Point(S2ms(it.X + offset), it.Y) }
                .toList()
    }

    override fun smoothData(ROI: Pair<Long, Long>): List<Point> = splineData(ROI) { this.Y(it) }

    override fun dYData(ROI: Pair<Long, Long>): List<Point> = splineData(ROI) { this.dY(it) }

    override fun smoothData(): List<Point> = splineData { this.Y(it) }

    override fun dYData(): List<Point> = splineData { this.dY(it) }

    //----------------------------------------------------------------------------------------------

    @Throws(IllegalStateException::class)
    private fun EnsireResultReady() {
        if (result == null) {
            calc_result()
        }
    }

    @Throws(IllegalStateException::class)
    private fun calc_result() {
        if (rawData.size < 3) {
            throw IllegalStateException("No enoth raw data present")
        }

        val dataOffset = dataOffset()
        val RawPoints = RawDataToPoints(dataOffset)
        val check_step = getMiddlePointStep(RawPoints) / 2.0
        val result_container = ResultHolder(dataOffset)

        result_container.spline.Update(RawPoints, pg)

        val kinks = find_d2Y_zeros(result_container.spline, RawPoints)

        if (kinks.isEmpty()) {
            throw IllegalStateException("No kinks found!")
        }

        val d1Y_local_msxs = kinks
                .map { Point(it, result_container.spline.dY(it)) }
                .filter {
                    val vp = result_container.spline.dY(it.X - check_step)
                    val va = result_container.spline.dY(it.X + check_step)

                    return@filter (vp < it.Y && va < it.Y
                            && it.Y <= 0.0) // Точки dY > 0 - точно ошибка
                }
                .filterKinks()

        if (d1Y_local_msxs.isEmpty()) {
            throw IllegalStateException("No maximums found!")
        }

        val Splinealue: (p: Point) -> Point = {
            val Y = result_container.spline.Y(it.X)
            Point(S2ms(it.X + result_container.offset), Y)
        }

        // выбрать масимум
        result_container.result = Splinealue(d1Y_local_msxs.maxBy { it.Y }!!)

        result_container.falseKinksResults = d1Y_local_msxs
                .filter { S2ms(it.X + result_container.offset) != result_container.result.X }
                .map(Splinealue)

        result = result_container // result ready
    }

    private fun List<Point>.filterKinks(): List<Point> {
        val res = LinkedList(this)

        do {
            val size = res.count()
            if (size <= 10)
                break

            val toRemove = mutableListOf<Int>()
            for (i in 1 until size - 1 step 2) {
                if (!testIslocalMaximum(res.listIterator(i)))
                    toRemove.add(i)
            }
            val removed = !toRemove.isEmpty()
            toRemove.reversed().onEach { res.removeAt(it) }
        } while (removed)

        return res
    }

    private fun testIslocalMaximum(it: MutableListIterator<Point>): Boolean {
        val prev = it.previous().Y
        val current = it.next().Y
        val next = it.next().Y

        return current > prev && current > next
    }

    private fun getMiddlePointStep(rawPoints: List<Point>): Double =
            (0..(rawPoints.size - 2))
                    .map { rawPoints[it + 1].X - rawPoints[it].X }
                    .average()

    private fun dataOffset(): Double = ms2S(rawData[0].timestamp)

    private fun RawDataToPoints(offset: Double): List<Point> {
        return rawData.map { d -> Point(ms2S(d.timestamp) - offset, d.temperature.toDouble()) }
    }

    private fun ms2S(timestamp: Date): Double = timestamp.time / 1000.0
    private fun ms2S(timestamp: Double): Double = timestamp / 1000.0

    private fun S2ms(v: Double): Double = v * 1000.0

}

internal fun find_d2Y_zeros(spline: SmoothSpline, rawPoints: List<Point>): List<Double> {
    val result = mutableListOf<Double>()

    for (i in 0 until rawPoints.size - 1) {
        val current_point = rawPoints[i]
        val next_point = rawPoints[i + 1]
        val X = (current_point.X + next_point.X) / 2.0

        val fragment = spline.find_fragment(X)

        val fragment_zero = fragment.zero_d2Y
        if (!java.lang.Double.isNaN(fragment_zero)) {
            result.add(fragment_zero)
        }
    }
    return result
}