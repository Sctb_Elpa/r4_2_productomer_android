package ru.sctbelpa.R4_2_productomer.DataStorage

import java.io.IOException
import java.io.InputStream
import java.text.ParseException

class CSVSessionParser @JvmOverloads constructor(
        private val istream: InputStream,
        val config: CSVgeneratorConfig = CSVgeneratorConfig()) {

    private data class SessionHolder(var Title: String = "") {
        lateinit var description: String
        var results: List<IRawValue> = mutableListOf()

        fun toSession(): ISession = DataSession(Title, description, results)
    }

    @Throws(IOException::class, ParseException::class)
    fun parse(): ISession {
        val holder = SessionHolder()

        parcecsv(istream, config) {
            holder.Title = readTitle()
            blankLine()
            holder.description = readDescription()
            readDate()
            readDuration()
            blankLine()
            skipToExperimentData()
            readDataTableHeader()

            while (true) {
                val result = readResult()
                if (result != null)
                    holder.results += result
                else
                    break
            }
        }

        return holder.toSession()
    }
}