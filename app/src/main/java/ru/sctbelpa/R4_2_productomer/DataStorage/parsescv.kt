package ru.sctbelpa.R4_2_productomer.DataStorage

import ru.sctbelpa.R4_2_productomer.Utils.S2ms
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.text.NumberFormat
import java.text.ParseException
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class CSVParceContext(istream: InputStream,
                      val config: CSVgeneratorConfig) {
    val reader = BufferedReader(InputStreamReader(istream))
    val myNumForm = NumberFormat.getInstance()
    val floatSplitter by lazy {
        val v = myNumForm.format(1.1)
        v[1].toString()
    }

    fun readline(): String = reader.readLine() ?: throw IOException("EOF reached")
    fun readlineOrNull(): String? = reader.readLine()
}

private val floatReplaceRe = Pattern.compile("[.,]")

@Throws(ParseException::class)
private fun parseString(string: String, parsePattern: Pattern): Matcher {
    val matcher = parsePattern.matcher(string)

    if (!matcher.find()) {
        throw ParseException("String \"$string\" does not match pattern \"$parsePattern\"", 0)
    }
    return matcher
}

inline fun parcecsv(istream: InputStream, config: CSVgeneratorConfig,
                    lambda: CSVParceContext.() -> Unit) {
    CSVParceContext(istream, config).lambda()
}

@Throws(IOException::class, ParseException::class)
fun CSVParceContext.readTitle(): String = parseString(readline(), config.TitleParser).group(1)

@Throws(ParseException::class)
fun CSVParceContext.blankLine() {
    if (!readline().isEmpty()) throw ParseException("Blank line expected", 0)
}

@Throws(IOException::class, ParseException::class)
fun CSVParceContext.readDescription(): String = parseString(readline(), config.DescriptionParser).group(1)

@Throws(IOException::class, ParseException::class)
fun CSVParceContext.readDate(): Date {
    val dateStr = parseString(readline(), config.dateParser).group(1)
    return config.dateFormatter.parse(dateStr)
}

@Throws(IOException::class, ParseException::class)
private fun CSVParceContext.parseFloat(floatStr: String): Float {
    val matcher = floatReplaceRe.matcher(floatStr)
    val text = matcher.replaceAll(floatSplitter)
    return if (text == "NaN") Float.NaN else myNumForm.parse(text).toFloat()
}

@Throws(IOException::class, ParseException::class)
fun CSVParceContext.readDuration(): Long {
    val durationStr = parseString(readline(), config.durationParser).group(1)
    return S2ms(parseFloat(durationStr))
}

@Throws(IOException::class)
fun CSVParceContext.skipToExperimentData() {
    val awaitString = config.experimentDataFormat.trim()
    while (true) {
        try {
            val s = readline()
            if (awaitString == s) {
                return
            }
        } catch (ex: IOException) {
            throw IOException("$awaitString wanted, but EOF reached")
        }
    }
}

@Throws(IOException::class, ParseException::class)
fun CSVParceContext.readDataTableHeader() {
    val line = readline()
    if (line != config.dataTableHeaderFormat)
        throw ParseException("\"${config.dataTableHeaderFormat}\" expected, but \"$line\" got", 0)
}

fun CSVParceContext.readResult(): IRawValue? {
    val l = readlineOrNull()
    if (l == null || l.isEmpty()) {
        return null
    }

    val fields = l.split(';')
    return RawValue(
            Integer.parseInt(fields[1]),
            config.dateFormatter.parse(fields[0]),
            parseFloat(fields[2]),
            parseFloat(fields[3])
    )

    /*
    val matcher = parseString(l, config.RawValueParser)

    return RawValue(
            Integer.parseInt(matcher.group(2)),
            config.dateFormatter.parse(matcher.group(1)),
            parseFloat(matcher.group(3)),
            parseFloat(matcher.group(4)))
            */
}