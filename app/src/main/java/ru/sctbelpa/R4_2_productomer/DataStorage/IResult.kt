package ru.sctbelpa.R4_2_productomer.DataStorage

import ru.sctbelpa.libsmoothspline.Point

interface IResult {
    val startTemperature: Float
    val endTemperature: Float
    val tresult: Float
    val chanelNumber: Int
    val rawData: List<IRawValue>
    val resultTimestamp: Long
    val falseKinksResults: List<Point>
    val aw: Float

    fun smoothData(): List<Point>

    fun dYData(): List<Point>

    fun smoothData(ROI: Pair<Long, Long>): List<Point>

    fun dYData(ROI: Pair<Long, Long>): List<Point>
}
