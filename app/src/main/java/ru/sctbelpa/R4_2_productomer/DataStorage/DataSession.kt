package ru.sctbelpa.R4_2_productomer.DataStorage

import ru.sctbelpa.R4_2_ProductomerService.ISessionID
import ru.sctbelpa.R4_2_productomer.Utils.collapseSeriesBy
import ru.sctbelpa.R4_2_productomer.Utils.monotonicDownBy
import ru.sctbelpa.R4_2_productomer.Utils.sameContentWith
import java.util.*

class DataSession @JvmOverloads constructor(
        _title: String,
        _description: String,
        data: List<IRawValue>? = null) : ISession {

    override var Title: String = _title
        set(value) {
            field = value
            notifySessionUpdated()
        }

    override var Description: String = _description
        set(value) {
            field = value
            notifySessionUpdated()
        }

    var m_RawData = data?.toMutableList() ?: mutableListOf()
        private set(value) {
            field = value
        }

    override var id: ISessionID? = null

    override val date: Date
        get() {
            if (RawData.isEmpty())
                throw IllegalStateException("DataSession is empty")
            return RawData.first().timestamp
        }

    override val SessionDuration: Long
        get() {
            if (RawData.isEmpty()) {
                throw IllegalStateException("DataSession is empty")
            }

            val timestamps_all: List<Long> = RawData.map { r -> r.timestamp.time }

            val min = timestamps_all.min()!!
            val max = timestamps_all.max()!!

            return max - min
        }

    override val RawData: List<IRawValue>
        get() = m_RawData

    override val Results: List<IResult>
        get() {
            return findChannels()
                    .map {
                        val result = Result(it)
                        result.rawData = RawDataByChanel(it)
                        return@map result
                    }
        }

    override fun RawDataByChanel(chanel: Int): List<IRawValue> {
        return RawData
                .asSequence()
                .filter { it.chanelNumber == chanel }
                //.distinctBy { it.temperature }
                .collapseSeriesBy { it.temperature }
                .toList()
    }

    @Transient
    override var OnSessionChanged: ISession.SessionUpdateListener? = null

    //----------------------------------------------------------------------------------------------

    fun Add(value: IRawValue) = m_RawData.add(value)

    private fun findChannels(): Set<Int> {
        return RawData
                .map { it.chanelNumber }
                .sorted()
                .distinct()
                .toSet()
    }

    override fun toString(): String {
        return buildString {
            appendln(Title)
            appendln(Description)
            appendln(date)
            appendln(SessionDuration)

            for (rv in RawData) {
                appendln(rv.toString())
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other !is ISession)
            return false

        if (other.Title != Title)
            return false
        if (other.date != date)
            return false
        if (other.SessionDuration != SessionDuration)
            return false
        if (other.Description != Description)
            return false

        return (RawData sameContentWith other.RawData)!!
    }

    private fun notifySessionUpdated() {
        OnSessionChanged?.SessionUpdated(this)
    }

    override fun simplfydata() {
        // неэффективно
        //monotonicDown()
        //dropDropBigData()
    }

    private fun monotonicDown() {
        m_RawData = m_RawData.asSequence().monotonicDownBy { it.temperature }.toMutableList()
    }

    private fun dropDropBigData() {
        val count = m_RawData.count()
        if (count > SIMPLIFY_TRESHOLD) {
            val chankSize = count / SIMPLIFY_TRESHOLD

            m_RawData = m_RawData.asSequence().chunked(chankSize).map { it[0] }.toMutableList()
        }
    }

    companion object {
        val SIMPLIFY_TRESHOLD = 500
    }

}