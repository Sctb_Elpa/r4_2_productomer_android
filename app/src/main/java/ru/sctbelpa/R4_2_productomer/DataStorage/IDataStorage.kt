package ru.sctbelpa.R4_2_productomer.DataStorage

import ru.sctbelpa.R4_2_ProductomerService.ISessionID
import java.io.IOException
import java.text.ParseException

interface IDataStorage {

    // Возвращает количество ссохраненных сессий
    @Deprecated("")
    @Throws(IOException::class)
    fun getSessionCount(): Int

    @Throws(IOException::class)
    fun getSessionList(): ArrayList<ISessionID>

    // получить сессию по номеру
    @Deprecated("")
    @Throws(IOException::class, ParseException::class)
    fun GetSessionByID(i: Int): ISession

    @Throws(IOException::class, ParseException::class)
    fun GetSessionByID(id: ISessionID?): ISession?

    @Throws(IllegalAccessException::class, IOException::class)
    fun SaveSession(session: ISession?): ISessionID?

    // закрыть хранилище
    fun close()

    @Throws(IOException::class)
    fun removeSession(id: ISessionID?)
}
