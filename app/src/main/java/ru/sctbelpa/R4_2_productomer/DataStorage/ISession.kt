package ru.sctbelpa.R4_2_productomer.DataStorage

import ru.sctbelpa.R4_2_ProductomerService.ISessionID
import java.io.Serializable
import java.util.*

interface ISession : Serializable {
    var Title: String                               // Название сессии
    var Description: String                         // Краткое описание
    var id: ISessionID?

    @get:Throws(IllegalStateException::class)
    val date: Date                                  // Дата начала

    @get:Throws(IllegalStateException::class)
    val SessionDuration: Long

    val RawData: List<IRawValue>                    // сырые данные измерений
    val Results: List<IResult>

    @Throws(IndexOutOfBoundsException::class)
    fun RawDataByChanel(chanel: Int): List<IRawValue>

    var OnSessionChanged: SessionUpdateListener?

    fun simplfydata()

    interface SessionUpdateListener {
        fun SessionUpdated(session: ISession)
    }
}