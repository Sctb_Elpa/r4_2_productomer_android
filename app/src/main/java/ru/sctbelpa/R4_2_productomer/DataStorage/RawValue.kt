package ru.sctbelpa.R4_2_productomer.DataStorage

import java.util.*

class RawValue(override val chanelNumber: Int,
               override val timestamp: Date,
               override val temperature: Float,
               override val ft: Float) : IRawValue {

    private infix fun Float.equalsNaN(other: Float): Boolean {
        return (this.isNaN() && other.isNaN()) || (this == other)
    }

    override fun equals(other: Any?): Boolean {

        if (other !is RawValue) {
            return false
        }

        val lr = other as IRawValue

        val r1 = lr.chanelNumber == chanelNumber &&
                lr.timestamp == timestamp
        val r2 = temperature equalsNaN lr.temperature
        val r3 = ft equalsNaN lr.ft

        return r1 and r2 and r3
    }

    override fun hashCode(): Int {
        return chanelNumber.hashCode() xor
                timestamp.hashCode() xor
                temperature.hashCode() xor
                ft.hashCode()
    }

    override fun toString(): String {
        return "Chanel $chanelNumber: T=$temperature, Ft=$ft"
    }
}
