package ru.sctbelpa.R4_2_productomer.DataStorage;

import java.io.IOError;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import ru.sctbelpa.R4_2_ProductomerService.ISessionID;

public class SQLiteDataStorage implements IDataStorage {

    @Override
    public int getSessionCount() throws IOError {
        return 0;
    }

    @Override
    public ISession GetSessionByID(int i) {
        return null;
    }

    @Override
    public void close() {
    }

    @Override
    public void removeSession(ISessionID id) throws IOException {

    }

    @Override
    public ArrayList<ISessionID> getSessionList() throws IOException {
        return new ArrayList<>();
    }

    @Override
    public ISession GetSessionByID(ISessionID id) throws IOException, ParseException {
        return null;
    }

    @Override
    public ISessionID SaveSession(ISession session) throws IllegalAccessException, IOException {
        return null;
    }
}
