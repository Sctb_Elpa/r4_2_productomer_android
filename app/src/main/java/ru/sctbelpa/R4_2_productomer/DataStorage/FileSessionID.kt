package ru.sctbelpa.R4_2_productomer.DataStorage

import ru.sctbelpa.R4_2_ProductomerService.ISessionID

class FileSessionID(val sessionFileName: String) : ISessionID {

    override fun toString(): String = "CSV sessionID: $sessionFileName"

    override fun equals(other: Any?): Boolean {
        return when (other) {
            null -> false
            is FileSessionID -> sessionFileName == other.sessionFileName
            else -> false
        }
    }

    override fun hashCode(): Int = sessionFileName.hashCode()
}
