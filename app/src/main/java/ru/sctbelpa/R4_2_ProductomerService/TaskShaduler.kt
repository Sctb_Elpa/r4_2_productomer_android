package ru.sctbelpa.R4_2_ProductomerService

import android.os.Bundle
import android.os.ResultReceiver
import java.io.Serializable
import java.util.*

class TaskShaduler {

    private val ShaduledTasks = HashSet<ShadulableTask>()

    abstract class ShadulableTask : TimerTask, Serializable {

        val destinationTime: Date
        private var ressiver: ResultReceiver? = null
        var shaduler: TaskShaduler? = null

        constructor(after_ms: Int) {
            val c = Calendar.getInstance()
            c.time = Date()
            c.add(Calendar.MILLISECOND, after_ms)

            this.destinationTime = c.time
        }

        constructor(destinationTime: Date) {
            this.destinationTime = destinationTime
        }

        fun setResultRessiver(ressiver: ResultReceiver): ShadulableTask {
            this.ressiver = ressiver
            return this
        }

        @Throws(Exception::class)
        abstract fun execute(bundle: Bundle)

        private fun sendResult(code: Int, bundle: Bundle) {
            if (ressiver != null) {
                ressiver!!.send(code, bundle)
            }
        }

        override fun run() {
            val bundle = Bundle()

            try {
                execute(bundle)
                sendResult(ProductomerResultReceiver.RESULT_CODE_OK, bundle)
            } catch (ex: Exception) {
                bundle.putSerializable(ProductomerResultReceiver.PARAM_EXCEPTION, ex)
                sendResult(ProductomerResultReceiver.RESULT_CODE_ERROR, bundle)
            }

            shaduler!!.CancelTask(this)
        }

        override fun cancel(): Boolean {
            shaduler!!.CancelTask(this)
            return super.cancel()
        }
    }

    fun shaduleTask(task: ShadulableTask) {
        val timer = Timer(task.toString() + " timer")

        timer.schedule(task, task.destinationTime)

        task.shaduler = this

        ShaduledTasks.add(task)
    }

    private fun CancelTask(task: ShadulableTask) {
        val contains = ShaduledTasks.contains(task)
        if (!contains) {
            throw IllegalArgumentException("Task is not in shaduled set.")
        }

        ShaduledTasks.remove(task)
    }

    @Throws(NoSuchElementException::class)
    fun cancelTaskByID(cancelID: Int?) {
        ShaduledTasks
                .first { t -> t.hashCode() == cancelID }
                .cancel()
    }
}
