package ru.sctbelpa.R4_2_ProductomerService

import android.bluetooth.BluetoothDevice
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.sctbelpa.R4_2_productomer.DataStorage.*
import ru.sctbelpa.R4_2_productomer.Utils.LOG_TAG
import java.io.IOException
import java.util.*
import kotlin.random.Random

class FakeWorkSession(override val device: BluetoothDevice,
                      private val service: Service) : IWorkSession {

    override val rawdata: LinkedList<IRawValue> = LinkedList()

    private var readJob: Job? = null

    private var RawData: List<IRawValue> = emptyList()
    private var it = RawData.iterator()

    override val id: IWorkSessionID
        get() = WorkSessionID(device.name, device.address, device.hashCode())

    override var enabledChannels: Array<Boolean> = arrayOf(true, true)
        set(value) {
            field = value

            synchronized(rawdata) {
                for (i in 0..1) {
                    if (!value[i]) {
                        val toRemove = rawdata.filter { it.chanelNumber == i + 1 }
                        rawdata.removeAll(toRemove)
                    }
                }
            }
        }

    private fun resetChanelEnable() {
        enabledChannels = arrayOf(true, true)
    }

    override var state: IWorkSession.State = IWorkSession.State.IDLE
        private set(value) {
            field = value
        }

    override fun connect(): IWorkSession.State {
        if (state == IWorkSession.State.ERROR || state == IWorkSession.State.IDLE) {
            state = IWorkSession.State.CONNECTING
            Thread.sleep(500)
            openfile()
            state = IWorkSession.State.DISPLAYING_DATA
            resetChanelEnable()
            startReadCycle()
        }
        return state
    }

    override fun close() {
        readJob?.cancel()

        state = IWorkSession.State.IDLE
    }

    private fun startReadCycle() {
        rawdata.clear()
        readJob = GlobalScope.launch {
            while (true) {
                delay(250L)
                val newData = generateNewResult() ?: break
                synchronized(rawdata) {
                    rawdata.addAll(newData)
                }
                if (state == IWorkSession.State.DISPLAYING_DATA) {
                    trimData(50)
                }

                service.sessionUpdated(this@FakeWorkSession)
            }
            state = IWorkSession.State.IDLE
        }
    }

    private fun generateNewResult(): List<IRawValue>? {
        if (!it.hasNext())
            return null

        val v = it.next()

        return if (false !in enabledChannels) {
            listOf(
                    RawValue(1, v.timestamp, v.temperature + Random.nextFloat() * 1e-1f, v.ft),
                    RawValue(2, v.timestamp, v.temperature + Random.nextFloat() * 1e-1f, v.ft)
            )
        } else {
            listOf(if (enabledChannels.first())
                RawValue(1, v.timestamp, v.temperature + Random.nextFloat() * 1e-1f, v.ft)
            else
                RawValue(2, v.timestamp, v.temperature + Random.nextFloat() * 1e-1f, v.ft)
            )
        }
    }

    private fun trimData(maxPointsPreChanel: Int) {

        val findIndexesToRemove: (chanel: Int) -> List<Int> = { chanel ->
            val count = rawdata.count { it.chanelNumber == chanel }
            if (count > maxPointsPreChanel) {
                rawdata
                        .asSequence()
                        .mapIndexed { i, v -> if (v.chanelNumber == chanel) i else -1 }
                        .filter { it >= 0 }
                        .take(count - maxPointsPreChanel)
                        .toList()
            } else {
                emptyList()
            }
        }

        // обратный порядок, чтобы при удалении сдвиг элементов не ломался.
        val indexesToRemove = arrayListOf(findIndexesToRemove(1), findIndexesToRemove(2))
                .flatten().sortedDescending()

        indexesToRemove.forEach { rawdata.removeAt(it) }
    }

    private fun openfile() {
        val storage = CSVDataStorage()
        val id = storage.getSessionList().first()
        val s = storage.GetSessionByID(id)

        RawData = s!!.RawData
        it = RawData.iterator()
    }

    @Throws(IllegalStateException::class)
    override fun startCollectData() {
        synchronized(rawdata) {
            rawdata.clear()
        }
        if (state != IWorkSession.State.DISPLAYING_DATA &&
                state != IWorkSession.State.COLLECTING_DATA) {
            throw IllegalStateException("Device not opened")
        }
        state = IWorkSession.State.COLLECTING_DATA
    }

    @Throws(IllegalStateException::class, IOException::class)
    override fun finalise(): ISessionID {
        close()
        try {
            val result = calculateResult()
            return saveResult(result)
        } catch (ex: Exception) {
            Log.e(LOG_TAG, "Inconsistent result", ex)
            throw IllegalStateException("Inconsistent result", ex)
        }
    }

    private fun calculateResult(): ISession {
        val firstPoint = rawdata.first()

        return DataSession("Новая сессия %s".format(firstPoint.timestamp.toString()), "Добавьте описание сессии",
                rawdata.clone() as List<IRawValue>)
    }

    @Throws(IOException::class)
    private fun saveResult(session: ISession): ISessionID {
        val id = service.saveDataSession(session) ?: throw IOException("Can't save session")
        service.notifySessionListChanged()
        return id
    }

}