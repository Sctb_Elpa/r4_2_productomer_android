package ru.sctbelpa.R4_2_ProductomerService

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.util.Log
import kotlinx.coroutines.*
import ru.sctbelpa.R4_2_productomer.DataStorage.DataSession
import ru.sctbelpa.R4_2_productomer.DataStorage.IRawValue
import ru.sctbelpa.R4_2_productomer.DataStorage.ISession
import ru.sctbelpa.R4_2_productomer.DataStorage.RawValue
import ru.sctbelpa.R4_2_productomer.service.*
import ru.sktbelpa.r4_2_prodm.ProtobufDevice0000E004
import java.io.IOException
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class ProductomerWorkSession(override val device: BluetoothDevice,
                             private val service: Service) : IWorkSession {

    override val rawdata: LinkedList<IRawValue> = LinkedList()

    private var btsocket: BluetoothSocket? = null
    private var dispatcher: Dispatcher? = null
    private var readJob: Job? = null

    override var state: IWorkSession.State = IWorkSession.State.IDLE
        private set(value) {
            field = value
        }

    override var enabledChannels: Array<Boolean> = arrayOf(true, true)
        set(value) {
            synchronized(rawdata) {
                for (i in 0..1) {
                    if (!value[i] && field[i]) {
                        val toRemove = rawdata.filter { it.chanelNumber == i + 1 }
                        synchronized(rawdata) {
                            rawdata.removeAll(toRemove)
                        }
                    }
                }
            }
            field = value
        }

    override fun close() {
        if (btsocket?.isConnected == true) {
            readJob?.cancel()
            dispatcher!!.interrupt()

            btsocket?.close()
        }

        state = IWorkSession.State.IDLE
    }

    override fun connect(): IWorkSession.State {
        if (state == IWorkSession.State.ERROR || state == IWorkSession.State.IDLE) {
            state = IWorkSession.State.CONNECTING
            try {
                btsocket = device.createRfcommSocketToServiceRecord(defaultUUID)
            } catch (ex: IOException) {
                Log.e(LOG_TAG, "Can't create bluetothsocket, is BT inabled?", ex)
                state = IWorkSession.State.ERROR
                return state
            }

            try {
                btsocket!!.connect()
            } catch (ex: IOException) {
                Log.e(LOG_TAG, "Can't open connection to $device", ex)
                state = IWorkSession.State.ERROR
                return state
            }


            dispatcher = Dispatcher(btsocket!!.inputStream, btsocket!!.outputStream).apply {
                start()

                if (synchronizeDeviceClock(this)) {
                    this@ProductomerWorkSession.state = IWorkSession.State.DISPLAYING_DATA
                    startReadCycle()
                } else {
                    interrupt()
                    this@ProductomerWorkSession.state = IWorkSession.State.ERROR
                }
            }
        }
        return state
    }

    private fun synchronizeDeviceClock(dispatcher: Dispatcher): Boolean {
        val request = ProtobufDevice0000E004.Request.newBuilder().apply {
            id = dispatcher.genNewUnusedId()
            deviceID = ProtobufDevice0000E004.INFO.R4_2_PRODUCTOMER_ID_VALUE
            protocolVersion = ProtobufDevice0000E004.INFO.PROTOCOL_VERSION_VALUE
            setClock = Date().time // in ms
        }
        val command = ProductomerCommand(dispatcher, request.build())
        val obj = java.lang.Object()
        var result = false

        with(command) {
            timeout = TIMEOUT
            onSuccess(object : ISuccessCallback {
                override fun processResult(response: ProtobufDevice0000E004.Response) {
                    result = response.globalStatus == ProtobufDevice0000E004.STATUS.OK
                    synchronized(obj) {
                        obj.notify()
                    }
                }
            })
            onError(object : IFailCallback {
                override fun processError(command: IProductomerCommand, ex: Exception) {
                    result = false
                    synchronized(obj) {
                        obj.notify()
                    }
                }
            })
            execute()
        }

        synchronized(obj) {
            obj.wait()
        }
        return result
    }

    override val id: IWorkSessionID
        get() = WorkSessionID(device.name, device.address, device.hashCode())

    @Throws(IllegalStateException::class)
    override fun startCollectData() {
        resetRawData()
        if (state != IWorkSession.State.DISPLAYING_DATA &&
                state != IWorkSession.State.COLLECTING_DATA) {
            throw IllegalStateException("Device not opened")
        }
        state = IWorkSession.State.COLLECTING_DATA
    }

    @Throws(IllegalStateException::class)
    override fun finalise(): ISessionID {
        close()
        try {
            val result = calculateResult()
            return saveResult(result)
        } catch (ex: Exception) {
            Log.e(ru.sctbelpa.R4_2_productomer.Utils.LOG_TAG, "Inconsistent result", ex)
            throw IllegalStateException("Inconsistent result", ex)
        }
    }

    @Synchronized
    private fun resetRawData() {
        synchronized(rawdata) {
            rawdata.clear()
        }
    }

    private fun startReadCycle() {
        resetRawData()
        readJob = GlobalScope.launch {
            val reference = ProtobufDevice0000E004.Request.newBuilder().apply {
                deviceID = ProtobufDevice0000E004.INFO.R4_2_PRODUCTOMER_ID_VALUE
                protocolVersion = ProtobufDevice0000E004.INFO.PROTOCOL_VERSION_VALUE
                getHistory = ProtobufDevice0000E004.HistoryReq.newBuilder().build()
            }

            while (true) {
                delay(500L)
                val newData = readNewResult(reference) ?: break
                synchronized(rawdata) {
                    mergeNewResults(rawdata, newData)
                }
                if (state == IWorkSession.State.DISPLAYING_DATA) {
                    trimData(50)
                }

                service.sessionUpdated(this@ProductomerWorkSession)
            }
            state = IWorkSession.State.IDLE
        }
    }

    private fun mergeNewResults(rawdata: LinkedList<IRawValue>, newData: List<IRawValue>) {
        for (i in 1..2) {
            if (enabledChannels[i - 1]) {
                val chanelData = newData.filter { it.chanelNumber == i }

                // последний элемент с этого канала в сохраненных данных
                val lastChannelValue = rawdata.lastOrNull { it.chanelNumber == i }

                // если в сохраненных данных вообще нет данных от этого канала, берем все
                if (lastChannelValue == null) {
                    rawdata.addAll(chanelData)
                    continue
                }


                // ищем в новых данных индекс элемента, совпадающего с последним тем, что у нас есть
                val indexOfLastNonCopy = chanelData.indexOfFirst { it == lastChannelValue }

                // если в новых данных нет элемента, который мы получали последним, то берем все
                if (indexOfLastNonCopy < 0) {
                    rawdata.addAll(chanelData)
                    continue
                }

                // если последний полученый элемент тот же что у нас уже есть, то и копировать нечего
                if (indexOfLastNonCopy == chanelData.size - 1) {
                    continue
                }

                // копируем только ту часть, которая определена новой
                rawdata.addAll(chanelData, indexOfLastNonCopy + 1)
            }
        }
        rawdata.sortBy { it.timestamp }
    }

    private fun <T> LinkedList<T>.addAll(src: Collection<T>, offset: Int) {
        if (src.size > offset) {
            addAll(src.drop(offset))
        }
    }

    private suspend fun readNewResult(requestBuilder: ProtobufDevice0000E004.Request.Builder): List<IRawValue>? {
        val d = dispatcher ?: return null
        if (!d.isRunning)
            return null

        requestBuilder.id = dispatcher!!.genNewUnusedId() // new ID

        val command = ProductomerCommand(d, requestBuilder.build())
        val response: ProtobufDevice0000E004.Response
        try {
            response = suspendCancellableCoroutine { continuation ->
                with(command) {
                    timeout = TIMEOUT
                    onSuccess(object : ISuccessCallback {
                        override fun processResult(response: ProtobufDevice0000E004.Response) =
                                continuation.resume(response)
                    })
                    onError(object : IFailCallback {
                        override fun processError(command: IProductomerCommand, ex: Exception) =
                                continuation.resumeWithException(ex)
                    })
                    execute()
                }
                continuation.invokeOnCancellation { command.cancel() }
            }
        } catch (ex: Exception) {
            //dispatcher!!.removeCommandFromPendingSet(command)
            return emptyList() // todo, may be cancel by returning null
        }

        return responce2rawValues(response)
    }

    private fun responce2rawValues(responce: ProtobufDevice0000E004.Response): List<IRawValue>? {
        return responce.history?.historyItemsList?.map {
            RawValue(it.chanelNumber + 1, Date(it.timestamp), it.temperature, it.freq)
        }
    }

    private fun trimData(maxPointsPreChanel: Int) {

        val findIndexesToRemove: (chanel: Int) -> List<Int> = { chanel ->
            val count = rawdata.count { it.chanelNumber == chanel }
            if (count > maxPointsPreChanel) {
                rawdata
                        .asSequence()
                        .mapIndexed { i, v -> if (v.chanelNumber == chanel) i else -1 }
                        .filter { it >= 0 }
                        .take(count - maxPointsPreChanel)
                        .toList()
            } else {
                emptyList()
            }
        }

        // обратный порядок, чтобы при удалении сдвиг элементов не ломался.
        val indexesToRemove = arrayListOf(findIndexesToRemove(1), findIndexesToRemove(2))
                .flatten().sortedDescending()

        indexesToRemove.forEach { rawdata.removeAt(it) }
    }

    private fun calculateResult(): ISession {
        val firstPoint = rawdata.first()

        return DataSession("Новая сессия %s".format(firstPoint.timestamp.toString()), "Добавьте описание сессии",
                rawdata.clone() as List<IRawValue>)
    }

    @Throws(IOException::class)
    private fun saveResult(session: ISession): ISessionID {
        val id = service.saveDataSession(session) ?: throw IOException("Can't save session")
        service.notifySessionListChanged()
        return id
    }

    companion object {
        val defaultUUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb")!!
        const val LOG_TAG = "ProductomerWorkSession"
        const val TIMEOUT = 1000L
    }
}