package ru.sctbelpa.R4_2_ProductomerService

class WorkSessionID(
        override val name: String,
        override val identity: String,
        val hash: Int = 0) : IWorkSessionID {

    override fun equals(other: Any?): Boolean = if (other is WorkSessionID) other.hash == hash else false

    override fun hashCode(): Int = hash

    override fun toString(): String = "SessionID0x%X".format(hash)
}
