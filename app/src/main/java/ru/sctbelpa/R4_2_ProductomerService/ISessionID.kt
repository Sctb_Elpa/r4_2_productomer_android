package ru.sctbelpa.R4_2_ProductomerService

import java.io.Serializable

/**
 * Идентификатор, связывающий сессию с хранилищем
 */
interface ISessionID : Serializable
