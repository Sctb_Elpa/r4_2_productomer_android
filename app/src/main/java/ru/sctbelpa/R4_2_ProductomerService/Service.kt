package ru.sctbelpa.R4_2_ProductomerService

import android.app.PendingIntent
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.os.IBinder
import android.os.ResultReceiver
import android.support.v4.app.NotificationCompat
import android.util.Log
import kotlinx.coroutines.Delay
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.sctbelpa.R4_2_productomer.DataStorage.CSVDataStorage
import ru.sctbelpa.R4_2_productomer.DataStorage.IDataStorage
import ru.sctbelpa.R4_2_productomer.DataStorage.ISession
import ru.sctbelpa.R4_2_productomer.DataStorage.MOCDataStorage
import ru.sctbelpa.R4_2_productomer.R
import ru.sctbelpa.R4_2_productomer.Utils.removeAll
import java.io.IOException
import java.security.InvalidKeyException

/**
 * Служба - бэкэнд для всех IO-операций
 */
class Service : android.app.Service() {

    private val shaduler = TaskShaduler()
    private val watchers = HashMap<Int, ResultReceiver>()

    private lateinit var storage: IDataStorage
    private var workSessionsMap = HashMap<IWorkSessionID, IWorkSession>()
    private var adapter: BluetoothAdapter? = null

    private var noStorageMode = false
    private var ROMode = false

    /**
     * \quote
     * Система вызывает этот метод, когда другой компонент, например, операция, запрашивает запуск
     * этой службы, вызывая startService(). После выполнения этого метода служба запускается и может
     * в течение неограниченного времени работать в фоновом режиме. Если вы реализуете такой метод,
     * вы обязаны остановить службу посредством вызова stopSelf() или stopService(). (Если требуется
     * только обеспечить привязку, реализовывать этот метод не обязательно).
     */
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        // сюда будут приходить интенты с командами, но в контексте вызывающего потока

        if (intent != null) {
            val resultReceiver = intent.getParcelableExtra<ResultReceiver?>(
                    ServiceControl.PARAM.RESULT_RECEIVER.name)
            val extras = intent.extras

            executeAction(intent.action, extras, resultReceiver)
        }

        GlobalScope.launch {
            delay(1000)
            tryDestroySelf()
        }

        return super.onStartCommand(intent, flags, startId)
    }

    /**
     * \quote
     * Система вызывает этот метод при первом создании службы для выполнения однократных процедур
     * настройки (перед вызовом onStartCommand() или onBind()). Если служба уже запущена, этот
     * метод не вызывается.
     */
    override fun onCreate() {
        // Инициализировать хранилище
        try {
            storage = CSVDataStorage()
        } catch (e: IOException) {
            // Не удалось инициализировать хранилище, создаём moc
            storage = MOCDataStorage()
            noStorageMode = true
        }

        // получить адаптер Bluetooth
        adapter = BluetoothAdapter.getDefaultAdapter()
        if (adapter == null) {
            // Bluetooth не поддерживается. Режим ReadOnly
            ROMode = true
        }
    }

    /**
     * \quote
     * Система вызывает этот метод, когда служба более не используется и выполняется ее уничтожение.
     * Ваша служба должна реализовать это для очистки ресурсов, таких как потоки, зарегистрированные
     * приемники, ресиверы и т. д. Это последний вызов, который получает служба.
     */
    override fun onDestroy() {
        for ((_, v) in workSessionsMap) {
            v.close()
        }

        // закрыть хранилище
        storage.close()

        // отключиться от Bluetooth
        adapter = null
    }

    // биндера не будет, будем передавать запросы через Intent'ы
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    /**
     * Установить сервис в важные, и что его нельзя убивать или отменить установку
     *
     * Когда идет измерительная сессия нельзя закрывать сервис
     */
    private fun makeForeground(startId: Int, isForeground: Boolean) {
        if (isForeground) {
            val b = with(NotificationCompat.Builder(this)) {
                setOngoing(true)
                setContentTitle(getString(R.string.Productomer_busy))
                setContentText(getString(R.string.Productomer_session_in_progress))
                setSmallIcon(R.drawable.ic_vegetables2)
                setContentIntent(newOpenProcessActivity())
            }

            startForeground(startId, b.build())
        } else {
            stopForeground(true)
        }
    }

    private fun getMethodName(): String {
        val trace = Thread.currentThread().stackTrace
        return trace[3].methodName
    }

    private fun notifyCollectingStateChanged(wsession: IWorkSessionID) = with(Bundle()) {
        putSerializable(ServiceControl.PARAM.SESSION_ID.name, wsession)
        putSerializable(ServiceControl.PARAM.CONNECTION_STATE.name,
                if (isCollectingData()) IWorkSession.State.COLLECTING_DATA.ordinal else IWorkSession.State.IDLE.ordinal)
        putString(SENDER, getMethodName())
        sendNotification(this)
    }

    private fun newOpenProcessActivity(): PendingIntent {
        val intent = Intent(this, ru.sctbelpa.R4_2_productomer.GUI.DataCollectionActivity::class.java)
        return PendingIntent.getActivity(this, 0, intent, Intent.FILL_IN_ACTION)
    }

    private fun executeAction(action: String?, extras: Bundle?, resultReceiver: ResultReceiver?) {
        when (action) {
            ServiceControl.ACTIONS.DATASESSION_LIST.name -> handleSessionList(resultReceiver)
            ServiceControl.ACTIONS.GET_DATASESSION.name -> {
                val id = extras!!.getSerializable(
                        ServiceControl.PARAM.SESSION_ID.name) as ISessionID
                handleGetSession(id, resultReceiver)
            }
            ServiceControl.ACTIONS.SAVE_DATASESSION.name -> {
                val session = extras!!.getSerializable(
                        ServiceControl.PARAM.SESSION_DATA.name) as ISession
                handleWriteSession(session, resultReceiver!!)
            }
            ServiceControl.ACTIONS.REMOVE_DATASESSION.name -> {
                val id = extras!!.getSerializable(
                        ServiceControl.PARAM.SESSION_ID.name) as ISessionID
                val onFinished = extras.getParcelable<ResultReceiver>(
                        ServiceControl.PARAM.ON_OPERATION_FINISHED.name)
                handleRemoveSession(id, onFinished, resultReceiver!!)
            }
            ServiceControl.ACTIONS.REMOVE_DATASESSION_CANCEL.name -> {
                val cancelID = extras!!.getInt(ServiceControl.PARAM.CANCEL_ID.name)
                handleCancelRemoving(cancelID, resultReceiver!!)
            }
            ServiceControl.ACTIONS.REGISTER_WATCHER.name -> {
                val watcherID = extras!!.getInt(ServiceControl.PARAM.WATCHER_ID.name)
                handleRegisterWatcher(resultReceiver!!, watcherID)
            }
            ServiceControl.ACTIONS.UNREGISTER_WATCHER.name -> {
                val watcherID = extras!!.getInt(ServiceControl.PARAM.WATCHER_ID.name)
                handleUnRegisterWatcher(watcherID)
            }
            ServiceControl.ACTIONS.ACTIVATE_WORK_SESSION.name -> {
                val wSessionID = extras!!.getSerializable(
                        ServiceControl.PARAM.SESSION_ID.name) as IWorkSessionID
                handleActivateWorkSession(wSessionID, resultReceiver)
            }
            ServiceControl.ACTIONS.FORCE_UPDATE_STATE.name -> handleUpdateState()
            ServiceControl.ACTIONS.WORK_SESSION_ID_LIST.name -> handleWorkSessionList(resultReceiver!!)
            ServiceControl.ACTIONS.START_COLLECTING_DATA.name -> {
                val wSessionID = extras!!.getSerializable(
                        ServiceControl.PARAM.SESSION_ID.name) as IWorkSessionID
                handleStartCollectingData(wSessionID, resultReceiver)
            }
            ServiceControl.ACTIONS.FINALISE_WORKSESSION.name -> {
                val wSessionID = extras!!.getSerializable(
                        ServiceControl.PARAM.SESSION_ID.name) as IWorkSessionID
                handleFinaliseSession(wSessionID, resultReceiver)
            }
            ServiceControl.ACTIONS.CHANEL_CONTROL.name -> {
                val wSessionID = extras!!.getSerializable(
                        ServiceControl.PARAM.SESSION_ID.name) as IWorkSessionID
                val controlArray = extras.getSerializable(
                        ServiceControl.PARAM.CHANEL_ENABLE_STATE.name) as Array<Boolean>

                handleChanelControl(wSessionID, controlArray, resultReceiver)
            }
            ServiceControl.ACTIONS.FIRST_BUSY_SESSION.name -> handleFirstBusySession(resultReceiver!!)
            ServiceControl.ACTIONS.GET_WORK_SESSION_ENABLED_CHANNELS.name -> {
                val wSessionID = extras!!.getSerializable(
                        ServiceControl.PARAM.SESSION_ID.name) as IWorkSessionID
                handleGETWorkSessionEnabledChannels(wSessionID, resultReceiver!!)
            }
            ServiceControl.ACTIONS.FINALISE_DISPLYING_SESSIONS.name -> handleFinaliseDisplayingSessions(resultReceiver)
        }
    }

    private fun tryDestroySelf() {
        if (!isCollectingData() && watchers.isEmpty()) {
            stopSelf()
        }
    }

    private fun handleFinaliseDisplayingSessions(resultReceiver: ResultReceiver?) {
        for ((k, v) in workSessionsMap) {
            if (v.state == IWorkSession.State.DISPLAYING_DATA) {
                v.close()
            }
        }

        if (resultReceiver != null) {
            with(Bundle()) {
                putSerializable(ProductomerResultReceiver.PARAM_RESULT, null)
                putString(SENDER, getMethodName())
                resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_OK, this)
            }
        }
    }

    private fun handleGETWorkSessionEnabledChannels(sessionID: IWorkSessionID, resultReceiver: ResultReceiver) {
        val wsession = findSessionByID(sessionID, resultReceiver) ?: return
        val enabledChannels = wsession.enabledChannels

        with(Bundle()) {
            putSerializable(ProductomerResultReceiver.PARAM_RESULT, enabledChannels)
            putString(SENDER, getMethodName())
            resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_OK, this)
        }
    }

    private fun handleFirstBusySession(resultReceiver: ResultReceiver) = with(Bundle()) {
        putSerializable(ProductomerResultReceiver.PARAM_RESULT,
                workSessionsMap.values
                        .firstOrNull { it.state == IWorkSession.State.COLLECTING_DATA }
                        ?.id)
        putString(SENDER, getMethodName())
        resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_OK, this)
    }

    private fun handleChanelControl(wSessionID: IWorkSessionID, controlArray: Array<Boolean>,
                                    resultReceiver: ResultReceiver?) {
        val s = findSessionByID(wSessionID, resultReceiver) ?: return

        s.enabledChannels = controlArray

        if (resultReceiver != null) {
            with(Bundle()) {
                putSerializable(ProductomerResultReceiver.PARAM_RESULT, wSessionID)
                putString(SENDER, getMethodName())
                resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_OK, this)
            }
        }
    }

    private fun handleFinaliseSession(wSessionID: IWorkSessionID, resultReceiver: ResultReceiver?) {
        val s = findSessionByID(wSessionID, resultReceiver) ?: return

        val resultSessionID = s.finalise()
        with(Bundle()) {
            putSerializable(ProductomerResultReceiver.PARAM_RESULT, resultSessionID)
            putString(SENDER, getMethodName())
            resultReceiver?.send(ProductomerResultReceiver.RESULT_CODE_OK, this)
        }
        if (!isCollectingData()) {
            notifyCollectingStateChanged(wSessionID)
            makeForeground(1, false)
        }
    }

    private fun handleStartCollectingData(wSessionID: IWorkSessionID, resultReceiver: ResultReceiver?) {
        val s = findSessionByID(wSessionID, resultReceiver) ?: return

        val bundle = Bundle()

        try {
            s.startCollectData()
            with(bundle) {
                putSerializable(ProductomerResultReceiver.PARAM_RESULT, wSessionID)
                putString(SENDER, getMethodName())
                resultReceiver?.send(ProductomerResultReceiver.RESULT_CODE_OK, this)
            }
            notifyCollectingStateChanged(wSessionID)
            makeForeground(1, true)
        } catch (e: IllegalStateException) {
            if (resultReceiver != null) {
                throwExceptionResult(e, "Can't start collecting data of session $wSessionID",
                        bundle, resultReceiver)
            }
        }
    }

    private fun handleWorkSessionList(resultReceiver: ResultReceiver) {
        updateSessionList()

        with(Bundle()) {
            putSerializable(ProductomerResultReceiver.PARAM_RESULT,
                    ArrayList(workSessionsMap.values.map { it.id }))
            putString(SENDER, getMethodName())
            resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_OK, this)
        }
    }

    private fun handleUpdateState() {
        updateSessionList()

        for ((k, v) in workSessionsMap) {
            notifyStateChanged(v.state, k)
        }
    }

    private fun findSessionByID(wSessionID: IWorkSessionID,
                                resultReceiver: ResultReceiver?): IWorkSession? {
        val wsession = workSessionsMap[wSessionID]

        return if (wsession != null) {
            wsession
        } else {
            if (resultReceiver != null) {
                throwExceptionResult(InvalidKeyException(), "DataSession $wSessionID not exists",
                        Bundle(), resultReceiver)
            }
            null
        }
    }

    private fun handleActivateWorkSession(wsessionid: IWorkSessionID, resultReceiver: ResultReceiver?) {
        val wsession = findSessionByID(wsessionid, resultReceiver)

        if (resultReceiver != null) {
            val res = if (wsession == null) InvalidKeyException("No session with ID=$wsessionid") else null
            val resultCode = if (res == null) ProductomerResultReceiver.RESULT_CODE_ERROR else ProductomerResultReceiver.RESULT_CODE_OK

            with(Bundle()) {
                putSerializable(ProductomerResultReceiver.PARAM_RESULT, res)
                putString(SENDER, getMethodName())
                resultReceiver.send(resultCode, this)
            }
        }

        if (wsession != null) {
            GlobalScope.launch {
                deactivateInfoSessions(skipSession = wsessionid)

                var result: IWorkSession.State = wsession.state
                if (result != IWorkSession.State.COLLECTING_DATA &&
                        result != IWorkSession.State.DISPLAYING_DATA) {
                    notifyStateChanged(IWorkSession.State.CONNECTING, wsessionid)
                    result = wsession.connect()
                }

                notifyStateChanged(result, wsessionid)
            }
        }
    }

    private fun notifyStateChanged(result: IWorkSession.State, wsessionid: IWorkSessionID) = with(Bundle()) {
            putInt(ServiceControl.PARAM.RESULT_TYPE.name, ServiceControl.RESULT_TYPE.STATE_CHANGED.ordinal)
            putInt(ServiceControl.PARAM.CONNECTION_STATE.name, result.ordinal)
            putSerializable(ServiceControl.PARAM.SESSION_ID.name, wsessionid)
            putString(SENDER, getMethodName())
            sendNotification(this)
        }


    private fun sendNotification(bundle: Bundle) =
            watchers.forEach { it.value.send(ProductomerResultReceiver.RESULT_CODE_OK, bundle) }

    private fun deactivateInfoSessions(skipSession: IWorkSessionID?) =
            workSessionsMap.filter { it.key != skipSession }.forEach { it.value.close() }

    private fun updateSessionList() {
        val bondedDevices = adapter?.bondedDevices ?: return

        val removedDevices = workSessionsMap
                .filter { !bondedDevices.contains(it.value.device) }
                .map { it.key }

        workSessionsMap.removeAll(removedDevices)

        val newDevices = bondedDevices.filter { btd ->
            workSessionsMap.filter { it.value.device == btd }.isNullOrEmpty()
        }
        newDevices
                .map { ProductomerWorkSession(it, this@Service) } // FakeWorkSession
                .forEach { workSessionsMap[it.id] = it }
    }

    private fun handleRegisterWatcher(resultReceiver: ResultReceiver, watcherID: Int) {
        watchers[watcherID] = resultReceiver

        with(Bundle()) {
            putInt(ServiceControl.PARAM.RESULT_TYPE.name, ServiceControl.RESULT_TYPE.STATE_CHANGED.ordinal)
            putInt(ServiceControl.PARAM.CONNECTION_STATE.name,
                    if (isCollectingData()) IWorkSession.State.COLLECTING_DATA.ordinal else IWorkSession.State.IDLE.ordinal)
            putSerializable(ServiceControl.PARAM.SESSION_ID.name, null)
            putString(SENDER, getMethodName())
            resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_OK, this)
        }
    }

    private fun handleUnRegisterWatcher(watcherID: Int) {
        watchers.remove(watcherID)
    }

    private fun isCollectingData(): Boolean =
            workSessionsMap.filter { it.value.state == IWorkSession.State.COLLECTING_DATA }.any()

    private fun handleCancelRemoving(cancelID: Int?, resultReceiver: ResultReceiver) {
        val bundle = Bundle()
        try {
            shaduler.cancelTaskByID(cancelID)

            bundle.apply {
                putSerializable(ProductomerResultReceiver.PARAM_RESULT, null)
                putString(SENDER, getMethodName())
            }
            resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_OK, bundle)
        } catch (ex: Exception) {
            throwExceptionResult(ex, "Can't cancel remove session wit ID=" + cancelID!!.toString(),
                    bundle, resultReceiver)
        }
    }

    private fun throwExceptionResult(ex: Exception, message: String, b: Bundle,
                                     resultReceiver: ResultReceiver) {
        Log.e(LOG_TAG, message, ex)

        b.apply {
            putSerializable(ProductomerResultReceiver.PARAM_EXCEPTION, ex)
            putString(SENDER, getMethodName())
        }

        resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_ERROR, b)
    }

    private fun handleRemoveSession(id: ISessionID?, onOperationFinished: ResultReceiver?,
                                    resultReceiver: ResultReceiver) {

        val bundle = Bundle()

        if (id == null) {
            throwExceptionResult(NullPointerException(), "Can't remove DataSession with ID=null",
                    bundle, resultReceiver)
            return
        }

        if (throwIllegalStorageState(noStorageMode || ROMode, "Storage not writable",
                        resultReceiver, bundle)) {
            return
        }

        val deleteTask = object : TaskShaduler.ShadulableTask(CANCEL_REMOVE_TIMEOUT) {

            @Throws(Exception::class)
            override fun execute(bundle: Bundle) {
                storage.removeSession(id)
            }

        }.setResultRessiver(onOperationFinished!!)

        try {
            shaduler.shaduleTask(deleteTask)
            bundle.apply {
                putSerializable(ProductomerResultReceiver.PARAM_RESULT, deleteTask.hashCode())
                putString(SENDER, getMethodName())
            }
            resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_OK, bundle)
        } catch (ex: Exception) {
            throwExceptionResult(ex, "Can't shadule remove DataSession ID=$id", bundle, resultReceiver)
        }
    }

    private fun handleSessionList(resultReceiver: ResultReceiver?) {
        if (resultReceiver == null) {
            return
        }

        val bundle = Bundle()

        if (throwIllegalStorageState(noStorageMode, "Storage not readable",
                        resultReceiver, bundle)) {
            return
        }

        try {
            val sessionList = storage.getSessionList()
            bundle.apply {
                putSerializable(ProductomerResultReceiver.PARAM_RESULT, sessionList)
                putString(SENDER, getMethodName())
            }

            resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_OK, bundle)
        } catch (ex: Exception) {
            throwExceptionResult(ex, "Failed to get session list", bundle, resultReceiver)
        }
    }

    private fun handleGetSession(id: ISessionID?, resultReceiver: ResultReceiver?) {
        if (resultReceiver == null) {
            return
        }

        if (id == null) {
            Log.e(LOG_TAG, "Null session ID %s")
            return
        }

        val bundle = Bundle()

        if (throwIllegalStorageState(noStorageMode, "Storage not readable", resultReceiver, bundle)) {
            return
        }

        try {
            val session = storage.GetSessionByID(id)
            bundle.apply {
                putSerializable(ProductomerResultReceiver.PARAM_RESULT, session)
                putString(SENDER, getMethodName())
            }

            resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_OK, bundle)
        } catch (ex: Exception) {
            throwExceptionResult(ex, "Failed to get session $id", bundle, resultReceiver)
        }
    }

    private fun throwIllegalStorageState(condition: Boolean, message: String,
                                         resultReceiver: ResultReceiver, bundle: Bundle): Boolean {
        if (condition) {
            bundle.apply {
                putSerializable(ProductomerResultReceiver.PARAM_EXCEPTION,
                        IllegalAccessException(message))
                putString(SENDER, getMethodName())
            }
            resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_ERROR, bundle)
        }
        return condition
    }

    fun saveDataSession(s: ISession?): ISessionID? = storage.SaveSession(s)

    private fun handleWriteSession(session: ISession?, resultReceiver: ResultReceiver) {
        if (session == null) {
            Log.e(LOG_TAG, "Attempt to save Null session")
            return
        }

        val bundle = Bundle()

        if (throwIllegalStorageState(noStorageMode || ROMode,
                        "Storage not writable", resultReceiver, bundle)) {
            return
        }

        try {
            val id = saveDataSession(session)
            bundle.apply {
                putSerializable(ProductomerResultReceiver.PARAM_RESULT, id)
                putString(SENDER, getMethodName())
            }

            resultReceiver.send(ProductomerResultReceiver.RESULT_CODE_OK, bundle)
        } catch (ex: Exception) {
            throwExceptionResult(ex, "Failed to write session $session", bundle, resultReceiver)
        }
    }

    fun sessionUpdated(workSession: IWorkSession) = with(Bundle()) {
        putInt(ServiceControl.PARAM.RESULT_TYPE.name, ServiceControl.RESULT_TYPE.DATA_UPDATED.ordinal)
        putSerializable(ServiceControl.PARAM.SESSION_ID.name, workSession.id)
        putSerializable(ServiceControl.PARAM.SESSION_DATA.name, workSession.rawdata)
        sendNotification(this)
    }

    fun notifySessionListChanged() = with(Bundle()) {
        putInt(ServiceControl.PARAM.RESULT_TYPE.name, ServiceControl.RESULT_TYPE.STORAGE_UPDATED.ordinal)
        sendNotification(this)
    }

    companion object {
        private const val SENDER = "Sender"
        private const val LOG_TAG = "Service"
        private const val CANCEL_REMOVE_TIMEOUT = 4500 // Snackbar.LENGTH_LONG + запас
    }
}

