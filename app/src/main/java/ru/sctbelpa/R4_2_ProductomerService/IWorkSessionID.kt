package ru.sctbelpa.R4_2_ProductomerService

import java.io.Serializable

interface IWorkSessionID : Serializable {
    val name: String
    val identity: String
}
