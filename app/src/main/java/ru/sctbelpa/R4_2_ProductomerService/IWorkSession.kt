package ru.sctbelpa.R4_2_ProductomerService

import android.bluetooth.BluetoothDevice
import ru.sctbelpa.R4_2_productomer.DataStorage.IRawValue
import java.io.IOException
import java.util.*

interface IWorkSession {
    enum class State { IDLE, ERROR, CONNECTING, DISPLAYING_DATA, COLLECTING_DATA }

    val device: BluetoothDevice
    val id: IWorkSessionID

    /** Текущай статус сессии */
    val state: State

    /** Данные, собраные за сессию */
    val rawdata: LinkedList<IRawValue>

    var enabledChannels: Array<Boolean>

    /** Синхронная операция подключения к РЧ */
    fun connect(): State

    /** Закрыть сессию, переходит в состояние IDLE */
    fun close()

    @Throws(IllegalStateException::class)
    fun startCollectData()

    @Throws(IllegalStateException::class, IOException::class)
    fun finalise(): ISessionID
}