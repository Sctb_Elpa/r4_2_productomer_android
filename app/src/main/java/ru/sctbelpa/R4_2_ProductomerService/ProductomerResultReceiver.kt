package ru.sctbelpa.R4_2_ProductomerService

import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException


/** ResultReceiver - это Parcelable, поэтому наследование от него кривое
 * нельзя просто взять и добавить парселизацию в этот класс, ибо конструкток из Parcelable в ResultReceiver
 * package-private. Невозможно будет его сконструировать здесь без очень грязных хаков
 */
class ProductomerResultReceiver(handler: Handler,
                                val Ressiver: ResultReceiverCallBack?) : ResultReceiver(handler) {

    interface ResultReceiverCallBack {
        fun onSuccess(data: Bundle?)
        fun onError(e: Exception?)
    }

    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
        if (Ressiver != null) {
            if (resultCode == RESULT_CODE_OK) {
                Ressiver.onSuccess(resultData)
            } else {
                Ressiver.onError(resultData?.getSerializable(PARAM_EXCEPTION) as Exception)
            }
        }
    }

    companion object {
        const val RESULT_CODE_OK = 1100
        const val RESULT_CODE_ERROR = 666
        const val PARAM_EXCEPTION = "exception"
        const val PARAM_RESULT = "result"

        suspend fun awaitCallback(block: (ResultReceiverCallBack) -> Unit): Bundle? =
                suspendCancellableCoroutine { cont ->
                    block(object : ResultReceiverCallBack {
                        override fun onSuccess(data: Bundle?) = cont.resume(data)
                        override fun onError(e: Exception?) {
                            e?.let { cont.resumeWithException(it) }
                        }
                    })
                }
    }

}

fun <T> Bundle?.getResult(): T? = this?.getSerializable(ProductomerResultReceiver.PARAM_RESULT) as T

fun <T> Bundle?.getResultKey(key: String): T? = this?.getSerializable(key) as T