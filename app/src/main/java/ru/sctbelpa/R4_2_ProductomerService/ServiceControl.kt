package ru.sctbelpa.R4_2_ProductomerService

class ServiceControl {

    enum class PARAM {
        RESULT_RECEIVER,
        SESSION_ID,
        SESSION_DATA,
        ON_OPERATION_FINISHED,
        CANCEL_ID,
        RESULT_TYPE,
        CONNECTION_STATE,
        WATCHER_ID,
        CHANEL_ENABLE_STATE,
        BUSY_STATE
    }

    enum class RESULT_TYPE {
        STATE_CHANGED,
        DATA_UPDATED,
        STORAGE_UPDATED
    }

    enum class ACTIONS {
        DATASESSION_LIST, // Получить список сохраненнных сессий
        GET_DATASESSION, // Получить сессию из хранилища
        SAVE_DATASESSION, // Записать сессию
        REMOVE_DATASESSION, // Удалить сессию
        REMOVE_DATASESSION_CANCEL, // Отмена удаления

        REGISTER_WATCHER, // добавить рессивер в всписок вотчеров состояния сервиса
        UNREGISTER_WATCHER, // отменить регистрацию вотчера

        ACTIVATE_WORK_SESSION, // Активировать рабочую сессию, начать пдключение и сбор данных
        FORCE_UPDATE_STATE, // приказ послать отправить всем зарегистрированным вотчерам новые данные
        WORK_SESSION_ID_LIST, // Получить список рабочих сессий
        START_COLLECTING_DATA, // начать сбор данных для вычиссления результата
        FINALISE_WORKSESSION, // остановить сбор данных, закончить сессию, сохранить результаты
        CHANEL_CONTROL, // разрешить/запретить каналы и
        FIRST_BUSY_SESSION, // получить ID занятой сессии (если их несколько - то первой)
        GET_WORK_SESSION_ENABLED_CHANNELS, // получить список активных каналов у рабочей сесси
        FINALISE_DISPLYING_SESSIONS, // закрыть сессии, которые не собирают данные
    }
}
