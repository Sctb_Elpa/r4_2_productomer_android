package ru.sctbelpa.MagickDelimitedProtobuf;

import java.io.IOException;

public class EndOfStreamException extends IOException {
    public EndOfStreamException(String reason) {
        super(reason);
    }
}