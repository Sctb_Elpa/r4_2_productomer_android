package ru.sctbelpa.MagickDelimitedProtobuf;

import android.support.annotation.NonNull;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

import java.io.IOException;
import java.io.OutputStream;

import ru.sktbelpa.protobuf_common.ProtobufDeviceCommon;

public class MagickDelimitedProtobufOutputStream extends OutputStream {
    public static final int MAGICK_VALUE = ProtobufDeviceCommon.INFO.MAGICK_VALUE;
    protected final OutputStream outputStream;

    public MagickDelimitedProtobufOutputStream(OutputStream outputStream) {
        super();
        this.outputStream = outputStream;
    }

    public int write(GeneratedMessageLite message) throws IOException {
        CodedOutputStream codedOutputStream = CodedOutputStream.newInstance(outputStream);
        int serializedSize = message.getSerializedSize();
        codedOutputStream.writeRawByte(MAGICK_VALUE);
        codedOutputStream.writeInt32NoTag(serializedSize);
        message.writeTo(codedOutputStream);
        codedOutputStream.flush();
        return serializedSize + CodedOutputStream.computeInt32SizeNoTag(serializedSize) + 1;
    }

    public int writeRawMsg(byte[] b) throws IOException {
        CodedOutputStream codedOutputStream = CodedOutputStream.newInstance(outputStream);
        codedOutputStream.writeRawByte(MAGICK_VALUE);
        codedOutputStream.writeInt32NoTag(b.length);
        codedOutputStream.write(b, 0, b.length);
        codedOutputStream.flush();
        return CodedOutputStream.computeInt32SizeNoTag(b.length) + b.length;
    }

    public static byte[] dress(byte[] data) throws IOException {
        int size_value_size = CodedOutputStream.computeInt32SizeNoTag(data.length);
        int size = size_value_size + data.length + 1;
        byte[] res = new byte[size];
        CodedOutputStream codedOutputStream = CodedOutputStream.newInstance(res);
        codedOutputStream.writeRawByte(MAGICK_VALUE);
        codedOutputStream.writeInt32NoTag(data.length);
        codedOutputStream.write(data, 0, data.length);
        codedOutputStream.flush();
        return res;
    }

    @Override
    public void write(int i) throws IOException {
        outputStream.write(i);
    }

    @Override
    public void write(@NonNull byte[] b) throws IOException {
        outputStream.write(b);
    }

    @Override
    public void write(@NonNull byte[] b, int off, int len) throws IOException {
        outputStream.write(b, off, len);
    }

    @Override
    public void close() throws IOException {
        outputStream.close();
        super.close();
    }
}
