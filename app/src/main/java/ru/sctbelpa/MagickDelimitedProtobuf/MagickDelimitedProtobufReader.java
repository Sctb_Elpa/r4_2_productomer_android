package ru.sctbelpa.MagickDelimitedProtobuf;


import com.google.protobuf.CodedInputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import ru.sktbelpa.protobuf_common.ProtobufDeviceCommon;

public class MagickDelimitedProtobufReader {
    public static final int MAX_MSG_SIZE = 1500;
    public static final int MIN_MSG_SIZE = 2; // TAG + value

    // protobuf-dependent names
    public static final String PARSE_FROM_METHOD_NAME = "parseFrom";
    public static final int MAGICK = ProtobufDeviceCommon.INFO.MAGICK_VALUE;

    private InputStreamRecycler dataRecycler;

    public void close() {
        try {
            dataRecycler.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dataRecycler = null;
    }

    private enum STATE {
        RECEIVING_MAGICK,
        RECEIVING_SIZE,
        RECEIVING_BODY
    }

    private enum EXCEPTION_CODE {
        MISSING_REQ_FIELDS, INPUT_ENDED_UNEXPECTLY,
        UNEXPECTED_TAG, INVALID_WIRE_TYPE, SIZE_LIMIT, NEGATIVE_SUBMESSAGE_SIZE, MALFORMED_VARIANT, ZERO_TAG
    }

    private static final Map<String, MagickDelimitedProtobufReader.EXCEPTION_CODE> exceptionMsgList;
    static {
        exceptionMsgList = new HashMap<>();
        exceptionMsgList.put(
                "Message was missing required fields.  (Lite runtime could not determine which fields were missing).",
                EXCEPTION_CODE.MISSING_REQ_FIELDS);
        exceptionMsgList.put(
                "While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.",
                EXCEPTION_CODE.INPUT_ENDED_UNEXPECTLY);
        exceptionMsgList.put(
                "Protocol message end-group tag did not match expected tag.",
                EXCEPTION_CODE.UNEXPECTED_TAG);
        exceptionMsgList.put(
                "Protocol message tag had invalid wire type.",
                EXCEPTION_CODE.INVALID_WIRE_TYPE);
        exceptionMsgList.put(
                "Protocol message contained an invalid tag (zero).",
                EXCEPTION_CODE.ZERO_TAG);
        exceptionMsgList.put(
                "Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.",
                EXCEPTION_CODE.SIZE_LIMIT);
        exceptionMsgList.put(
                "CodedInputStream encountered an embedded string or message which claimed to have negative size.",
                EXCEPTION_CODE.NEGATIVE_SUBMESSAGE_SIZE);
        exceptionMsgList.put(
                "CodedInputStream encountered a malformed varint.",
                EXCEPTION_CODE.MALFORMED_VARIANT);
    }

    public MagickDelimitedProtobufReader(InputStream inputStream) {
        dataRecycler = new InputStreamRecycler(inputStream);
    }

    private long readRawVariant64SlowPath(InputStream is) throws IOException {
        long result = 0L;

        for (int shift = 0; shift < 64; shift += 7) {
            int b = is.read();
            result |= (long) (b & 127) << shift;
            if ((b & 128) == 0) {
                return result;
            }
        }

        throw new InvalidProtocolBufferException(
                "CodedInputStream encountered a malformed variant.");
    }

    private long readSize() throws IOException {
        return readRawVariant64SlowPath(dataRecycler);
    }

    public <T extends GeneratedMessageLite> T read(Class<T> messageType)
            throws IOException, InterruptedException,
            ReflectiveOperationException {

        STATE state = STATE.RECEIVING_MAGICK;
        byte[] buf = null;
        Method parser = messageType.getMethod(PARSE_FROM_METHOD_NAME, CodedInputStream.class);

        while (true) {
            if (Thread.currentThread().isInterrupted())
                throw new InterruptedException("Thread was interrupted");

            switch (state) {
                case RECEIVING_MAGICK: {
                    int magick = dataRecycler.read();
                    if (magick == MAGICK) {
                        state = STATE.RECEIVING_SIZE;
                    } else {
                        dataRecycler.DropByte();
                        dataRecycler.Recycle();
                    }
                    break;
                }
                case RECEIVING_SIZE: {
                    int msg_size = (int) readSize();
                    if (msg_size < MIN_MSG_SIZE || msg_size > MAX_MSG_SIZE) {
                        dataRecycler.DropByte();
                        dataRecycler.Recycle();
                        state = STATE.RECEIVING_MAGICK;
                    } else {
                        buf = new byte[msg_size];
                        state = STATE.RECEIVING_BODY;
                    }
                    break;
                }
                case RECEIVING_BODY: {
                    int readed = dataRecycler.read(buf);
                    if (readed < buf.length) {
                        // ошибка, видимо это некорректный размер, попробуем распарсить хотябы то, что
                        // в буффере рецикла
                        dataRecycler.DropByte();
                        dataRecycler.Recycle();
                        state = STATE.RECEIVING_MAGICK;
                        continue;
                    }

                    try {
                        T msg = (T) parser.invoke(null, CodedInputStream.newInstance(buf));
                        dataRecycler.DropRecycled();
                        return msg;
                    } catch (InvocationTargetException e) {
                        EXCEPTION_CODE code = exceptionMsgList.get(e.getTargetException().getMessage());
                        if (code != null) {
                            dataRecycler.DropByte();
                            dataRecycler.Recycle();
                            state = STATE.RECEIVING_MAGICK;
                        } else {
                            throw e;
                        }
                    }
                }
            }
        }
    }
}
