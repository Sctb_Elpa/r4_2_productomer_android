package ru.sctbelpa.MagickDelimitedProtobuf;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

class InputStreamRecycler extends InputStream {

    private final List<Byte> recycleBuf = new ArrayList<Byte>();
    private int offset;
    private final InputStream BaseStream;

    public InputStreamRecycler(InputStream baseStream) {
        BaseStream = baseStream;
        offset = 0;
    }

    /**
     * Прочитать один байт из потока.
     * Если идет рециклинг, то сначало пытаемся читать из рецикленого, если оно кончилось,
     * то из BaseStream
     *
     * @return Байт
     * @throws IOException, если попытка чтения BaseStream неудачна
     */
    @Override
    public int read() throws IOException {
        synchronized (recycleBuf) {
            if (!recycleBuf.isEmpty() && (offset < recycleBuf.size())) {
                return recycleBuf.get(offset++);
            }

            int b = BaseStream.read();
            if (b < 0) {
                throw new EndOfStreamException("Input stream was cloased");
            }
            recycleBuf.add((byte) b);
            ++offset;
            return b;
        }
    }

    /**
     * Унечтожает все рецикленое
     */
    public void DropAllRecycled() {
        synchronized (recycleBuf) {
            recycleBuf.clear();
            offset = 0;
        }
    }

    /**
     * Унечтожить самый старый байт в буфере рецикла
     */
    public void DropByte() throws IOException {
        synchronized (recycleBuf) {
            if (recycleBuf.isEmpty()) {
                throw new IOException("Recycle buffer empty!");
            }
            recycleBuf.remove(0);
            if (offset > 0) {
                --offset;
            }
        }
    }

    /**
     * Начать рециклинг заново
     */
    public void Recycle() {
        offset = 0;
    }

    /**
     * Сбросить столько байт, сколько сейчас прочитано из буфера рецикла
     */
    public void DropRecycled() {
        synchronized (recycleBuf) {
            while (offset > 0) {
                recycleBuf.remove(0);
                --offset;
            }
        }
    }

    @Override
    public void close() throws IOException {
        BaseStream.close();
    }
}
