package ru.sctbelpa.MagickDelimitedProtobuf;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

import ru.sktbelpa.Productomer_2_test.TestMessages;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class MagickDelimitedProtobufOutputStreamTest {

    private ByteArrayOutputStream ostream;
    private MagickDelimitedProtobufOutputStream writer;

    @Before
    public void setUp() throws Exception {
        ostream = spy(ByteArrayOutputStream.class);
        writer = new MagickDelimitedProtobufOutputStream(ostream);
    }

    @Test
    public void testWriteNormal() throws Exception {
        TestMessages.SimpleMessage msg = TestMessages.SimpleMessage.newBuilder().setVal(42).build();
        int writen = writer.write(msg);

        verify(ostream, atLeast(1)).write(any(byte[].class), anyInt(), anyInt());
        assertTrue(msg.getSerializedSize() + 2 == writen);

        byte[] bytes = ostream.toByteArray();
        assertArrayEquals(msg.toByteArray(), Arrays.copyOfRange(bytes, 2, bytes.length));
    }

    @Test
    public void WriteVeryLongMsg() throws Exception {
        TestMessages.VeryLongMsg.Builder builder = TestMessages.VeryLongMsg.newBuilder();

        for (int i = 0; i < 128; ++i)
            builder.addV(i);

        TestMessages.VeryLongMsg msg = builder.build();

        int writen = writer.write(msg);
        assertEquals(msg.getSerializedSize() + 3, writen);

        byte[] bytes = ostream.toByteArray();
        assertArrayEquals(msg.toByteArray(), Arrays.copyOfRange(bytes, 3, bytes.length));
    }

    @Test
    public void testDress() throws Exception {
        TestMessages.SimpleMessage msg = TestMessages.SimpleMessage.newBuilder().setVal(42).build();
        writer.write(msg);

        byte[] dress = MagickDelimitedProtobufOutputStream.dress(msg.toByteArray());
        assertArrayEquals(ostream.toByteArray(), dress);
    }
}