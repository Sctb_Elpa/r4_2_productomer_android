package ru.sctbelpa.MagickDelimitedProtobuf;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Arrays;
import java.util.Random;

import ru.sctbelpa.TestUtils.Repeat;
import ru.sctbelpa.TestUtils.RepeatRule;
import ru.sktbelpa.Productomer_2_test.TestMessages;
import ru.sktbelpa.r4_2_prodm.ProtobufDevice0000E004;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(Enclosed.class)
public class MagickDelimitedProtobufReaderTest {

    public static class Suite1 {

        private PipedInputStream istream;
        private MagickDelimitedProtobufReader reader;
        private PipedOutputStream ostream = new PipedOutputStream();
        private MagickDelimitedProtobufOutputStream writer;

        @Before
        public void before() throws Exception {
            istream = new PipedInputStream(ostream);

            writer = new MagickDelimitedProtobufOutputStream(ostream);
            reader = new MagickDelimitedProtobufReader(istream);
        }

        @Test
        public void TestNormalBehavior() throws Exception {
            TestMessages.SimpleMessage msg = TestMessages.SimpleMessage.newBuilder().setVal(42).build();

            writer.write(msg);
            TestMessages.SimpleMessage read = reader.read(TestMessages.SimpleMessage.class);

            assertNotNull(read);
            assertEquals(msg.getVal(), read.getVal());
        }

        @Test(expected = EndOfStreamException.class)
        public void testInterruptedMessage() throws Exception {
            TestMessages.SimpleMessage msg = TestMessages.SimpleMessage.newBuilder().setVal(42).build();

            writer.writeRawMsg(Arrays.copyOfRange(msg.toByteArray(), 0, 1));
            writer.close();
            reader.read(TestMessages.SimpleMessage.class);
        }

        @Test
        public void testComplexSearchMessageInSpecial() throws Exception {
            TestMessages.ComplexMessage ref_msg = TestMessages.ComplexMessage.newBuilder()
                    .setV1(42)
                    .setV2(11)
                    .setV3(0)
                    .build();

            byte[] msg_d = MagickDelimitedProtobufOutputStream.dress(ref_msg.toByteArray());
            byte[] random_data = new byte[msg_d.length * 3];

            random_data[0] = 1;
            random_data[1] = 6;
            random_data[2] = -27;
            random_data[3] = 3;
            random_data[4] = 8;

            System.arraycopy(msg_d, 0, random_data, msg_d.length, msg_d.length);

            writer.write(random_data);
            writer.close();
            TestMessages.ComplexMessage msg = reader.read(TestMessages.ComplexMessage.class);

            assertNotNull(msg);
            assertEquals(ref_msg.getV1(), msg.getV1());
            assertEquals(ref_msg.getV2(), msg.getV2());
            assertEquals(ref_msg.getV3(), msg.getV3());
        }

        @Test(expected = EndOfStreamException.class)
        public void testFullyIncorrectMessage() throws Exception {
            byte[] data = new byte[]{-95, -21, -7, 2, 8, 42, 93, -88, 125};

            writer.write(data);
            writer.close();
            reader.read(TestMessages.ComplexMessage.class);
        }

        /**
         * 1. Создаём легитимное сообщение и буфер случайных чисел размером в три раза больше его.
         * 2. Копируем первый байт легитимного сообщения во второй байт рандомного
         * 3. Копируем все легитимное сообщение в середину рандомного
         * 4. Отправляем парсеру все рандомное сообщение
         * <p>
         * Что происходит?
         * Корректно обнаружен magick
         * Попытка парсинга размера может получиться достаточно большое число, но не больше лимита
         * Попытка прочитать из потока это большое количество байт заваливается т.к. достигнут конец потока
         * <p>
         * Нужно не выпадать с ошибкой сразу, а попытаться найти сообщение в этом "большом" куске
         */
        @Test
        public void testComplexSearchMessageInGadbrage() throws Exception {
            TestMessages.ComplexMessage ref_msg = TestMessages.ComplexMessage.newBuilder()
                    .setV1(42)
                    .setV2(11)
                    .setV3(0)
                    .build();

            byte[] msg_d = MagickDelimitedProtobufOutputStream.dress(ref_msg.toByteArray());
            byte[] random_data = new byte[msg_d.length * 3];
            new Random().nextBytes(random_data);

            random_data[1] = msg_d[0]; // add fake start

            System.arraycopy(msg_d, 0, random_data, msg_d.length, msg_d.length);

            writer.write(random_data);
            writer.close();
            TestMessages.ComplexMessage msg = reader.read(TestMessages.ComplexMessage.class);

            assertNotNull(msg);
            assertEquals(ref_msg.getV1(), msg.getV1());
            assertEquals(ref_msg.getV2(), msg.getV2());
            assertEquals(ref_msg.getV3(), msg.getV3());
        }

        @Test
        public void testGadbrageWithFakeMagickPreReal() throws Exception {
            TestMessages.ComplexMessage ref_msg = TestMessages.ComplexMessage.newBuilder()
                    .setV1(42)
                    .setV2(11)
                    .setV3(0)
                    .build();

            byte[] msg_d = MagickDelimitedProtobufOutputStream.dress(ref_msg.toByteArray());
            byte[] random_data = new byte[msg_d.length * 3];
            new Random().nextBytes(random_data);

            System.arraycopy(msg_d, 0, random_data, msg_d.length, msg_d.length);
            random_data[msg_d.length - 1] = msg_d[0];

            writer.write(random_data);
            writer.close();
            TestMessages.ComplexMessage msg = reader.read(TestMessages.ComplexMessage.class);

            assertNotNull(msg);
            assertEquals(ref_msg.getV1(), msg.getV1());
            assertEquals(ref_msg.getV2(), msg.getV2());
            assertEquals(ref_msg.getV3(), msg.getV3());
        }

        @Test
        public void findInInput() throws Exception {
            byte[] random_data = new byte[128 * 1024];
            new Random().nextBytes(random_data);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            reader.read(TestMessages.SimpleMessage.class);
                        } catch (EndOfStreamException ex) {
                            break;
                        } catch (Exception e) {
                            e.printStackTrace();
                            break;
                        }
                    }
                    reader.close();
                }
            }).start();

            try {
                writer.write(random_data);
            } catch (IOException ex) {
                assertEquals("Pipe closed", ex.getMessage());
            }
            writer.close();
        }

        @Test
        public void findInInputWithNormalEnd() throws Exception {

            final TestMessages.ComplexMessage orig_msg = TestMessages.ComplexMessage.newBuilder()
                    .setV1(1)
                    .setV2(2)
                    .setV3(42)
                    .build();

            byte[] random_data = new byte[1024];
            new Random().nextBytes(random_data);
            byte[] dress = MagickDelimitedProtobufOutputStream.dress(orig_msg.toByteArray());
            System.arraycopy(dress, 0, random_data, random_data.length - dress.length - 1,
                    dress.length);

            writer.write(random_data);
            writer.close();

            TestMessages.ComplexMessage msg = reader.read(TestMessages.ComplexMessage.class);

            assertNotNull(msg);
            assertTrue(msg.isInitialized());
            assertEquals(orig_msg.getV1(), msg.getV1());
            assertEquals(orig_msg.getV2(), msg.getV2());
            assertEquals(orig_msg.getV3(), msg.getV3());
        }

        @Test
        public void findOptionalInInputWithNormalEnd() throws Exception {
            final TestMessages.MessageWithOptionalField orig_msg = TestMessages.MessageWithOptionalField
                    .newBuilder()
                    .setReqVal1(42)
                    .setReqVal2(7)
                    .setOptVal(18)
                    .build();

            byte[] random_data = new byte[1024];
            new Random().nextBytes(random_data);
            byte[] dress = MagickDelimitedProtobufOutputStream.dress(orig_msg.toByteArray());
            System.arraycopy(dress, 0, random_data, random_data.length - dress.length - 1,
                    dress.length);

            writer.write(random_data);
            writer.close();

            TestMessages.MessageWithOptionalField msg =
                    reader.read(TestMessages.MessageWithOptionalField.class);

            assertNotNull(msg);
            assertTrue(msg.isInitialized());
            assertEquals(orig_msg.getReqVal1(), msg.getReqVal1());
            assertEquals(orig_msg.getReqVal2(), msg.getReqVal2());
            assertTrue(msg.hasOptVal());
            assertEquals(orig_msg.getOptVal(), msg.getOptVal());
        }

        @Test(expected = EndOfStreamException.class)
        public void InvalidMessage() throws Exception {

            final TestMessages.ComplexMessage orig_msg = TestMessages.ComplexMessage.newBuilder()
                    .setV1(1)
                    .setV2(2)
                    .setV3(42)
                    .build();

            writer.write(orig_msg);
            writer.close();

            reader.read(TestMessages.IncompatableMessage.class);
        }

        @Test(expected = EndOfStreamException.class)
        public void TestDataOnlyMagick() throws Exception {
            byte[] magicks = new byte[128];
            Arrays.fill(magicks, (byte) ProtobufDevice0000E004.INFO.MAGICK_VALUE);
            writer.write(magicks);
            writer.close();

            reader.read(TestMessages.SimpleMessage.class);
        }
    }

    public static class Suite2 {
        @Rule
        public RepeatRule repeatRule = new RepeatRule();

        @Test
        @Repeat(100)
        public void testSearchMessageInGadbrage() throws Exception {
            PipedOutputStream ostream = new PipedOutputStream();
            PipedInputStream istream = new PipedInputStream(ostream);
            MagickDelimitedProtobufOutputStream writer =
                    new MagickDelimitedProtobufOutputStream(ostream);
            MagickDelimitedProtobufReader reader = new MagickDelimitedProtobufReader(istream);

            int testValue = 42;
            byte[] msg_d = MagickDelimitedProtobufOutputStream.dress(
                    TestMessages.SimpleMessage.newBuilder()
                            .setVal(testValue).build().toByteArray());
            byte[] random_data = new byte[msg_d.length * 3];
            new Random().nextBytes(random_data);

            System.arraycopy(msg_d, 0, random_data, msg_d.length, msg_d.length);

            writer.write(random_data);
            writer.close();

            try {
                TestMessages.SimpleMessage msg = reader.read(TestMessages.SimpleMessage.class);
                assertNotNull(msg);
                assertEquals(testValue, msg.getVal());
                return;
            } catch (EndOfStreamException ex) {
                PipedOutputStream _ostream = new PipedOutputStream();
                PipedInputStream _istream = new PipedInputStream(_ostream);
                MagickDelimitedProtobufOutputStream _writer =
                        new MagickDelimitedProtobufOutputStream(_ostream);
                MagickDelimitedProtobufReader _reader = new MagickDelimitedProtobufReader(_istream);

                _writer.write(random_data);
                _writer.close();

                _reader.read(TestMessages.SimpleMessage.class);
            }
        }
    }
}