package ru.sctbelpa.R4_2_productomer.service;

import com.google.protobuf.AbstractMessageLite;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import ru.sctbelpa.MagickDelimitedProtobuf.MagickDelimitedProtobufOutputStream;
import ru.sktbelpa.Productomer_2_test.TestMessages;
import ru.sktbelpa.r4_2_prodm.ProtobufDevice0000E004;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DispatcherTest {

    ByteArrayOutputStream ostream = new ByteArrayOutputStream();

    @Mock
    InputStream istream;

    PipedOutputStream p_out = new PipedOutputStream();
    PipedInputStream p_in;

    Dispatcher dispatcher_w;
    Dispatcher dispatcher_r;
    ProtobufDevice0000E004.Request requesttemplate;

    static byte[] formatMessageByteArray(AbstractMessageLite msg) throws IOException {
        return MagickDelimitedProtobufOutputStream.dress(msg.toByteArray());
    }

    @Before
    public void prepare() throws IOException {
        MockitoAnnotations.initMocks(this);
        dispatcher_w = new Dispatcher(istream, ostream);
        requesttemplate = ProtobufDevice0000E004.Request.newBuilder()
                .setId(42)
                .setProtocolVersion(0)
                .setDeviceID(ProtobufDevice0000E004.INFO.ID_DISCOVER.getNumber())
                .build();

        p_in = new PipedInputStream(p_out);

        dispatcher_r = new Dispatcher(p_in, ostream);
    }

    @Test
    public void writeDataToOStream() throws Exception {
        ProductomerCommand command = spy(new ProductomerCommand(mock(Dispatcher.class),
                ProtobufDevice0000E004.Request.getDefaultInstance()));

        when(command.getRequest()).thenReturn(requesttemplate);
        when(command.getId()).thenReturn(42);
        dispatcher_w.executeCommand(command);

        Thread.sleep(Dispatcher.COMMAND_COOLDOWN * 5);
        assertTrue(dispatcher_w.getPendingCommands().contains(command));
        assertArrayEquals(ostream.toByteArray(), formatMessageByteArray(requesttemplate));
    }

    @Test
    public void testOnInputClose() throws Exception {
        dispatcher_r.start();
        p_in.close();
        dispatcher_r.join();
        assertFalse(dispatcher_r.isAlive());
    }

    @Test
    public void testInputIncorrectMessage() throws Exception {
        final TestMessages.SimpleMessage msg = TestMessages.SimpleMessage.newBuilder()
                .setVal(0)
                .build();
        dispatcher_r.start();
        msg.writeDelimitedTo(p_out);
        Thread.sleep(20);
        assertTrue(dispatcher_r.isAlive());
        p_out.close();
        dispatcher_r.join();
        assertFalse(dispatcher_r.isAlive());
    }

    @Test
    public void testKillDispatcher() throws Exception {
        dispatcher_r.start();
        Thread.sleep(20);
        dispatcher_r.interrupt();
        dispatcher_r.join();
        assertFalse(dispatcher_r.isAlive());
    }

    @Test
    public void testReceiveData() throws Exception {
        final ProtobufDevice0000E004.Response resp = ProtobufDevice0000E004.Response.newBuilder()
                .setId(42)
                .setDeviceID(ProtobufDevice0000E004.INFO.R4_2_PRODUCTOMER_ID_VALUE)
                .setProtocolVersion(ProtobufDevice0000E004.INFO.PROTOCOL_VERSION_VALUE)
                .setGlobalStatus(ProtobufDevice0000E004.STATUS.OK)
                .setTimestamp(System.currentTimeMillis())
                .build();

        ProductomerCommand command = new ProductomerCommand(dispatcher_r, requesttemplate);
        ISuccessCallback mockSuccess = mock(ISuccessCallback.class);
        command.onSuccess(mockSuccess);

        dispatcher_r.start();
        command.execute();
        p_out.write(formatMessageByteArray(resp));
        p_out.close();
        dispatcher_r.join();

        verify(mockSuccess).processResult(resp);
    }

    @Test
    public void testCheckPending() throws Exception {
        ProductomerCommand command = spy(new ProductomerCommand(mock(Dispatcher.class),
                ProtobufDevice0000E004.Request.getDefaultInstance()));

        when(command.getRequest()).thenReturn(requesttemplate);
        when(command.getId()).thenReturn(42);
        dispatcher_r.executeCommand(command);

        Thread.sleep(Dispatcher.COMMAND_COOLDOWN * 5);
        assertTrue(dispatcher_r.getPendingCommands().contains(command));
        assertTrue(dispatcher_r.isCommandPending(command));
    }

    @Test
    public void testCancelPendings() throws Exception {
        ProductomerCommand command = spy(new ProductomerCommand(mock(Dispatcher.class),
                ProtobufDevice0000E004.Request.getDefaultInstance()));

        when(command.getRequest()).thenReturn(requesttemplate);
        when(command.getId()).thenReturn(42);
        dispatcher_r.executeCommand(command);

        dispatcher_r.start();
        Thread.sleep(Dispatcher.COMMAND_COOLDOWN * 5);
        dispatcher_r.interrupt();
        dispatcher_r.join();

        verify(command).cancel();
    }
}