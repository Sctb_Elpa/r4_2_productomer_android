package ru.sctbelpa.R4_2_productomer.DataStorage;


import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class RawDataToResult {

    private static DataSession testSession = null;
    private static float compare_delta = 1e-7f;

    private static float[] exp_data = {
            7.176f,
            6.911f,
            6.611f,
            6.343f,
            6.092f,
            5.85f,
            5.616f,
            5.388f,
            5.165f,
            4.945f,
            4.726f,
            4.507f,
            4.287f,
            4.063f,
            3.835f,
            3.604f,
            3.37f,
            3.137f,
            2.908f,
            2.686f,
            2.47f,
            2.264f,
            2.069f,
            1.884f,
            1.71f,
            1.546f,
            1.394f,
            1.251f,
            1.117f,
            0.992f,
            0.876f,
            0.769f,
            0.67f,
            0.581f,
            0.499f,
            0.424f,
            0.359f,
            0.299f,
            0.248f,
            0.203f,
            0.165f,
            0.133f,
            0.103f,
            0.079f,
            0.054f,
            0.033f,
            0.008f,
            -0.034f,
            -0.113f,
            -0.246f,
            -0.462f,
            -0.795f,
            -1.23f,
            -1.734f,
            -2.299f,
            -2.917f,
            -3.594f,
            -4.336f,
            -5.153f,
            -6.048f,
    };

    @BeforeClass
    public static void beforeClass() {

        testSession = new DataSession("title", "desc");

        for (int i = 0; i < exp_data.length; ++i) {
            testSession.Add(new RawValue(1, new Date(i), exp_data[i], 0));
        }
    }

    @Test
    public void GetResults() throws Exception {

        final List<IResult> results = testSession.getResults();

        assertEquals(1, results.size());

        IResult r0 = results.get(0);

        assertNotNull(r0);
        assertEquals(1, r0.getChanelNumber());
        assertEquals(exp_data[0], r0.getStartTemperature(), compare_delta);
        assertEquals(exp_data[exp_data.length - 1], r0.getEndTemperature(), compare_delta);
        assertEquals(0.3446235276597226, r0.getTresult(), compare_delta);
        assertTrue(r0.getFalseKinksResults().isEmpty());
        assertEquals(1.0040973, r0.getAw(), compare_delta);
    }
}
