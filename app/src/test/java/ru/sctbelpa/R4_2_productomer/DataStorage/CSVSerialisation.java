package ru.sctbelpa.R4_2_productomer.DataStorage;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static ru.sctbelpa.R4_2_productomer.Utils.TimeUtilsKt.ms2S;

public class CSVSerialisation {

    private static final float[] testdata = new float[]{1.0f, 0.03f, -0.01f, -0.1f, -1.0f};

    private static final Pattern coma2dot = Pattern.compile("[,.]");
    public static DataSession testSession = null;
    public static DataSession incolmpleteSession = null;

    private PipedOutputStream ostream;
    private PipedInputStream istream;

    @Before
    public void setup() throws Exception {
        ostream = new PipedOutputStream();
        istream = new PipedInputStream(ostream, 4096);
    }

    @BeforeClass
    public static void setupClass() {
        testSession = new DataSession("Testtitle", "description");

        for (int i = 0; i < testdata.length; ++i) {
            for (int chanel = 1; chanel < 3; chanel++) {
                testSession.Add(new RawValue(chanel, new Date(TimeUnit.SECONDS.toMillis(i)),
                        testdata[i] + chanel * 0.1f, i + 1));
            }
        }

        incolmpleteSession = new DataSession("Incomplete", "no data to processing");

        for (int i = 0; i < 3; ++i) {
            for (int chanel = 1; chanel < 3; chanel++) {
                incolmpleteSession.Add(new RawValue(chanel, new Date(TimeUnit.SECONDS.toMillis(i)), 0, 0));
            }
        }
    }

    String replacecoma2Dot(String s) {
        final Matcher matcher = coma2dot.matcher(s);
        return matcher.replaceAll(".");
    }

    @Test
    public void serialise() throws Exception {

        final CSVSessionSerializer serialiser = new CSVSessionSerializer(ostream);
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(istream));

        serialiser.serialise(testSession);
        ostream.close();

        assertEquals("Сессия;" + testSession.getTitle(), bufferedReader.readLine());
        assertEquals("", bufferedReader.readLine());

        assertEquals("Описание;" + testSession.getDescription(), bufferedReader.readLine());
        assertEquals("Дата проведения;" +
                        serialiser.getConfig().getDateFormatter().format(testSession.getDate()),
                bufferedReader.readLine());
        assertEquals("Продолжительность эксперимента [с];" +
                        TimeUnit.MILLISECONDS.toSeconds(testSession.getSessionDuration()),
                bufferedReader.readLine());

        assertEquals("", bufferedReader.readLine());

        assertEquals("Результаты:", bufferedReader.readLine());
        for (final IResult res : testSession.getResults()) {
            assertEquals("Канал " + res.getChanelNumber(), bufferedReader.readLine());
            assertEquals(String.format("Температура в точке перегиба [°С];%f", res.getTresult()),
                    bufferedReader.readLine());
            assertEquals(String.format("Активность воды (aW);%g", res.getAw()),
                    bufferedReader.readLine());
            assertEquals("", bufferedReader.readLine());
        }

        assertEquals("", bufferedReader.readLine());

        assertEquals("Экспериментальные данные:", bufferedReader.readLine());
        assertEquals("Штамп времени;Канал;Температура [°С];Частота [Гц]",
                bufferedReader.readLine());

        assertEquals("1970-01-01 03:00:00.000;1;1,100000;1,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:00.000;2;1,200000;1,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:01.000;1;0,130000;2,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:01.000;2;0,230000;2,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:02.000;1;0,090000;3,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:02.000;2;0,190000;3,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:03.000;1;0,000000;4,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:03.000;2;0,100000;4,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:04.000;1;-0,900000;5,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:04.000;2;-0,800000;5,000000", bufferedReader.readLine());
        assertNull(bufferedReader.readLine());
    }

    @Test
    public void serialiseIncomplete() throws Exception {

        final CSVSessionSerializer serialiser = new CSVSessionSerializer(ostream);
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(istream));

        serialiser.serialise(incolmpleteSession);
        ostream.close();

        assertEquals("Сессия;" + incolmpleteSession.getTitle(), bufferedReader.readLine());
        assertEquals("", bufferedReader.readLine());

        assertEquals("Описание;" + incolmpleteSession.getDescription(), bufferedReader.readLine());
        assertEquals("Дата проведения;" +
                        serialiser.getConfig().getDateFormatter().format(incolmpleteSession.getDate()),
                bufferedReader.readLine());
        assertEquals("Продолжительность эксперимента [с];" +
                        TimeUnit.MILLISECONDS.toSeconds(incolmpleteSession.getSessionDuration()),
                bufferedReader.readLine());

        assertEquals("", bufferedReader.readLine());

        assertEquals("Результаты:", bufferedReader.readLine());
        for (final IResult res : incolmpleteSession.getResults()) {
            assertEquals("Канал " + res.getChanelNumber(), bufferedReader.readLine());
            assertEquals("Некоректная измерительная сессия, результат отсутствует", bufferedReader.readLine());
            assertEquals("", bufferedReader.readLine());
        }

        assertEquals("", bufferedReader.readLine());

        assertEquals("Экспериментальные данные:", bufferedReader.readLine());
        assertEquals("Штамп времени;Канал;Температура [°С];Частота [Гц]",
                bufferedReader.readLine());

        assertEquals("1970-01-01 03:00:00.000;1;0,000000;0,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:00.000;2;0,000000;0,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:01.000;1;0,000000;0,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:01.000;2;0,000000;0,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:02.000;1;0,000000;0,000000", bufferedReader.readLine());
        assertEquals("1970-01-01 03:00:02.000;2;0,000000;0,000000", bufferedReader.readLine());
        assertNull(bufferedReader.readLine());
    }

    @Test
    public void deserialise() throws Exception {
        final PrintWriter printer = new PrintWriter(ostream);
        final CSVgeneratorConfig cfg = new CSVgeneratorConfig();
        final CSVSessionParser parcer = new CSVSessionParser(istream, cfg);

        printer.printf(cfg.getTitleFormat(), cfg.getCVSplitter(), testSession.getTitle());
        printer.println();
        printer.printf(cfg.getDescriptionFormat(), cfg.getCVSplitter(), testSession.getDescription());
        printer.printf(cfg.getDateFormat(), cfg.getCVSplitter(),
                cfg.getDateFormatter().format(testSession.getDate()));
        printer.printf(cfg.getDurationformat(), cfg.getCVSplitter(), ms2S(testSession.getSessionDuration()));
        printer.println();
        printer.println(cfg.getResultsFormat());
        for (int i = 0; i < 2; ++i) {
            final IResult result = testSession.getResults().get(i);

            printer.printf(cfg.getChanelIDFormat(), result.getChanelNumber());
            printer.printf(cfg.getTresultFormat(), cfg.getCVSplitter(), result.getTresult());
            printer.printf(cfg.getAWResultFormat(), cfg.getCVSplitter(), result.getAw());
            printer.println();
        }
        printer.println();
        printer.println(cfg.getExperimentDataFormat());
        printer.println(cfg.getDataTableHeaderFormat());

        for (int i = 0; i < testdata.length; ++i) {
            for (int chanel = 1; chanel < 3; chanel++) {
                printer.printf(cfg.getRawValueFormat(),
                        cfg.getDateFormatter().format(new Date(TimeUnit.SECONDS.toMillis(i))),
                        chanel, testdata[i] + chanel * 0.1f, (float) i + 1);
            }
        }

        printer.close();

        ISession rs = parcer.parse();

        assertEquals(testSession, rs);
    }

    @Test
    public void deserialiseIncoplete() throws Exception {
        final PrintWriter printer = new PrintWriter(ostream);
        final CSVgeneratorConfig cfg = new CSVgeneratorConfig();
        final CSVSessionParser parcer = new CSVSessionParser(istream, cfg);

        printer.printf(cfg.getTitleFormat(), cfg.getCVSplitter(), incolmpleteSession.getTitle());
        printer.println();
        printer.printf(cfg.getDescriptionFormat(), cfg.getCVSplitter(), incolmpleteSession.getDescription());
        printer.printf(cfg.getDateFormat(), cfg.getCVSplitter(),
                cfg.getDateFormatter().format(incolmpleteSession.getDate()));
        printer.printf(cfg.getDurationformat(), cfg.getCVSplitter(), ms2S(incolmpleteSession.getSessionDuration()));
        printer.println();
        printer.println(cfg.getResultsFormat());
        for (int i = 0; i < 2; ++i) {
            final IResult result = incolmpleteSession.getResults().get(i);

            printer.printf(cfg.getChanelIDFormat(), result.getChanelNumber());
            printer.println(cfg.getNoResultAvailable());
            printer.println();
        }
        printer.println();
        printer.println(cfg.getExperimentDataFormat());
        printer.println(cfg.getDataTableHeaderFormat());

        for (int i = 0; i < 3; ++i) {
            for (int chanel = 1; chanel < 3; chanel++) {
                printer.printf(cfg.getRawValueFormat(),
                        cfg.getDateFormatter().format(new Date(TimeUnit.SECONDS.toMillis(i))),
                        chanel, 0F, 0F);
            }
        }

        printer.close();

        ISession rs = parcer.parse();

        assertEquals(incolmpleteSession, rs);
    }

    @Test
    public void deSerialiseFloatDot() throws Exception {
        final PrintWriter printer = new PrintWriter(ostream);
        final CSVgeneratorConfig cfg = new CSVgeneratorConfig();
        final CSVSessionParser parcer = new CSVSessionParser(istream, cfg);

        printer.printf(cfg.getTitleFormat(), cfg.getCVSplitter(), testSession.getTitle());
        printer.println();
        printer.printf(cfg.getDescriptionFormat(), cfg.getCVSplitter(), testSession.getDescription());
        printer.printf(cfg.getDateFormat(), cfg.getCVSplitter(),
                cfg.getDateFormatter().format(testSession.getDate()));
        printer.print(replacecoma2Dot(String.format(
                cfg.getDurationformat(), cfg.getCVSplitter(),
                ms2S(testSession.getSessionDuration()))));
        printer.println();
        printer.println(cfg.getResultsFormat());
        for (int i = 0; i < 2; ++i) {
            final IResult result = testSession.getResults().get(i);

            printer.printf(cfg.getChanelIDFormat(), result.getChanelNumber());
            printer.printf(replacecoma2Dot(String.format(
                    cfg.getTresultFormat(), cfg.getCVSplitter(), result.getTresult())));
            printer.printf(replacecoma2Dot(String.format(
                    cfg.getAWResultFormat(), cfg.getCVSplitter(), result.getAw())));
            printer.println();
        }
        printer.println();
        printer.println(cfg.getExperimentDataFormat());
        printer.println(cfg.getDataTableHeaderFormat());

        for (int i = 0; i < testdata.length; ++i) {
            for (int chanel = 1; chanel < 3; chanel++) {
                printer.printf(replacecoma2Dot(String.format(cfg.getRawValueFormat(),
                        cfg.getDateFormatter().format(new Date(TimeUnit.SECONDS.toMillis(i))),
                        chanel, testdata[i] + chanel * 0.1f, (float) i + 1)));
            }
        }

        printer.close();

        ISession rs = parcer.parse();

        assertEquals(testSession, rs);
    }

    @Test
    public void serialisationCycle() throws Exception {
        final CSVgeneratorConfig cfg = new CSVgeneratorConfig();
        final CSVSessionSerializer serialiser = new CSVSessionSerializer(ostream, cfg);
        final CSVSessionParser parcer = new CSVSessionParser(istream, cfg);

        serialiser.serialise(testSession);
        ostream.close();

        ISession rs = parcer.parse();

        assertEquals(testSession, rs);
    }

    @Test
    public void serialisationCycleIncompleteSession() throws Exception {
        final CSVgeneratorConfig cfg = new CSVgeneratorConfig();
        final CSVSessionSerializer serialiser = new CSVSessionSerializer(ostream, cfg);
        final CSVSessionParser parcer = new CSVSessionParser(istream, cfg);

        serialiser.serialise(incolmpleteSession);
        ostream.close();

        ISession rs = parcer.parse();

        assertEquals(incolmpleteSession, rs);
    }
}
