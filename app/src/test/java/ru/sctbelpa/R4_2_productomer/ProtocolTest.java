package ru.sctbelpa.R4_2_productomer;

import org.junit.Test;

import java.util.Random;

import ru.sktbelpa.r4_2_prodm.ProtobufDevice0000E004;

import static org.junit.Assert.assertEquals;

// Эти тесты запускать на этапе написания кода
public class ProtocolTest {
    @Test
    public void create_messageObjects() throws Exception {
        System.out.println("create_messageObjects");
        ProtobufDevice0000E004.Request request =
                ProtobufDevice0000E004.Request.getDefaultInstance();
        ProtobufDevice0000E004.Response response =
                ProtobufDevice0000E004.Response.getDefaultInstance();
    }

    @Test
    public void test_pingRequest() throws Exception {
        System.out.println("test_pingRequest");
        final Random id = new Random();

        ProtobufDevice0000E004.Request request = ProtobufDevice0000E004.Request.newBuilder()
                .setId(id.nextInt())
                .setDeviceID(ProtobufDevice0000E004.INFO.ID_DISCOVER.getNumber())
                .setProtocolVersion(0)
                .build();
        byte [] result = request.toByteArray();

        ProtobufDevice0000E004.Request res = ProtobufDevice0000E004.Request.parseFrom(result);

        assertEquals(request.getId(), res.getId());
        assertEquals(request.getDeviceID(), res.getDeviceID());
        assertEquals(request.getProtocolVersion(), res.getProtocolVersion());
    }
}