package ru.sctbelpa.R4_2_productomer;


import org.junit.Test;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Timer;
import java.util.TimerTask;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestStreams {

    @Test
    public void testAwaitOnRead() throws Exception {
        final PipedOutputStream ostream = new PipedOutputStream();
        final PipedInputStream istream = new PipedInputStream(ostream);
        final int delay = 10;

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    ostream.write(42);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }, delay);

        assertEquals(0, istream.available());

        long start = System.currentTimeMillis();
        assertEquals(42, istream.read());
        assertTrue(System.currentTimeMillis() - start >= delay);
    }
}
