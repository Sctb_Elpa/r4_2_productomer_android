package ru.sctbelpa.R4_2_productomer.DataStorage;

import android.Manifest;
import android.content.Context;
import android.os.Environment;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class DataStorageTest {

    /**
     * оно в упор не работает на моём планшете, если не заданы оба правила (внимательнее!)
     */
    @Rule
    public GrantPermissionRule WriteRule =
            GrantPermissionRule.grant(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    @Rule
    public GrantPermissionRule ReadRule =
            GrantPermissionRule.grant(Manifest.permission.READ_EXTERNAL_STORAGE);

    private static final String test_dir = Environment.getExternalStorageDirectory() + "/csvTestDir";
    private static final int testItems = 10;
    private static final int testItemscorrect = 5;

    private Context instrumentationCtx;

    @Before
    public void setup() {
        instrumentationCtx = InstrumentationRegistry.getContext();
    }

    @BeforeClass
    public static void setupClass() throws IOException {
        File testdir = new File(test_dir);

        testdir.mkdir();

        for (int i = 0; i < testItemscorrect; ++i) {
            GenerateTestFile(testdir, String.valueOf(i) + ".buildcsv");
        }

        for (int i = 0; i < testItems - testItemscorrect; ++i) {
            GenerateEmptyFile(testdir, String.valueOf(i));
        }
    }

    static public boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    @AfterClass
    public static void teardownClass() throws IOException {
        File testdir = new File(test_dir);
        deleteDirectory(testdir);
    }

    private static void GenerateTestFile(File testdir, String s) throws IOException {
        File f = new File(testdir, s);
        f.createNewFile();

        FileOutputStream os = new FileOutputStream(f);
        os.write(s.getBytes());
    }

    private static void GenerateEmptyFile(File testdir, String s) throws IOException {
        File f = new File(testdir, s);
        f.createNewFile();
    }

    /**
     * Простой тест на возможность создавать файлы на флешке
     */
    @Test
    public void createTestFile() throws Exception {
        File f = new File(Environment.getExternalStorageDirectory() + "/test");
        f.createNewFile();
        f.delete();
    }

    // Тест проверяет, что корректно количество читается без падений
    @Test
    public void getStoredSessionCount() throws Exception {

        IDataStorage storage = new CSVDataStorage(test_dir);

        assertEquals(testItemscorrect, storage.getSessionCount());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getSessionNegative() throws Exception {
        IDataStorage storage = new CSVDataStorage(test_dir);

        storage.GetSessionByID(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getSessionOverflow() throws Exception {
        IDataStorage storage = new CSVDataStorage(test_dir);

        int element_count = storage.getSessionCount();
        storage.GetSessionByID(element_count);
    }

    @Test
    public void SaveSessions() throws Exception {
        IDataStorage storage = new CSVDataStorage(test_dir);

        int element_count = storage.getSessionCount();

        DataSession s = new DataSession("Test1", "t");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        for (int i = 0; i < 10; ++i) {
            s.Add(new RawValue(0, c.getTime(), i, i * 2));
            c.add(Calendar.SECOND, 1);
        }

        storage.SaveSession(s);

        assertEquals(element_count + 1, storage.getSessionCount());

        ISession ss = storage.GetSessionByID(element_count);

        assertEquals(s, ss);
    }
}