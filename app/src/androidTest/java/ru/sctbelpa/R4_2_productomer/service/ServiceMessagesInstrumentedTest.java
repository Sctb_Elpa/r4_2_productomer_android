package ru.sctbelpa.R4_2_productomer.service;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;
import android.util.Pair;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import ru.sktbelpa.r4_2_prodm.ProtobufDevice0000E004;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(AndroidJUnit4.class)
public class ServiceMessagesInstrumentedTest {

    private boolean CompareCoeffs(@NonNull ProtobufDevice0000E004.TCoefficientsSet expected,
                                  @NonNull ProtobufDevice0000E004.TCoefficientsGet actual) {
        boolean res = true;
        if (expected.hasT0())
            res &= actual.getT0() == expected.getT0();
        if (expected.hasF0())
            res &= actual.getF0() == expected.getF0();
        if (expected.hasC0())
            res &= actual.getC0() == expected.getC0();
        if (expected.hasC1())
            res &= actual.getC1() == expected.getC1();
        if (expected.hasC2())
            res &= actual.getC2() == expected.getC2();

        return res;
    }

    @ClassRule
    public static final ServiceTestRule mServiceRule = new ServiceTestRule();
    private static ProductomerService service;
    private final int TIMEOUT = 1000;
    private static final String LOG_TAG = "TEST";

    @BeforeClass
    public static void BeforeClass() throws TimeoutException {
        // Create the service Intent.
        Intent serviceIntent = new Intent(InstrumentationRegistry.getTargetContext(),
                ProductomerService.class);
        IBinder binder = mServiceRule.bindService(serviceIntent);

        service = ((ProductomerService.LocalBinder) binder).getService();

        BluetoothDevice TestDevice = TestUtils.findTestDevise(
                service.getAdapter().getBondedDevices(), TestUtils.testMAC);
        service.setTargetServer(TestDevice, ProductomerService.defaultUUID);
    }

    @AfterClass
    public static void AfterClass() throws InterruptedException {
        service.dispatcher.interrupt();
        service.dispatcher.join();
    }

    @NonNull
    private IFailCallback getErrorCallback(final Object event) {
        return new IFailCallback() {
            @Override
            public void processError(IProductomerCommand command, Exception ex) {
                Log.w(LOG_TAG, command.toString() + " Error: " + ex.getMessage());
                synchronized (event) {
                    event.notify();
                }
            }
        };
    }

    @NonNull
    private ISuccessCallback getSuccessCallback(final Object event) {
        return new ISuccessCallback() {
            @Override
            public void processResult(ProtobufDevice0000E004.Response response) {
                assertEquals(ProtobufDevice0000E004.INFO.R4_2_PRODUCTOMER_ID_VALUE,
                        response.getDeviceID());
                assertEquals(ProtobufDevice0000E004.STATUS.OK, response.getGlobalStatus());
                assertEquals(ProtobufDevice0000E004.INFO.PROTOCOL_VERSION_VALUE,
                        response.getProtocolVersion());
                synchronized (event) {
                    event.notify();
                }
            }
        };
    }

    @Test
    public void testPing() throws Exception {
        final Object event = new Object();

        CommandBuilder commandBuilder = service.newCommandBuilder();

        commandBuilder.successCallback = spy(getSuccessCallback(event));
        commandBuilder.failCallback = spy(getErrorCallback(event));

        IProductomerCommand ping_cmd = commandBuilder.build(
                ProtobufDevice0000E004.INFO.R4_2_PRODUCTOMER_ID_VALUE);

        ping_cmd.setTimeout(TIMEOUT).execute();

        synchronized (event) {
            event.wait();
        }

        verify(commandBuilder.failCallback, never()).processError(any(), any(Exception.class));
        verify(commandBuilder.successCallback).processResult(
                any(ProtobufDevice0000E004.Response.class));
    }

    @Test
    public void testScan() throws Exception {
        final Object event = new Object();

        CommandBuilder commandBuilder = service.newCommandBuilder();

        commandBuilder.successCallback = spy(getSuccessCallback(event));
        commandBuilder.failCallback = spy(getErrorCallback(event));

        IProductomerCommand ping_cmd = commandBuilder.buildBroadcast();

        ping_cmd.setTimeout(TIMEOUT).execute();
        synchronized (event) {
            event.wait();
        }

        verify(commandBuilder.failCallback, never()).processError(any(), any(Exception.class));
        verify(commandBuilder.successCallback).processResult(
                any(ProtobufDevice0000E004.Response.class));
    }

    @Test
    public void testNormalSetSettings() throws Exception {
        final Object event = new Object();

        final ProtobufDevice0000E004.WriteSettingsReq.Builder writeSettingsBuilder =
                ProtobufDevice0000E004.WriteSettingsReq.newBuilder()
                        .setSerial(42)
                        .setChanel1MesureTimeMs(1000)
                        .setChanel2MesureTimeMs(995)
                        .setFref(16000005)
                        .setChanel1Enabled(true)
                        .setChanel2Enabled(false)
                        .setChanel1ShowFreq(false)
                        .setChanel1ShowFreq(true)
                        .setChanel1Coefficients(ProtobufDevice0000E004.TCoefficientsSet.newBuilder()
                                .setT0(195).setF0(312)
                                .setC0(1.0F).setC1(2.0F).setC2(3.0F))
                        .setChanel2Coefficients(ProtobufDevice0000E004.TCoefficientsSet.newBuilder()
                                .setT0(0.9F).setF0(3.5F)
                                .setC0(1.0F).setC1(2.0F).setC2(3.0F));

        final CommandBuilder commandBuilder = service.newCommandBuilder()
                .settingsOperation(writeSettingsBuilder);

        commandBuilder.successCallback = spy(new ISuccessCallback() {
            @Override
            public void processResult(ProtobufDevice0000E004.Response response) {
                assertTrue(response.hasSettings());
                assertEquals(ProtobufDevice0000E004.STATUS.OK, response.getGlobalStatus());

                ProtobufDevice0000E004.SettingsResponse settings = response.getSettings();

                assertEquals(writeSettingsBuilder.getSerial(), settings.getSerial());
                assertEquals(writeSettingsBuilder.getChanel1MesureTimeMs(), settings.getChanel1MesureTimeMs());
                assertEquals(writeSettingsBuilder.getChanel2MesureTimeMs(), settings.getChanel2MesureTimeMs());
                assertEquals(writeSettingsBuilder.getFref(), settings.getFref());
                assertEquals(writeSettingsBuilder.getChanel1Enabled(), settings.getChanel1Enabled());
                assertEquals(writeSettingsBuilder.getChanel2Enabled(), settings.getChanel2Enabled());
                assertEquals(writeSettingsBuilder.getChanel1ShowFreq(), settings.getChanel1ShowFreq());
                assertEquals(writeSettingsBuilder.getChanel2ShowFreq(), settings.getChanel2ShowFreq());

                assertTrue(CompareCoeffs(writeSettingsBuilder.getChanel1Coefficients(), settings.getChanel1Coefficients()));
                assertTrue(CompareCoeffs(writeSettingsBuilder.getChanel2Coefficients(), settings.getChanel2Coefficients()));

                synchronized (event) {
                    event.notify();
                }
            }
        });
        commandBuilder.failCallback = spy(getErrorCallback(event));

        IProductomerCommand cmd = commandBuilder.buildBroadcast();

        cmd.setTimeout(TIMEOUT).execute();
        synchronized (event) {
            event.wait();
        }

        verify(commandBuilder.failCallback, never()).processError(any(), any(Exception.class));
        verify(commandBuilder.successCallback).processResult(
                any(ProtobufDevice0000E004.Response.class));
    }

    abstract class settingsRequestBuilder {
        abstract ProtobufDevice0000E004.WriteSettingsReq.Builder build();

        ProtobufDevice0000E004.WriteSettingsReq.Builder newBuilder() {
            return ProtobufDevice0000E004.WriteSettingsReq.newBuilder();
        }
    }

    private Pair<settingsRequestBuilder, ISuccessCallback>
    createBuilderVerificator(final String field, final Object value, Class arg_class,
                             final boolean isCorrect) {
        final Method setter, getter;
        try {
            setter = ProtobufDevice0000E004.WriteSettingsReq.Builder.class.getMethod(
                    "set" + field, arg_class);
            getter = ProtobufDevice0000E004.SettingsResponse.class.getMethod("get" + field);
        } catch (NoSuchMethodException e) {
            return null;
        }

        return new Pair<>(new settingsRequestBuilder() {
            @Override
            public ProtobufDevice0000E004.WriteSettingsReq.Builder build() {
                try {
                    return (ProtobufDevice0000E004.WriteSettingsReq.Builder) setter.invoke(
                            newBuilder(), value);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }, response -> {
            ProtobufDevice0000E004.SettingsResponse settings = response.getSettings();
            try {
                if (isCorrect)
                    assertEquals(field, value, getter.invoke(settings));
                else
                    assertNotEquals(field, value, getter.invoke(settings));
            } catch (Exception ex) {
                assertNotNull(null);
            }
        });
    }

    @Test
    public void testIncorrectSettings() throws Exception {
        final Object event = new Object();
        IFailCallback errCallback = spy(getErrorCallback(event));
        final String T1Coeffs = "Chanel1Coefficients";
        final String T2Coeffs = "Chanel2Coefficients";

        List<Pair<settingsRequestBuilder, ISuccessCallback>> tests = Arrays.asList(

                createBuilderVerificator("Chanel1MesureTimeMs", 0, int.class, false),
                createBuilderVerificator("Chanel1MesureTimeMs", 100, int.class, true),
                createBuilderVerificator("Chanel1MesureTimeMs", 5001, int.class, false),

                createBuilderVerificator("Chanel2MesureTimeMs", 0, int.class, false),
                createBuilderVerificator("Chanel2MesureTimeMs", 100, int.class, true),
                createBuilderVerificator("Chanel2MesureTimeMs", 5001, int.class, false),

                createBuilderVerificator("Fref", -1, int.class, false),
                createBuilderVerificator("Fref", 16000000 + 499, int.class, true),
                createBuilderVerificator("Fref", 16000000 - 499, int.class, true),
                createBuilderVerificator("Fref", Integer.MAX_VALUE, int.class, false),

                createBuilderVerificator(T1Coeffs,
                        ProtobufDevice0000E004.TCoefficientsSet.newBuilder().setF0(Float.NaN).setT0(0)
                                .setC0(0F).setC1(0F).setC2(0F),
                        ProtobufDevice0000E004.TCoefficientsSet.Builder.class, false),
                createBuilderVerificator(T1Coeffs,
                        ProtobufDevice0000E004.TCoefficientsSet.newBuilder().setF0(0F).setT0(Float.NaN)
                                .setC0(0F).setC1(0F).setC2(0F),
                        ProtobufDevice0000E004.TCoefficientsSet.Builder.class, false),
                createBuilderVerificator(T1Coeffs,
                        ProtobufDevice0000E004.TCoefficientsSet.newBuilder().setF0(0F).setT0(0)
                                .setC0(Float.NaN).setC1(0F).setC2(0F),
                        ProtobufDevice0000E004.TCoefficientsSet.Builder.class, false),
                createBuilderVerificator(T1Coeffs,
                        ProtobufDevice0000E004.TCoefficientsSet.newBuilder().setF0(0F).setT0(0)
                                .setC0(0F).setC1(Float.NaN).setC2(0F),
                        ProtobufDevice0000E004.TCoefficientsSet.Builder.class, false),
                createBuilderVerificator(T1Coeffs,
                        ProtobufDevice0000E004.TCoefficientsSet.newBuilder().setF0(0F).setT0(0)
                                .setC0(0F).setC1(0F).setC2(Float.NaN),
                        ProtobufDevice0000E004.TCoefficientsSet.Builder.class, false),
                createBuilderVerificator(T1Coeffs,
                        ProtobufDevice0000E004.TCoefficientsSet.newBuilder().setF0(0F).setT0(0)
                                .setC0(1F).setC1(0F).setC2(0F),
                        ProtobufDevice0000E004.TCoefficientsSet.Builder.class, false),

                createBuilderVerificator(T2Coeffs,
                        ProtobufDevice0000E004.TCoefficientsSet.newBuilder().setF0(Float.NaN).setT0(0)
                                .setC0(0F).setC1(0F).setC2(0F),
                        ProtobufDevice0000E004.TCoefficientsSet.Builder.class, false),
                createBuilderVerificator(T2Coeffs,
                        ProtobufDevice0000E004.TCoefficientsSet.newBuilder().setF0(0F).setT0(Float.NaN)
                                .setC0(0F).setC1(0F).setC2(0F),
                        ProtobufDevice0000E004.TCoefficientsSet.Builder.class, false),
                createBuilderVerificator(T2Coeffs,
                        ProtobufDevice0000E004.TCoefficientsSet.newBuilder().setF0(0F).setT0(0)
                                .setC0(Float.NaN).setC1(0F).setC2(0F),
                        ProtobufDevice0000E004.TCoefficientsSet.Builder.class, false),
                createBuilderVerificator(T2Coeffs,
                        ProtobufDevice0000E004.TCoefficientsSet.newBuilder().setF0(0F).setT0(0)
                                .setC0(0F).setC1(Float.NaN).setC2(0F),
                        ProtobufDevice0000E004.TCoefficientsSet.Builder.class, false),
                createBuilderVerificator(T2Coeffs,
                        ProtobufDevice0000E004.TCoefficientsSet.newBuilder().setF0(0F).setT0(0)
                                .setC0(0F).setC1(0F).setC2(Float.NaN),
                        ProtobufDevice0000E004.TCoefficientsSet.Builder.class, false),
                createBuilderVerificator(T2Coeffs,
                        ProtobufDevice0000E004.TCoefficientsSet.newBuilder().setF0(0F).setT0(0)
                                .setC0(1F).setC1(0F).setC2(0F),
                        ProtobufDevice0000E004.TCoefficientsSet.Builder.class, false),

                // Просто последний запрос, который должен завершить набор тестов через event.notify();
                new Pair<settingsRequestBuilder, ISuccessCallback>(new settingsRequestBuilder() {
                    @Override
                    public ProtobufDevice0000E004.WriteSettingsReq.Builder build() {
                        return newBuilder();
                    }
                }, response -> {
                    assertNotNull(response.getSettings());

                    synchronized (event) {
                        event.notify();
                    }
                })
        );

        for (Pair<settingsRequestBuilder, ISuccessCallback> test : tests) {
            CommandBuilder builder =
                    service.newCommandBuilder().settingsOperation(test.first.build());
            builder.successCallback = test.second;
            builder.failCallback = errCallback;
            builder.build(ProtobufDevice0000E004.INFO.R4_2_PRODUCTOMER_ID_VALUE)
                    .setTimeout(TIMEOUT).execute();
        }

        synchronized (event) {
            event.wait();
        }

        verify(errCallback, never()).processError(any(), any(Exception.class));
    }

    @Test
    public void testgetHistory() throws Exception {
        final Object event = new Object();

        CommandBuilder commandBuilder = service.newCommandBuilder();

        commandBuilder.successCallback = spy(getSuccessCallback(event));
        commandBuilder.failCallback = spy(getErrorCallback(event));

        IProductomerCommand ping_cmd = commandBuilder
                .getHistoryOperation(ProtobufDevice0000E004.HistoryReq.newBuilder().setMaxSize(-1))
                .build(ProtobufDevice0000E004.INFO.R4_2_PRODUCTOMER_ID_VALUE);

        ping_cmd.setTimeout(TIMEOUT).execute();

        synchronized (event) {
            event.wait();
        }

        verify(commandBuilder.failCallback, never()).processError(any(), any(Exception.class));
    }
}
