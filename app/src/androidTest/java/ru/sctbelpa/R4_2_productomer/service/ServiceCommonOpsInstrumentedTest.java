package ru.sctbelpa.R4_2_productomer.service;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.IBinder;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static ru.sctbelpa.R4_2_productomer.service.TestUtils.findTestDevise;
import static ru.sctbelpa.R4_2_productomer.service.TestUtils.testMAC;


@RunWith(AndroidJUnit4.class)
public class ServiceCommonOpsInstrumentedTest {

    private ProductomerService service;

    @Rule
    public final ServiceTestRule mServiceRule = new ServiceTestRule();

    @Before
    public void prepareService() throws TimeoutException {
        // Create the service Intent.
        Intent serviceIntent =
                new Intent(InstrumentationRegistry.getTargetContext(),
                        ProductomerService.class);

        // Data can be passed to the service via the Intent.
        //serviceIntent.putExtra(ProductomerService.SEED_KEY, 42L);

        // Bind the service and grab a reference to the binder.
        IBinder binder = mServiceRule.bindService(serviceIntent);

        // Get the reference to the service, or you can call
        // public methods on the binder directly.
        service = ((ProductomerService.LocalBinder) binder).getService();
    }

    private void setTargetToTestDevice() {
        BluetoothDevice TestDevice =
                findTestDevise(service.getAdapter().getBondedDevices(), testMAC);
        service.setTargetServer(TestDevice, ProductomerService.defaultUUID);
    }

    @Test
    public void testGetBtAdapter() throws Exception {
        assertNotNull(service.getAdapter());
    }

    @Test
    public void testAdapterIsOn() throws Exception {
        BluetoothAdapter adapter = service.getAdapter();
        assertTrue(adapter.isEnabled());
    }

    @Test
    public void testAdapterState() throws Exception {
        assertEquals(service.getAdapter().getState(), BluetoothAdapter.STATE_ON);
    }

    @Test
    public void testIsTestPointPaired() throws Exception {
        Set<BluetoothDevice> paired = service.adapter.getBondedDevices();
        assertNotNull(findTestDevise(paired, testMAC));
    }

    @Test(expected=NullPointerException.class)
    public void TestNullDevice() throws Exception {
        service.setTargetServer(null, null);
    }

    @Test
    public void testSetTarget() throws Exception {
        Set<BluetoothDevice> paired = service.adapter.getBondedDevices();
        for (BluetoothDevice d: paired) {
            UUID uuid = UUID.randomUUID();
            service.setTargetServer(d, uuid);

            assertEquals(service.getDevice(), d);
            assertEquals(service.getUUID(), uuid);
        }


        BluetoothDevice d = paired.iterator().next();
        service.setTargetServer(d, ProductomerService.defaultUUID);
        assertEquals(service.getDevice(), d);
        service.setTargetServer(d, null);
        assertEquals(service.getUUID(), ProductomerService.defaultUUID);
    }

    @Test
    public void testConnect() throws Exception {
        setTargetToTestDevice();
        service.connect();
        assertTrue(service.isConnected());
    }

    @Test(expected = IOException.class)
    public void testFailToConnect() throws Exception {
        BluetoothDevice TestDevice =
                findTestDevise(service.getAdapter().getBondedDevices(), testMAC);
        service.setTargetServer(TestDevice, TestUtils.invalidUUID);

        service.connect();
    }
}
