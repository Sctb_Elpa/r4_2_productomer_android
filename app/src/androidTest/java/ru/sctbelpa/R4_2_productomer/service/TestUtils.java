package ru.sctbelpa.R4_2_productomer.service;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.Nullable;

import java.util.UUID;

class TestUtils {
    static final String testMAC = "00:80:E1:BF:0C:FF";
    static final UUID invalidUUID = UUID.fromString("00000000-0000-0000-0000-000000000000");

    @Nullable
    static BluetoothDevice findTestDevise(Iterable<BluetoothDevice> devices,
                                          String address) {
        for (BluetoothDevice d : devices) {
            if (d.getAddress().equals(address))
                return d;
        }
        return null;
    }
}
